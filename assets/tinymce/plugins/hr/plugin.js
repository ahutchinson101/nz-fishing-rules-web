(function () {
var hr = (function () {
  'use strict';

  var global = tinymce.util.Tools.resolve('tinymce.PluginManager');

  var register = function (editor) {
    editor.addCommand('InsertHorizontalRule', function () {
      editor.execCommand('mceInsertContent', false, '<hr />');
    });
  };
  var $_640u33c7ji4ujids = { register: register };

  var register$1 = function (editor) {
    editor.addButton('hr', {
      icon: 'hr',
      tooltip: 'Horizontal line',
      cmd: 'InsertHorizontalRule'
    });
    editor.addMenuItem('hr', {
      icon: 'hr',
      text: 'Horizontal line',
      cmd: 'InsertHorizontalRule',
      context: 'insert'
    });
  };
  var $_3fo5cpc8ji4ujidt = { register: register$1 };

  global.add('hr', function (editor) {
    $_640u33c7ji4ujids.register(editor);
    $_3fo5cpc8ji4ujidt.register(editor);
  });
  function Plugin () {
  }

  return Plugin;

}());
})();
