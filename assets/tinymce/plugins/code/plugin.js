(function () {
var code = (function () {
  'use strict';

  var global = tinymce.util.Tools.resolve('tinymce.PluginManager');

  var global$1 = tinymce.util.Tools.resolve('tinymce.dom.DOMUtils');

  var getMinWidth = function (editor) {
    return editor.getParam('code_dialog_width', 600);
  };
  var getMinHeight = function (editor) {
    return editor.getParam('code_dialog_height', Math.min(global$1.DOM.getViewPort().h - 200, 500));
  };
  var $_7epnch9oji4uji2w = {
    getMinWidth: getMinWidth,
    getMinHeight: getMinHeight
  };

  var setContent = function (editor, html) {
    editor.focus();
    editor.undoManager.transact(function () {
      editor.setContent(html);
    });
    editor.selection.setCursorLocation();
    editor.nodeChanged();
  };
  var getContent = function (editor) {
    return editor.getContent({ source_view: true });
  };
  var $_6bmmqb9qji4uji2x = {
    setContent: setContent,
    getContent: getContent
  };

  var open = function (editor) {
    var minWidth = $_7epnch9oji4uji2w.getMinWidth(editor);
    var minHeight = $_7epnch9oji4uji2w.getMinHeight(editor);
    var win = editor.windowManager.open({
      title: 'Source code',
      body: {
        type: 'textbox',
        name: 'code',
        multiline: true,
        minWidth: minWidth,
        minHeight: minHeight,
        spellcheck: false,
        style: 'direction: ltr; text-align: left'
      },
      onSubmit: function (e) {
        $_6bmmqb9qji4uji2x.setContent(editor, e.data.code);
      }
    });
    win.find('#code').value($_6bmmqb9qji4uji2x.getContent(editor));
  };
  var $_2yrr7z9nji4uji2v = { open: open };

  var register = function (editor) {
    editor.addCommand('mceCodeEditor', function () {
      $_2yrr7z9nji4uji2v.open(editor);
    });
  };
  var $_7widig9mji4uji2u = { register: register };

  var register$1 = function (editor) {
    editor.addButton('code', {
      icon: 'code',
      tooltip: 'Source code',
      onclick: function () {
        $_2yrr7z9nji4uji2v.open(editor);
      }
    });
    editor.addMenuItem('code', {
      icon: 'code',
      text: 'Source code',
      onclick: function () {
        $_2yrr7z9nji4uji2v.open(editor);
      }
    });
  };
  var $_6ujflm9rji4uji2y = { register: register$1 };

  global.add('code', function (editor) {
    $_7widig9mji4uji2u.register(editor);
    $_6ujflm9rji4uji2y.register(editor);
    return {};
  });
  function Plugin () {
  }

  return Plugin;

}());
})();
