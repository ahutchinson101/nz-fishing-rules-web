jQuery(document).ready(function(e) {

	let maxtextsize= 100;
	let fontsize_list=""

	for (let i = 1 ; i < maxtextsize; i++){
		if (i === maxtextsize-1 ){
			fontsize_list += i+"px";
		}else{
			fontsize_list += i+"px ";
		}
	}

	$('textarea[name="region_msg"]').each(function (){
		$(this).tinymce({
			mode: "none",
			selector: "textarea",
			fontsize_formats: fontsize_list,
			font_formats: "Poppins='Poppins', sans-serif;Open Sans=Open Sans,sans-serif;Andale Mono=andale mono,times;" +
				"Arial=arial,helvetica,sans-serif;" +
				"Arial Black=arial black,avant garde;" +
				"Book Antiqua=book antiqua,palatino;" +
				"Comic Sans MS=comic sans ms,sans-serif;" +
				"Courier New=courier new,courier;" +
				"Georgia=georgia,palatino;" +
				"Helvetica=helvetica;" +
				"Impact=impact,chicago;" +
				"Symbol=symbol;" +
				"Tahoma=tahoma,arial,helvetica,sans-serif;" +
				"Terminal=terminal,monaco;" +
				"Times New Roman=times new roman,times;" +
				"Trebuchet MS=trebuchet ms,geneva;" +
				"Verdana=verdana,geneva;" +
				"Webdings=webdings;" +
				"Wingdings=wingdings,zapf dingbats",
			theme: "modern",
			plugins: [
				"advlist autolink lists link image charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code fullscreen",
				"insertdatetime media nonbreaking save table contextmenu directionality",
				"emoticons template paste textcolor colorpicker textpattern selectfish selectregion filemanager"
			],
			toolbar1: "fontselect |  fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
			toolbar2: "forecolor | selectfish selectregion filemanager",
			image_advtab: true,
			templates: [
				{title: 'Test template 1', content: 'Test 1'},
				{title: 'Test template 2', content: 'Test 2'}
			],
			external_filemanager_path: "/assets/tinymce/plugins/filemanager/",
			filemanager_title: "Responsive Filemanager",
			external_plugins: {"filemanager": "/assets/tinymce/plugins/filemanager/plugin.min.js"},
			file_picker_callback: function (cb, value, meta) {
				var width = window.innerWidth - 30;
				var height = window.innerHeight - 60;
				if (width > 1800) width = 1800;
				if (height > 1200) height = 1200;
				if (width > 600) {
					var width_reduce = (width - 20) % 138;
					width = width - width_reduce + 10;
				}
				var urltype = 2;
				if (meta.filetype == 'image') {
					urltype = 1;
				}
				if (meta.filetype == 'media') {
					urltype = 3;
				}
				var title = "RESPONSIVE FileManager";
				if (typeof this.settings.filemanager_title !== "undefined" && this.settings.filemanager_title) {
					title = this.settings.filemanager_title;
				}
				var akey = "key";
				if (typeof this.settings.filemanager_access_key !== "undefined" && this.settings.filemanager_access_key) {
					akey = this.settings.filemanager_access_key;
				}
				var sort_by = "";
				if (typeof this.settings.filemanager_sort_by !== "undefined" && this.settings.filemanager_sort_by) {
					sort_by = "&sort_by=" + this.settings.filemanager_sort_by;
				}
				var descending = "false";
				if (typeof this.settings.filemanager_descending !== "undefined" && this.settings.filemanager_descending) {
					descending = this.settings.filemanager_descending;
				}
				var fldr = "";
				if (typeof this.settings.filemanager_subfolder !== "undefined" && this.settings.filemanager_subfolder) {
					fldr = "&fldr=" + this.settings.filemanager_subfolder;
				}
				var crossdomain = "";
				if (typeof this.settings.filemanager_crossdomain !== "undefined" && this.settings.filemanager_crossdomain) {
					crossdomain = "&crossdomain=1";
					if (window.addEventListener) {
						window.addEventListener('message', filemanager_onMessage, false);
					} else {
						window.attachEvent('onmessage', filemanager_onMessage);
					}
				}
				tinymce.activeEditor.windowManager.open({
					title: title,
					file: this.settings.external_filemanager_path + 'dialog.php?type=' + urltype + '&descending=' + descending + sort_by + fldr + crossdomain + '&lang=' + this.settings.language + '&akey=' + akey,
					width: width,
					height: height,
					resizable: true,
					maximizable: true,
					inline: 1
				}, {
					setUrl: function (url) {
						cb(url);
					}
				});
			}
		});
	});

	/**/

	$('textarea[name="extra_note"]').each(function (){
		$(this).tinymce({
			mode: "none",
			selector: "textarea",
			fontsize_formats: fontsize_list,
			font_formats: "Poppins='Poppins', sans-serif;Open Sans=Open Sans,sans-serif;Andale Mono=andale mono,times;" +
				"Arial=arial,helvetica,sans-serif;" +
				"Arial Black=arial black,avant garde;" +
				"Book Antiqua=book antiqua,palatino;" +
				"Comic Sans MS=comic sans ms,sans-serif;" +
				"Courier New=courier new,courier;" +
				"Georgia=georgia,palatino;" +
				"Helvetica=helvetica;" +
				"Impact=impact,chicago;" +
				"Symbol=symbol;" +
				"Tahoma=tahoma,arial,helvetica,sans-serif;" +
				"Terminal=terminal,monaco;" +
				"Times New Roman=times new roman,times;" +
				"Trebuchet MS=trebuchet ms,geneva;" +
				"Verdana=verdana,geneva;" +
				"Webdings=webdings;" +
				"Wingdings=wingdings,zapf dingbats",
			theme: "modern",
			plugins: [
				"advlist autolink lists link image charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code fullscreen",
				"insertdatetime media nonbreaking save table contextmenu directionality",
				"emoticons template paste textcolor colorpicker textpattern selectfish selectregion filemanager"
			],
			toolbar1: "fontselect |  fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
			toolbar2: "forecolor | selectfish selectregion filemanager",
			image_advtab: true,
			templates: [
				{title: 'Test template 1', content: 'Test 1'},
				{title: 'Test template 2', content: 'Test 2'}
			],
			external_filemanager_path: "/assets/tinymce/plugins/filemanager/",
			filemanager_title: "Responsive Filemanager",
			external_plugins: {"filemanager": "/assets/tinymce/plugins/filemanager/plugin.min.js"},
			file_picker_callback: function (cb, value, meta) {
				var width = window.innerWidth - 30;
				var height = window.innerHeight - 60;
				if (width > 1800) width = 1800;
				if (height > 1200) height = 1200;
				if (width > 600) {
					var width_reduce = (width - 20) % 138;
					width = width - width_reduce + 10;
				}
				var urltype = 2;
				if (meta.filetype == 'image') {
					urltype = 1;
				}
				if (meta.filetype == 'media') {
					urltype = 3;
				}
				var title = "RESPONSIVE FileManager";
				if (typeof this.settings.filemanager_title !== "undefined" && this.settings.filemanager_title) {
					title = this.settings.filemanager_title;
				}
				var akey = "key";
				if (typeof this.settings.filemanager_access_key !== "undefined" && this.settings.filemanager_access_key) {
					akey = this.settings.filemanager_access_key;
				}
				var sort_by = "";
				if (typeof this.settings.filemanager_sort_by !== "undefined" && this.settings.filemanager_sort_by) {
					sort_by = "&sort_by=" + this.settings.filemanager_sort_by;
				}
				var descending = "false";
				if (typeof this.settings.filemanager_descending !== "undefined" && this.settings.filemanager_descending) {
					descending = this.settings.filemanager_descending;
				}
				var fldr = "";
				if (typeof this.settings.filemanager_subfolder !== "undefined" && this.settings.filemanager_subfolder) {
					fldr = "&fldr=" + this.settings.filemanager_subfolder;
				}
				var crossdomain = "";
				if (typeof this.settings.filemanager_crossdomain !== "undefined" && this.settings.filemanager_crossdomain) {
					crossdomain = "&crossdomain=1";
					if (window.addEventListener) {
						window.addEventListener('message', filemanager_onMessage, false);
					} else {
						window.attachEvent('onmessage', filemanager_onMessage);
					}
				}
				tinymce.activeEditor.windowManager.open({
					title: title,
					file: this.settings.external_filemanager_path + 'dialog.php?type=' + urltype + '&descending=' + descending + sort_by + fldr + crossdomain + '&lang=' + this.settings.language + '&akey=' + akey,
					width: width,
					height: height,
					resizable: true,
					maximizable: true,
					inline: 1
				}, {
					setUrl: function (url) {
						cb(url);
					}
				});
			}
		});
	});


	Dropzone.options.myAwesomeDropzone = {
		init: function() {


			this.on("addedfile", function(file) {

				// Create the remove button
				var removeButton = Dropzone.createElement("<div class='dz-delete-mark'><button type='button' class='button'>Remove file</button></div>");


				// Capture the Dropzone instance as closure.
				var _this = this;

				// Listen to the click event
				removeButton.addEventListener("click", function(e) {
					// Make sure the button click doesn't submit the form:
					e.preventDefault();
					e.stopPropagation();

					console.log(file);


					var deleteFishImageUrl = $('#deleteFishImageUrl').val();
					if(file.id) {
						jQuery.ajax({
							url:deleteFishImageUrl,
							data:{
								fish_image_id: file.id
							},
							type:'POST',
							success:function(response) {

							}
						});
					} else {
						jQuery.ajax({
							url:deleteFishImageUrl,
							data:{
								file_name: file.name
							},
							type:'POST',
							success:function(response) {

							}
						});
					}
					// Remove the file preview.
					_this.removeFile(file);
					// If you want to the delete the file on the server as well,
					// you can do the AJAX request here.
				});
				// Add the button to the file preview element.
				file.previewElement.appendChild(removeButton);
			});


			let myDropzone = this;

			var images = JSON.parse($('textarea[name="images_json"]').val());

			var baseurl=jQuery("#fishurl").val();

			$.each(images,function (index,obj) {
				if(typeof obj == 'object') {
					myDropzone.displayExistingFile(obj, baseurl+obj.fish_image);
				} else {
					myDropzone.displayExistingFile({'name':obj}, baseurl+obj);
				}
			})


			// If you only have access to the original image sizes on your server,
			// and want to resize them in the browser:
			/*let mockFile = { name: "Filename 2", size: 12345 };
			myDropzone.displayExistingFile(mockFile, "https://i.picsum.photos/id/959/600/600.jpg");

			// If the thumbnail is already in the right size on your server:
			let mockFile = { name: "Filename", size: 12345 };
			let callback = null; // Optional callback when it's done
			let crossOrigin = null; // Added to the `img` tag for crossOrigin handling
			let resizeThumbnail = false; // Tells Dropzone whether it should resize the image first
			myDropzone.displayExistingFile(mockFile, "https://i.picsum.photos/id/959/120/120.jpg", callback, crossOrigin, resizeThumbnail);

			// If you use the maxFiles option, make sure you adjust it to the
			// correct amount:
			let fileCountOnServer = 2; // The number of files already uploaded
			myDropzone.options.maxFiles = myDropzone.options.maxFiles - fileCountOnServer;*/




		}
	};



	jQuery('#region').live("change",function(){
		var id=jQuery(this).val();
		var baseurl=jQuery("#baseurl").val();
		window.location=baseurl+id;
		//return false;
	});
	jQuery('#region_res').live("change",function(){
		var id=jQuery(this).val();
		var baseurl=jQuery("#baseurl").val();
		window.location=baseurl+id;
		//return false;
	});

	jQuery('input[name="regions[]"]').live("click",function() {

		var regions = [];
		jQuery('input[name="regions[]"]').each(function (index, element) {
			if(jQuery(this).is(':checked')) {
				regions.push(jQuery(this).val());
			}
		});

		if(regions.length) {
			var baseurl=jQuery("#check_fish_new_url").val();
			jQuery.ajax({
				url:baseurl,
				dataType:'HTML',
				type:'POST',
				data: {
					'regions':regions
				},
				success:function(response)
				{
					jQuery("#fish_combo").css("display","block");
					jQuery("#fish_combo").html(response);
					jQuery( "#yes" ).prop( "checked", false);
					jQuery( "#no" ).prop( "checked", true );
					jQuery("label[for='no']").addClass("ui-state-active");
					jQuery("#fish_detail").css("display","none");
					jQuery("#restricted_toggle").css("display","none");
					jQuery("#restricted_detail").css("display","none");
				}
			});
		} else {
			jQuery("#fish_combo").html('');
		}


	});


	jQuery('#add_fish_region').live("change",function(){
		var id=jQuery(this).val();
		var baseurl=jQuery("#baseurl").val();
		baseurl=baseurl+id;
		jQuery.ajax({
			url:baseurl,
			dataType:'HTML',
			type:'GET',
			success:function(response){
				jQuery("#fish_combo").css("display","block");
				jQuery("#fish_combo").html(response);
				jQuery( "#yes" ).prop( "checked", false);
				jQuery( "#no" ).prop( "checked", true );
				jQuery("label[for='no']").addClass("ui-state-active");
				jQuery("#fish_detail").css("display","none");
				jQuery("#restricted_toggle").css("display","none");
				jQuery("#restricted_detail").css("display","none");
			}
		});
	});
	
	jQuery("#fish").live("change",function(){
		var region=jQuery("#add_fish_region").val();
		var fish=jQuery("#fish").val();
		var id=jQuery(this).val();
		var baseurl=jQuery("#baseurl_fish").val();
		//baseurl=baseurl+region+'/'+fish;
		baseurl=baseurl+fish;
		jQuery.ajax({
			url:baseurl,
			dataType:'HTML',
			type:'GET',
			success:function(response){
				jQuery("#fish_detail").css("display","block");
				jQuery("#fish_detail").html(response);
				jQuery("#restricted_toggle").css("display","block");
				jQuery( "#radioset" ).buttonset();
				//jQuery("#restricted_detail").css("display","none");
			}
		});
		/*baseurl=jQuery("#baseurl_rest").val();
		baseurl=baseurl+region+'/'+fish;
		jQuery.ajax({
			url:baseurl,
			dataType:'HTML',
			type:'GET',
			success:function(response){
				if(response==0){
					jQuery( "#yes" ).prop( "checked", false);
					jQuery( "#no" ).prop( "checked", true );
					jQuery("label[for='no']").addClass("ui-state-active");
					jQuery("label[for='yes']").removeClass("ui-state-active");
					jQuery("#restricted_detail").css("display","none");
					jQuery("#restricted_data").html('<ul id="rest_sort" class="restricted_detail_item"><li id="nodata">No Data Available.</li></ul>');	
				}
				else
				{
					jQuery( "#yes" ).prop( "checked", true);
					jQuery( "#no" ).prop( "checked", false);
					jQuery("label[for='yes']").addClass("ui-state-active");
					jQuery("label[for='no']").removeClass("ui-state-active");
					jQuery("#restricted_detail").css("display","block");
					loadjquerydata(region,fish,response);
				}
			}
		});*/
	});

    jQuery("#toxin_alerts_fish_region").live("change",function(){
        var region=jQuery("#toxin_alerts_fish_region").val();
        var baseurl=jQuery("#alertslist_url").val();
        baseurl=baseurl+region;
		if(jQuery(this).val() == null || jQuery(this).val() == ""){
			baseurl = baseurl.replace('/alert_list/','');
		}
        window.location.replace(baseurl);
    });

	jQuery("#closures_fish_region").live("change",function(){
		var region=jQuery("#closures_fish_region").val();
		var baseurl=jQuery("#closureslist_url").val();
		baseurl=baseurl+region;
		if(jQuery(this).val() == null || jQuery(this).val() == ""){
			baseurl = baseurl.replace('/closure_list/','');
		}
		window.location.replace(baseurl);
		/*jQuery.ajax({
			url:baseurl,
			dataType:'HTML',
			type:'GET',
			success:function(response){
				jQuery("#restricted_detail").css("display","block");
				if(response==0)
				{
					jQuery("#restricted_detail").css("display","block");
					jQuery("#restricted_data").html('<ul id="rest_sort" class="restricted_detail_item"><li id="nodata">No Data Available.</li></ul>');						
				}
				else
				{
					jQuery("#restricted_detail").css("display","block");
					loadclosuresjquerydata(region,response);
				}
			}
		});*/
	});
	
	jQuery('#rest_sort').sortable({
		update: function(event, ui) {
			var restOrder = jQuery(this).sortable('toArray', { attribute: 'id' });
			var region=jQuery("#closures_fish_region").val();
			var baseurl=jQuery("#closures_sorturl").val();
			baseurl=baseurl+region;
			jQuery.ajax({
				url:baseurl,
				data:{
			        sortables: restOrder
			    },
				type:'POST',
				success:function(response){

				}
			});
		}
	});

    jQuery('#toxin_sort').sortable({
        update: function(event, ui) {
            var restOrder = jQuery(this).sortable('toArray', { attribute: 'id' });
            var region=jQuery("#toxin_alerts_fish_region").val();
            var baseurl=jQuery("#alerts_sorturl").val();
            baseurl=baseurl+region;
            jQuery.ajax({
                url:baseurl,
                data:{
                    sortables: restOrder
                },
                type:'POST',
                success:function(response){

                }
            });
        }
    });

	jQuery(".remove_closure").live("click",function(){
		if(confirm('Are you sure delete this record and his data?')){
		var currentEle =  jQuery(this);
		var id=this.id;
		var baseurl=jQuery("#closures_deleteurl").val();
		baseurl=baseurl+id;
		jQuery.ajax({
			url:baseurl,
			dataType:'HTML',
			type:'GET',
			success:function(response){
				if(response==1)
				{
					jQuery(".message").css("display","none");
					jQuery(currentEle).closest('li').remove();
					
					if(jQuery("#rest_sort .liitem").length>0)
					{
						
					}
					else
					{
						jQuery("#rest_sort").append('<li id="nodata">No Data Available.</li>');
						return false;
					}
				}
				else{
					jQuery(".message").css("display","block");
					jQuery('.message').text('Restricted Fish in Region Not Deleted.');
				}
			}
		});
		}
		return false;
	});

    jQuery(".remove_toxin_alert").live("click",function(){
        if(confirm('Are you sure delete this record and his data?')){
            var currentEle =  jQuery(this);
            var id=this.id;
            var baseurl=jQuery("#alerts_deleteurl").val();
            baseurl=baseurl+id;
            jQuery.ajax({
                url:baseurl,
                dataType:'HTML',
                type:'GET',
                success:function(response){
                    if(response==1)
                    {
                        jQuery(".message").css("display","none");
                        jQuery(currentEle).closest('li').remove();

                        if(jQuery("#toxin_sort .liitem").length>0)
                        {

                        }
                        else
                        {
                            jQuery("#toxin_sort").append('<li id="nodata">No Data Available.</li>');
                            return false;
                        }
                    }
                    else{
                        jQuery(".message").css("display","block");
                        jQuery('.message').text('Toxin Alert in Region Not Deleted.');
                    }
                }
            });
        }
        return false;
    });
	
	/*jQuery(".edit_closure").live("click",function(){
		var currentEle =  jQuery(this);
		jQuery(currentEle).closest('li').children('.item').css("display","none");
		jQuery(currentEle).closest('li').children('#closures_restrictionsupdatediv').css("display","block");
		return false;
	});*/

    /*jQuery(".edit_toxin_alert").live("click",function(){
        var currentEle =  jQuery(this);
        jQuery(currentEle).closest('li').children('.item').css("display","none");
        jQuery(currentEle).closest('li').children('#toxin_alertsupdatediv').css("display","block");
        return false;
    });*/
	
	jQuery('button[name="cancelbutton"]').live('click',function(){
		var currentEle =  jQuery(this);
		jQuery(".message").css("display","none");
		jQuery(currentEle).closest('li').children('#closures_restrictionsupdatediv').css("display","none");
		jQuery(currentEle).closest('li').children('.item').css("display","block");
	});

    jQuery('button[name="toxin_alert_cancelbutton"]').live('click',function(){
        var currentEle =  jQuery(this);
        jQuery(".message").css("display","none");
        jQuery(currentEle).closest('li').children('#toxin_alertsupdatediv').css("display","none");
        jQuery(currentEle).closest('li').children('.item').css("display","block");
    });

	jQuery('form[name="toxin_alertsupdateform"]').live('submit', function () {

        var currentEle =  jQuery(this);
        var rst_id = currentEle.find('input[name="toxin_alert_id"]').val();
        var areas = currentEle.find('input[name="areas"]').val();
        var title = currentEle.find('input[name="title"]').val();
        var date_added = currentEle.find('input[name="date_added"]').val();
        var key_points = currentEle.find('input[name="key_points"]').val();
        var content = currentEle.find('textarea[name="content"]').val();
        var oldcontent = currentEle.find('input[name="oldcontent"]').val();

        if(title==""){
            jQuery(".message").css("display","block");
            jQuery('.message').append('Insert title.');
            return false;
        }
        /*if(areas=="")
        {
            jQuery(".message").css("display","block");
            jQuery('.message').append('Insert areas.');
            return false;
        }*/

        var baseurl=jQuery("#alerts_upadteurl").val();
        baseurl=baseurl+rst_id;

        jQuery.ajax({
            url:baseurl,
            data:{ "areas":areas,"date_added":date_added,"title":title,"key_points":key_points,"content":content,"oldcontent":oldcontent },
            dataType:'HTML',
            type:'POST',
            success:function(response){
                if(response==1)
                {
					var editToxinAlertElement = jQuery('#edit-toxin-alert-'+rst_id);
                    jQuery(".message").css("display","none");
					jQuery('#toxin_alertsupdatediv').dialog('close');
                    editToxinAlertElement.closest('li').children('.item').html('<span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span>'+title+' '+areas);
                    editToxinAlertElement.closest('li').children('.item').css("display","block");
                }
                else
                {
                    jQuery(".message").css("display","block");
                    jQuery('.message').append('Toxin Alert not updated.');
                }
            }
        });
        //event.preventDefault(); // if you want to disable the action
        return false;
    });

	jQuery('form[name="closures_restrictionsupdateform"]').live('submit', function ()
	{
		var currentEle =  jQuery(this);
		var rst_id = currentEle.find('input[name="closure_updateid"]').val();
		var areas = currentEle.find('input[name="areas"]').val();
		var title = currentEle.find('input[name="title"]').val();
		var date_added = currentEle.find('input[name="date_added"]').val();
		var key_points = currentEle.find('input[name="key_points"]').val();
		var content = currentEle.find('textarea[name="content"]').val();
		var oldcontent = currentEle.find('input[name="oldcontent"]').val();
		
		if(title==""){
			jQuery(".message").css("display","block");
			jQuery('.message').append('Insert title.');
			return false;
		}
		/*if(areas=="")
		{
			jQuery(".message").css("display","block");
			jQuery('.message').append('Insert areas.');
			return false;
		}*/
		
		var baseurl=jQuery("#closures_upadteurl").val();
		baseurl=baseurl+rst_id;
		
		jQuery.ajax({
			url:baseurl,
			data:{ "areas":areas,"date_added":date_added,"title":title,"key_points":key_points,"content":content,"oldcontent":oldcontent },
			dataType:'HTML',
			type:'POST',
			success:function(response){
				if(response==1)
				{
					var editClosureElement = jQuery('#edit-closure-'+rst_id);
					jQuery(".message").css("display","none");
					jQuery('#closures_restrictionsupdatediv').dialog('close');
					editClosureElement.closest('li').children('.item').html('<span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span>'+title+' '+areas);
					editClosureElement.closest('li').children('.item').css("display","block");
				}
				else
				{
					jQuery(".message").css("display","block");
					jQuery('.message').append('Closures and Restrictions not updated.');
				}
			}
		});
		//event.preventDefault(); // if you want to disable the action
		return false;
	});
	
	jQuery(".is_restricted_areas").live("change",function(){
		if (jQuery("#yes").attr("checked")) {
			var region=jQuery("#add_fish_region").val();
			var fish=jQuery("#fish").val();
			var baseurl=jQuery("#baseurl_rest").val();
			baseurl=baseurl+region+'/'+fish;
			jQuery.ajax({
				url:baseurl,
				dataType:'HTML',
				type:'GET',
				success:function(response){
					jQuery("#restricted_detail").css("display","block");
					if(response==0)
					{
						jQuery("#restricted_data").html('<ul id="rest_sort" class="restricted_detail_item"><li id="nodata">No Data Available.</li></ul>');						
					}
					else
					{
						loadjquerydata(region,fish,response);
					}
					
				}
			});
        }
        else {
        	jQuery("#restricted_detail").css("display","none");
        }
	});
	
	jQuery('#toxin_alerts_form').submit(function(event)
	{

        var currentEle =  jQuery(this);
        var region=jQuery("#toxin_alerts_fish_region").val();
        var areas = currentEle.find('input[name="areas"]').val();
        var title = currentEle.find('input[name="title"]').val();
        var date_added = currentEle.find('input[name="date_added"]').val();
        var key_points = currentEle.find('input[name="key_points"]').val();
        var content = currentEle.find('textarea[name="content"]').val();

        if(region=="")
        {
            jQuery(".message").css("display","block");
            jQuery('.message').append('Select Region.');
            return false;
        }else if(title==""){
            jQuery(".message").css("display","block");
            jQuery('.message').append('Insert title.');
            return false;
        }
        /*if(areas=="")   comment date 5 12 2014
        {
            jQuery(".message").css("display","block");
            jQuery('.message').append('Insert areas.');
            return false;
        }*/

        var baseurl=jQuery("#baseurltext").val();
        baseurl=baseurl+region;
        jQuery.ajax({
            url:baseurl,
            data:{ "areas":areas,"date_added":date_added,"title":title,"key_points":key_points,"content":content },
            dataType:'HTML',
            type:'POST',
            success:function(response){
                if(response==1)
                {
                    jQuery(".message").css("display","block");
                    jQuery('.message').append('Closures and Restrictions not added.');
                }
                else
                {
                    location.reload(true);
                }
            }
        });
        event.preventDefault(); // if you want to disable the action
        return false;

    });

	jQuery('#closures_restrictionsform').submit(function(event)
	{
		var currentEle =  jQuery(this);
		var region=jQuery("#closures_fish_region").val();
		var areas = currentEle.find('input[name="areas"]').val();
		var title = currentEle.find('input[name="title"]').val();
		var date_added = currentEle.find('input[name="date_added"]').val();
		var key_points = currentEle.find('input[name="key_points"]').val();
		var content = currentEle.find('textarea[name="content"]').val();
		
		if(region=="")
		{
			jQuery(".message").css("display","block");
			jQuery('.message').append('Select Region.');
			return false;
		}else if(title==""){
			jQuery(".message").css("display","block");
			jQuery('.message').append('Insert title.');
			return false;
		}
		/*if(areas=="")   comment date 5 12 2014
		{
			jQuery(".message").css("display","block");
			jQuery('.message').append('Insert areas.');
			return false;
		}*/
		
		var baseurl=jQuery("#baseurltext").val();
		baseurl=baseurl+region;
		jQuery.ajax({
			url:baseurl,
			data:{ "areas":areas,"date_added":date_added,"title":title,"key_points":key_points,"content":content },
			dataType:'HTML',
			type:'POST',
			success:function(response){
				if(response==1)
				{
					jQuery(".message").css("display","block");
					jQuery('.message').append('Closures and Restrictions not added.');
				}
				else
				{
					location.reload(true);
					//jQuery("#nodata").remove();
					//jQuery(".message").css("display","none");
					//jQuery("#areas").val('');
					//jQuery("#title").val('');
					//jQuery("#key_points").val('');
					//jQuery("#newcontent").val('');
					//jQuery(".restricted_detail_item").append(response);
					//jQuery( "#closures_restrictionsdiv" ).dialog("close");
				}
			}
		});
		event.preventDefault(); // if you want to disable the action
		return false;
	});
	
	jQuery('#cancelbuttonimage').live('click',function(){
		jQuery("#restricted_imagediv").dialog("close");
		//jQuery("#restricted_textdiv").dialog("close");
		//jQuery('#restricted_imagediv').css('display','none');
		//jQuery('#restricted_textdiv').css('display','none');
	});
	jQuery('#cancelbuttontext').live('click',function(){
		//jQuery("#restricted_imagediv").dialog("close");
		jQuery("#restricted_textdiv").dialog("close");
		//jQuery('#restricted_imagediv').css('display','none');
		//jQuery('#restricted_textdiv').css('display','none');
	});
	jQuery('#restrictedtextform').submit(function(event)
	{
		var region=jQuery("#add_fish_region").val();
		var fish=jQuery("#fish").val();
		var restricted_msg=jQuery("#restricted_msg").val();
		var is_bold=0;
		if (jQuery("#is_restricted_text_bold_add").is(":checked"))
		{
			is_bold=1;
		}
		if(region=="")
		{
			jQuery(".message").css("display","block");
			jQuery('.message').append('Select Region.');
			return false;
		}
		if(fish=="")
		{
			jQuery(".message").css("display","block");
			jQuery('.message').append('Select Fish.');
			return false;
		}
		if(restricted_msg=="")
		{
			jQuery(".message").css("display","block");
			jQuery('.message').append('Insert Text.');
			return false;
		}
		var baseurl=jQuery("#baseurltext").val();
		baseurl=baseurl+region+'/'+fish;
		jQuery.ajax({
			url:baseurl,
			data:{ "msg":restricted_msg,"is_restricted_text_bold":is_bold },
			dataType:'HTML',
			type:'GET',
			success:function(response){
				if(response==1)
				{
					jQuery(".message").css("display","block");
					jQuery('.message').append('Restricted Fish in region not added.');
				}
				else
				{
					jQuery("#nodata").remove();
					jQuery(".message").css("display","none");
					jQuery("#restricted_msg").val('');
					jQuery("#is_restricted_text_bold").attr('checked', false);
					jQuery(".restricted_detail_item").append(response);
					jQuery( "#restricted_textdiv" ).dialog("close");
				}
			}
		});
		event.preventDefault(); // if you want to disable the action
		return false;
	});
	
	jQuery("#add_image").live("click",function(){
		var region=jQuery("#add_fish_region").val();
		var fish=jQuery("#fish").val();
		jQuery("#region").val(region);
		jQuery("#fishid").val(fish);
		jQuery("#restricted_imagediv").dialog("open");
		jQuery( "#restricted_textdiv" ).dialog("close");
		//jQuery("#restricted_imagediv").css("display","block");
        //jQuery("#restricted_textdiv").css("display","none");
        return false;
	});
	jQuery("#restricted_imagediv").dialog({
		autoOpen: false,
		height: 225,
		width: 400,
		modal: true,
		resizable:false
	 });
	jQuery("#restricted_textdiv").dialog({
		autoOpen: false,
		height: 225,
		width: 450,
		modal: true,
		resizable:false
	 });
	jQuery("#closures_restrictionsdiv, #closures_restrictionsupdatediv, #toxin_alertsupdatediv").dialog({
		autoOpen: false,
		height: 550,
		width: 800,
		modal: true,
		resizable:false
 	});


	jQuery("#toxin_alertsdiv").dialog({
		autoOpen: false,
		height: 550,
		width: 800,
		modal: true,
		resizable:false
	 });
	jQuery("#add_text").live("click",function(){
		jQuery("#restricted_textdiv").dialog("open");
		jQuery("#restricted_imagediv").dialog("close");
        return false;
	});

	jQuery('#cancelclosures').live('click',function(){
		jQuery("#closures_restrictionsdiv").dialog("close");
	});

	jQuery('.close-dialog').live('click',function(){
		jQuery('#closures_restrictionsupdatediv').dialog("close");
	});

	jQuery('#toxin_alert_cancelbutton').live('click',function(){
		jQuery('#toxin_alertsupdatediv').dialog("close");
	});

    jQuery('#cancel_toxin_alerts').live('click',function(){
        jQuery("#toxin_alertsdiv").dialog("close");
    });


	jQuery("#add_toxin_alerts").live("click",function(){
        jQuery("#toxin_alertsdiv").dialog("open");
        return false;
    });

	jQuery("#addclosures_restrictions").live("click",function(){
		jQuery("#closures_restrictionsdiv").dialog("open");
		return false;
	});
	
	jQuery('.edit_closure').live('click', function () {
		let id = $(this).data('id');
		let messageContainer = jQuery(".message");
		jQuery.ajax({
			type: 'get',
			url: '/closures_and_restrictions/edit_closure/'+id,
			success: function (response) {
				if (response.status === 'success') {
					var dialogElement = jQuery('#closures_restrictionsupdatediv');
					dialogElement.html(response.response.html);
					dialogElement.find("input[type=date]").dateinput({
						format: "dd mmmm yyyy"
					});
					tinyMCE.remove("#edit-closure-content");
					dialogElement.find('#edit-closure-content').tinymce({
						mode: "none",
						selector: "textarea",
						fontsize_formats: fontsize_list,
						font_formats: "Poppins='Poppins', sans-serif;Open Sans=Open Sans,sans-serif;Andale Mono=andale mono,times;" +
							"Arial=arial,helvetica,sans-serif;" +
							"Arial Black=arial black,avant garde;" +
							"Book Antiqua=book antiqua,palatino;" +
							"Comic Sans MS=comic sans ms,sans-serif;" +
							"Courier New=courier new,courier;" +
							"Georgia=georgia,palatino;" +
							"Helvetica=helvetica;" +
							"Impact=impact,chicago;" +
							"Symbol=symbol;" +
							"Tahoma=tahoma,arial,helvetica,sans-serif;" +
							"Terminal=terminal,monaco;" +
							"Times New Roman=times new roman,times;" +
							"Trebuchet MS=trebuchet ms,geneva;" +
							"Verdana=verdana,geneva;" +
							"Webdings=webdings;" +
							"Wingdings=wingdings,zapf dingbats",
						theme: "modern",
						plugins: [
							"advlist autolink lists link image charmap print preview hr anchor pagebreak",
							"searchreplace wordcount visualblocks visualchars code fullscreen",
							"insertdatetime media nonbreaking save table contextmenu directionality",
							"emoticons template paste textcolor colorpicker textpattern selectfish selectregion filemanager"
						],
						toolbar1: "fontselect |  fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
						toolbar2: "forecolor | selectfish selectregion filemanager",
						image_advtab: true,
						templates: [
							{title: 'Test template 1', content: 'Test 1'},
							{title: 'Test template 2', content: 'Test 2'}
						],
						external_filemanager_path: "/assets/tinymce/plugins/filemanager/",
						filemanager_title: "Responsive Filemanager",
						external_plugins: {"filemanager": "/assets/tinymce/plugins/filemanager/plugin.min.js"},
						file_picker_callback: function (cb, value, meta) {
							var width = window.innerWidth - 30;
							var height = window.innerHeight - 60;
							if (width > 1800) width = 1800;
							if (height > 1200) height = 1200;
							if (width > 600) {
								var width_reduce = (width - 20) % 138;
								width = width - width_reduce + 10;
							}
							var urltype = 2;
							if (meta.filetype == 'image') {
								urltype = 1;
							}
							if (meta.filetype == 'media') {
								urltype = 3;
							}
							var title = "RESPONSIVE FileManager";
							if (typeof this.settings.filemanager_title !== "undefined" && this.settings.filemanager_title) {
								title = this.settings.filemanager_title;
							}
							var akey = "key";
							if (typeof this.settings.filemanager_access_key !== "undefined" && this.settings.filemanager_access_key) {
								akey = this.settings.filemanager_access_key;
							}
							var sort_by = "";
							if (typeof this.settings.filemanager_sort_by !== "undefined" && this.settings.filemanager_sort_by) {
								sort_by = "&sort_by=" + this.settings.filemanager_sort_by;
							}
							var descending = "false";
							if (typeof this.settings.filemanager_descending !== "undefined" && this.settings.filemanager_descending) {
								descending = this.settings.filemanager_descending;
							}
							var fldr = "";
							if (typeof this.settings.filemanager_subfolder !== "undefined" && this.settings.filemanager_subfolder) {
								fldr = "&fldr=" + this.settings.filemanager_subfolder;
							}
							var crossdomain = "";
							if (typeof this.settings.filemanager_crossdomain !== "undefined" && this.settings.filemanager_crossdomain) {
								crossdomain = "&crossdomain=1";
								if (window.addEventListener) {
									window.addEventListener('message', filemanager_onMessage, false);
								} else {
									window.attachEvent('onmessage', filemanager_onMessage);
								}
							}
							tinymce.activeEditor.windowManager.open({
								title: title,
								file: this.settings.external_filemanager_path + 'dialog.php?type=' + urltype + '&descending=' + descending + sort_by + fldr + crossdomain + '&lang=' + this.settings.language + '&akey=' + akey,
								width: width,
								height: height,
								resizable: true,
								maximizable: true,
								inline: 1
							}, {
								setUrl: function (url) {
									cb(url);
								}
							});
						}
					});
					dialogElement.dialog('open');
				} else {
					messageContainer.css("display","block");
					messageContainer.append('Something went wrong.');
				}
			},
			error: function () {
				messageContainer.css("display","block");
				messageContainer.append('Something went wrong.');
			}
		})
	})

	jQuery('.edit_toxin_alert').live('click',function(){
		let id = $(this).data('id');
		let messageContainer = jQuery(".message");
		jQuery.ajax({
			type: 'get',
			url: '/toxin_alerts/edit_toxin_alert/'+id,
			success: function (response) {
				if (response.status === 'success') {
					var dialogElement = jQuery('#toxin_alertsupdatediv');
					dialogElement.html(response.response.html);
					dialogElement.find("input[type=date]").dateinput({
						format: "dd mmmm yyyy"
					});
					tinyMCE.remove("#edit-toxin-alert-content");
					dialogElement.find('#edit-toxin-alert-content').tinymce({
						mode: "none",
						selector: "textarea",
						fontsize_formats: fontsize_list,
						font_formats: "Poppins='Poppins', sans-serif;Open Sans=Open Sans,sans-serif;Andale Mono=andale mono,times;" +
							"Arial=arial,helvetica,sans-serif;" +
							"Arial Black=arial black,avant garde;" +
							"Book Antiqua=book antiqua,palatino;" +
							"Comic Sans MS=comic sans ms,sans-serif;" +
							"Courier New=courier new,courier;" +
							"Georgia=georgia,palatino;" +
							"Helvetica=helvetica;" +
							"Impact=impact,chicago;" +
							"Symbol=symbol;" +
							"Tahoma=tahoma,arial,helvetica,sans-serif;" +
							"Terminal=terminal,monaco;" +
							"Times New Roman=times new roman,times;" +
							"Trebuchet MS=trebuchet ms,geneva;" +
							"Verdana=verdana,geneva;" +
							"Webdings=webdings;" +
							"Wingdings=wingdings,zapf dingbats",
						theme: "modern",
						plugins: [
							"advlist autolink lists link image charmap print preview hr anchor pagebreak",
							"searchreplace wordcount visualblocks visualchars code fullscreen",
							"insertdatetime media nonbreaking save table contextmenu directionality",
							"emoticons template paste textcolor colorpicker textpattern selectfish selectregion filemanager"
						],
						toolbar1: "fontselect |  fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
						toolbar2: "forecolor | selectfish selectregion filemanager",
						image_advtab: true,
						templates: [
							{title: 'Test template 1', content: 'Test 1'},
							{title: 'Test template 2', content: 'Test 2'}
						],
						external_filemanager_path: "/assets/tinymce/plugins/filemanager/",
						filemanager_title: "Responsive Filemanager",
						external_plugins: {"filemanager": "/assets/tinymce/plugins/filemanager/plugin.min.js"},
						file_picker_callback: function (cb, value, meta) {
							var width = window.innerWidth - 30;
							var height = window.innerHeight - 60;
							if (width > 1800) width = 1800;
							if (height > 1200) height = 1200;
							if (width > 600) {
								var width_reduce = (width - 20) % 138;
								width = width - width_reduce + 10;
							}
							var urltype = 2;
							if (meta.filetype == 'image') {
								urltype = 1;
							}
							if (meta.filetype == 'media') {
								urltype = 3;
							}
							var title = "RESPONSIVE FileManager";
							if (typeof this.settings.filemanager_title !== "undefined" && this.settings.filemanager_title) {
								title = this.settings.filemanager_title;
							}
							var akey = "key";
							if (typeof this.settings.filemanager_access_key !== "undefined" && this.settings.filemanager_access_key) {
								akey = this.settings.filemanager_access_key;
							}
							var sort_by = "";
							if (typeof this.settings.filemanager_sort_by !== "undefined" && this.settings.filemanager_sort_by) {
								sort_by = "&sort_by=" + this.settings.filemanager_sort_by;
							}
							var descending = "false";
							if (typeof this.settings.filemanager_descending !== "undefined" && this.settings.filemanager_descending) {
								descending = this.settings.filemanager_descending;
							}
							var fldr = "";
							if (typeof this.settings.filemanager_subfolder !== "undefined" && this.settings.filemanager_subfolder) {
								fldr = "&fldr=" + this.settings.filemanager_subfolder;
							}
							var crossdomain = "";
							if (typeof this.settings.filemanager_crossdomain !== "undefined" && this.settings.filemanager_crossdomain) {
								crossdomain = "&crossdomain=1";
								if (window.addEventListener) {
									window.addEventListener('message', filemanager_onMessage, false);
								} else {
									window.attachEvent('onmessage', filemanager_onMessage);
								}
							}
							tinymce.activeEditor.windowManager.open({
								title: title,
								file: this.settings.external_filemanager_path + 'dialog.php?type=' + urltype + '&descending=' + descending + sort_by + fldr + crossdomain + '&lang=' + this.settings.language + '&akey=' + akey,
								width: width,
								height: height,
								resizable: true,
								maximizable: true,
								inline: 1
							}, {
								setUrl: function (url) {
									cb(url);
								}
							});
						}
					});
					dialogElement.dialog('open');
				} else {
					messageContainer.css("display","block");
					messageContainer.append('Something went wrong.');
				}
			},
			error: function () {
				messageContainer.css("display","block");
				messageContainer.append('Something went wrong.');
			}
		})
	});

	jQuery("#part_of_area_bag_limit").live("click",function(){
		if(jQuery(this).is(":checked")){
		}else{
			jQuery(this).parent("span").removeClass('checked');
		}
	});
	jQuery("#uniform-fish select").live("change",function(){
		jQuery(this).prev('span').html(this.options[this.selectedIndex].innerHTML);
		return false;
	});
	jQuery('input[name="video_checkbox"]').change( function() { file_or_field(); });
	file_or_field();
});
function file_or_field(){
	var value = jQuery('input[name="video_checkbox"]:checked').val();
	jQuery('#video_file_field').css("display",value=="file"?"block":"none");
	jQuery('#video_url_field').css("display",value=="url"?"block":"none");
}

/*-----file attchment using ajax-----*/
jQuery(document).ready(function(e) {
	var options = { 
	  	beforeSubmit:  showRequest,  // pre-submit callback 
	    success:       showResponse  // post-submit callback 
	};
	jQuery('#restrictedimageform').ajaxForm(options);
});
function showRequest(formData, jqForm, options) 
{
	var queryString = jQuery.param(formData);
	return true; 
} 
	 
function showResponse(responseText, statusText, xhr, $form)  
{ 
	if(responseText==3)
	{
		jQuery(".message").css("display","block");
		jQuery('.message').append('Select Region and Fish.');
	}
	if(responseText==0)
	{
		jQuery(".message").css("display","block");
		jQuery('.message').append('Select File.');
	}
	else if(responseText==1)
	{
		jQuery(".message").css("display","block");
		jQuery('.message').append('File Upload Error.');
	}
	else
	{
		jQuery("#nodata").remove();
		jQuery(".message").css("display","none");
		jQuery(".restricted_detail_item").append(responseText);
		jQuery("#restricted_imagediv").dialog("close");
	}
}

function updateRequest(formData, jqForm, options) 
{
	var queryString = jQuery.param(formData);
	return true; 
} 
function updateResponse(responseText, statusText, xhr, $form)  
{ 
	var currentEle =  $form;
	if(responseText==0)
	{
		jQuery(".message").css("display","block");
		jQuery('.message').append('Select File.');
	}
	else if(responseText==1)
	{
		jQuery(".message").css("display","block");
		jQuery('.message').append('File Upload Error.');
	}
	else if(responseText==3)
	{
		jQuery(".message").css("display","block");
		jQuery('.message').append('Image Not Update.');
	}
	else
	{
		jQuery(".message").css("display","none");
		jQuery(currentEle).closest('li').children('.item').html('<span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span>'+responseText);
		jQuery(currentEle).closest('li').children('#restricted_imageupdatediv').css("display","none");
		jQuery(currentEle).closest('li').children('.item').css("display","block");
	}
}
function loadjquerydata(region,fish,response){
	jQuery("#restricted_data").html(response);	
	jQuery('#rest_sort').sortable({
		update: function(event, ui) {
			var restOrder = jQuery(this).sortable('toArray', { attribute: 'id' });
			baseurl='restricted_fish_in_region/restorder/'+region+'/'+fish;
			jQuery.ajax({
				url:baseurl,
				data:{
			        sortables: restOrder
			    },
				type:'POST',
				success:function(response){
					
				}
			});
		}
	});
	
	jQuery(".remove_rest").live("click",function(){
		var currentEle =  jQuery(this);
		var id=this.id;
		baseurl='restricted_fish_in_region/res_delete/'+id;
		jQuery.ajax({
			url:baseurl,
			dataType:'HTML',
			type:'GET',
			success:function(response){
				if(response==1)
				{
					jQuery(".message").css("display","none");
					jQuery(currentEle).closest('li').remove();
					
					if(jQuery("#rest_sort .liitem").length>0)
					{
						
					}
					else
					{
						jQuery("#rest_sort").append('<li id="nodata">No Data Available.</li>');
						return false;
					}
				}
				else{
					jQuery(".message").css("display","block");
					jQuery('.message').text('Restricted Fish in Region Not Deleted.');
				}
			}
		});
		return false;
	});
	jQuery(".edit_rest_text").live("click",function(){
		var currentEle =  jQuery(this);
		//var data = this.id;
		//var arr = data.split('/');
		//id=arr[0];
		//res_msg=arr[1];
			
		jQuery(currentEle).closest('li').children('.item').css("display","none");
		jQuery(currentEle).closest('li').children('#restricted_textupdatediv').css("display","block");
		return false;
	});
	jQuery('button[name="cancelbutton"]').live('click',function(){
		var currentEle =  jQuery(this);
		jQuery(".message").css("display","none");
		jQuery(currentEle).closest('li').children('#restricted_textupdatediv').css("display","none");
		jQuery(currentEle).closest('li').children('#restricted_imageupdatediv').css("display","none");
		jQuery(currentEle).closest('li').children('.item').css("display","block");
	});
	jQuery('form[name="restrictedtextupdateform"]').live('submit', function () 
	{
		var currentEle =  jQuery(this);
		var rst_id = currentEle.find('input[name="restricted_updatemsgid"]').val();
		var restricted_msg = currentEle.find('textarea[name="restricted_updatemsg"]').val();
		var is_bold=0;
		if (currentEle.find('input[name="is_restricted_text_bold"]').is(":checked"))
		{
			is_bold=1;
		}
		
		if(restricted_msg=="")
		{
			jQuery(".message").css("display","block");
			jQuery('.message').append('Insert Text.');
			return false;
		}
		baseurl='restricted_fish_in_region/res_text_update/'+rst_id;
		jQuery.ajax({
			url:baseurl,
			data:{ "msg":restricted_msg,"is_restricted_text_bold":is_bold },
			dataType:'HTML',
			type:'GET',
			success:function(response){
				if(response==1)
				{
					jQuery(".message").css("display","none");
					jQuery(currentEle).closest('li').children('#restricted_textupdatediv').css("display","none");
					if(is_bold==1){
						jQuery(currentEle).closest('li').children('.item').html('<span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span><b>'+restricted_msg.replace(/\n/g, '<br/>')+'</b>');
					}else{
						jQuery(currentEle).closest('li').children('.item').html('<span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span>'+restricted_msg.replace(/\n/g, '<br/>'));
					}
					jQuery(currentEle).closest('li').children('.item').css("display","block");
				}
				else
				{
					jQuery(".message").css("display","block");
					jQuery('.message').append('Restricted Fish in region not updated.');
				}
			}
		});
		//event.preventDefault(); // if you want to disable the action
		return false;
	});
	jQuery(".edit_rest_image").live("click",function(){
		var currentEle =  jQuery(this);
		//jQuery(currentEle).closest('li').children('.item').css("display","none");
		jQuery(currentEle).closest('li').children('#restricted_imageupdatediv').css("display","block");
		return false;
	});
	
	var options = { 
			beforeSubmit:  updateRequest,  // pre-submit callback 
			success:       updateResponse  // post-submit callback 
	};
	jQuery(".restrictedimageupdateform").ajaxForm(options);
}


function loadclosuresjquerydata(region,response){
	jQuery("#restricted_data").html(response);
	
	jQuery(".has-validation").validator({
        position : 'bottom left', 
        offset : [5, 0], 
        messageClass : 'form-error', 
        message : '<div><em/></div>'// em element is the arrow
    }).attr('novalidate', 'novalidate');
}
