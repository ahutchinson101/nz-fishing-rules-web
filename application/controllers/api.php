<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('api_model');
		set_time_limit(0);
		ini_set('memory_limit','-1');
	}
	
	public function index()
	{
		$response=array();
		$api_access=false;
		$last_requested_date=isset($_POST['last_requested_date'])?$_POST['last_requested_date']:'';
		$api_users=isset($_POST['api_users'])?$_POST['api_users']:'';
		$dv=isset($_POST['dv'])?$_POST['dv']:'';
		$ta=isset($_POST['ta'])?$_POST['ta']:'';
		$api_password=isset($_POST['api_password'])?$_POST['api_password']:'';
		if($this->api_model->get_api($api_users,$api_password)==true){
		
			$fish_type = array();
			$fish_in_region = array();
			$fish = array();
			$fish_gallery = array();

			// kludge to exclude fish updates when sending to old iOS due to new-fish-type crash bug (now fixed)
			if($dv>1.9 || (strpos($_SERVER['HTTP_USER_AGENT'],'Darwin') === false)) {
        $fish_type = $this->api_model->get_fish_type($last_requested_date);
        $fish = $this->api_model->get_fish($last_requested_date);
        $fish_gallery = $this->api_model->get_fish_gallery($last_requested_date);
        if(strlen($dv)>0){
          $fish_in_region = $this->api_model->get_fish_in_region($last_requested_date);
        }
        else{
          $fish_in_region = $this->api_model->get_fish_in_region_old_dv($last_requested_date);
        }
			}
			
			if(strlen($dv)>0){
				$alert_in_region = $this->api_model->get_alert_in_region($last_requested_date);
			}
			/*if(strlen($dv)>0){
				$restricted_fish_in_region = $this->api_model->get_restricted_fish_in_region($last_requested_date);
			}*/
			
			$fish_images=$this->api_model->get_fish_images($last_requested_date);
			/*if(strlen($dv)>0){
				$restricted_images=$this->api_model->get_restricted_images($last_requested_date);
			}*/
			
			if(strlen($ta)>0){
				$toxin_alerts_sync = array();
				$toxin_alerts = $this->api_model->get_toxin_alerts($last_requested_date);
				foreach ($toxin_alerts as $toxin_alert)
				{
					if($toxin_alert->content!=''){
						$doc = new DOMDocument();
						$doc->loadHTML('<?xml encoding="utf-8" ?>'.$toxin_alert->content);
						$tags = $doc->getElementsByTagName('img');
					}else{
						$tags=array();
					}

					foreach ($tags as $tag) {
						$imgsrc=$tag->getAttribute('src');
						$img_array=explode('/',$imgsrc);
						$img_url=$img_array[count($img_array)-1];
						$tag->setAttribute("src",$img_url);
						$tag->removeAttribute('width');
						$tag->removeAttribute('height');
						$tag->setAttribute("style","max-width:100%");
					}
                    if($toxin_alert->content!='') {
                        $html = $doc->saveHTML();
                        $toxin_alert->content = $html;
                    }
					if($toxin_alert->date_added){
						try{
							$toxin_alert->title = "Added On ".date('d/m',strtotime($toxin_alert->date_added)).' - '.$toxin_alert->title;
						}catch (\Exception $e){

						}
					}
					$toxin_alerts_sync[] = $toxin_alert;
				}
			}

			if(strlen($ta)>0){
				$toxin_alert_images=$this->api_model->get_toxin_alerts_images($last_requested_date);
			}

			if(strlen($ta)>0){
				$deleted_toxin_alerts=$this->api_model->get_deleted_toxin_alerts($last_requested_date);
			}
			if(strlen($ta)>0){
				$deleted_toxin_alerts_images=$this->api_model->get_deleted_toxin_alerts_images($last_requested_date);
			}

			if(strlen($dv)>0){
				$closures_and_restrictions_sync = array();
				$closures_and_restrictions = $this->api_model->get_closures_and_restrictions($last_requested_date);
				foreach ($closures_and_restrictions as $cs)
				{
					if($cs->content!=''){
						$doc = new DOMDocument();
						$doc->loadHTML('<?xml encoding="utf-8" ?>'.$cs->content);
						$tags = $doc->getElementsByTagName('img');
					}else{
						$tags=array();
					}
					
					foreach ($tags as $tag) {
						$imgsrc=$tag->getAttribute('src');
						$img_array=explode('/',$imgsrc);
						$img_url=$img_array[count($img_array)-1];
						$tag->setAttribute("src",$img_url);
						$tag->removeAttribute('width');
						$tag->removeAttribute('height');
						$tag->setAttribute("style","max-width:100%");
					}
					$html=$doc->saveHTML();
					$cs->content = $html;
					/*if($cs->date_added){
						try{
							$cs->title = "Added On ".date('d/m',strtotime($cs->date_added)).' - '.$cs->title;
						}catch (\Exception $e){

						}
					}*/
					$closures_and_restrictions_sync[] = $cs;
				}
			}
			
			if(strlen($dv)>0){
				$closures_images=$this->api_model->get_closures_images($last_requested_date);
			}
			
			$deleted_fish_type=$this->api_model->get_deleted_fish_type($last_requested_date);
			$deleted_fish=$this->api_model->get_deleted_fish($last_requested_date);
			$deleted_fish_gallery=$this->api_model->get_deleted_fish_gallery($last_requested_date);
			$deleted_fish_in_region=$this->api_model->get_deleted_fish_in_region($last_requested_date);
			/*if(strlen($dv)>0){
				$deleted_restricted_fish_in_region=$this->api_model->get_deleted_restricted_fish_in_region($last_requested_date);
			}*/
			if(strlen($dv)>0){
				$deleted_closures_and_restrictions=$this->api_model->get_deleted_closures_and_restrictions($last_requested_date);
			}
			if(strlen($dv)>0){
				$deleted_closures_images=$this->api_model->get_deleted_closures_images($last_requested_date);
			}
			//$deleted_alert_in_region=$this->api_model->get_deleted_alert_in_region($last_requested_date);
			$response['Result']='Ok'; 
			$response['last_response_date']=date('Y-m-d H:i:s');
			if(count($fish_type)>0){
				$response['fish_type']=$fish_type;
			}
			if(count($fish)>0){
				$response['fish']=$fish;
			}
            if(count($fish_gallery)>0){
				$response['fish_gallery'] = $fish_gallery;
			}
			if(count($fish_in_region)>0){
				$response['fish_in_region']=$fish_in_region;
			}
			/*if(strlen($dv)>0){
				if(count($restricted_fish_in_region)>0){
					$response['restricted_fish_in_region']=$restricted_fish_in_region;
				}
			}*/
			if(strlen($dv)>0){
				if(count($closures_and_restrictions_sync)>0){
					$response['closures_and_restrictions']=$closures_and_restrictions_sync;
				}
			}
			if(strlen($ta)>0){
				if(count($toxin_alerts_sync)>0){
					$response['toxin_alerts']=$toxin_alerts_sync;
				}
			}
			if(strlen($dv)>0){
				if(count($alert_in_region)>0){
					$response['alert_in_region']=$alert_in_region;
				}
			}
			if(count($deleted_fish_type)>0){
				$deleted_fish_type1=array();
				foreach ($deleted_fish_type as $deleted_fish_type){
					$deleted_fish_type1[] = $deleted_fish_type->fish_type_id;
				}
				$response['deleted_fish_type']=$deleted_fish_type1;
			}
			if(count($deleted_fish)>0){
				$deleted_fish1=array();
				foreach ($deleted_fish as $deleted_fish){
					$deleted_fish1[]=$deleted_fish->fish_id;
				}
				$response['deleted_fish']=$deleted_fish1;
			}
            if(count($deleted_fish_gallery)>0){
                $deleted_fish_gallery1=array();
				foreach ($deleted_fish_gallery as $deleted_fish_gallery_item){
                    $deleted_fish_gallery1[]=$deleted_fish_gallery_item->fish_image_id;
				}
				$response['deleted_fish_gallery']=$deleted_fish_gallery1;
			}
			if(count($deleted_fish_in_region)>0){
				$deleted_fish_in_region1=array();
				foreach ($deleted_fish_in_region as $deleted_fish_in_region){
					$deleted_fish_in_region1[]=$deleted_fish_in_region->fish_in_region_id;
				}
				$response['deleted_fish_in_region']=$deleted_fish_in_region1;
			}
			/*if(strlen($dv)>0){
				if(count($deleted_restricted_fish_in_region)>0){
					$deleted_restricted_fish_in_region1=array();
					foreach ($deleted_restricted_fish_in_region as $deleted_restricted_fish_in_region){
						$deleted_restricted_fish_in_region1[]=$deleted_restricted_fish_in_region->restricted_fish_in_region_id;
					}
					$response['deleted_restricted_fish_in_region']=$deleted_restricted_fish_in_region1;
				}
			}*/
			if(strlen($dv)>0){
				if(count($deleted_closures_and_restrictions)>0){
					$deleted_closures_and_restrictions1=array();
					foreach ($deleted_closures_and_restrictions as $deleted_closures_and_restrictions){
						$deleted_closures_and_restrictions1[]=$deleted_closures_and_restrictions->closures_and_restrictions_id;
					}
					$response['deleted_closures_and_restrictions']=$deleted_closures_and_restrictions1;
				}
			}
			if(strlen($dv)>0){
				if(count($deleted_closures_images)>0){
					$deleted_closures_images1=array();
					foreach ($deleted_closures_images as $deleted_closures_images){
						$deleted_closures_images1[]=$deleted_closures_images->deleted_closures_image;
					}
					$response['deleted_closures_images']=$deleted_closures_images1;
				}
			}

			if(strlen($ta)>0){
				if(count($deleted_toxin_alerts)>0){
					$deleted_toxin_alerts1=array();
					foreach ($deleted_toxin_alerts as $deleted_toxin_alerts_1){
						$deleted_toxin_alerts1[]=$deleted_toxin_alerts_1->toxin_alert_id;
					}
					$response['deleted_toxin_alerts']=$deleted_toxin_alerts1;
				}
			}
			if(strlen($ta)>0){
				if(count($deleted_toxin_alerts_images)>0){
					$deleted_toxin_alerts_images1=array();
					foreach ($deleted_toxin_alerts_images as $deleted_toxin_alerts_images_1){
						$deleted_toxin_alerts_images1[]=$deleted_toxin_alerts_images_1->deleted_toxin_alerts_image;
					}
					$response['deleted_toxin_alerts_images']=$deleted_toxin_alerts_images1;
				}
			}
			
			/*if(count($deleted_alert_in_region)>0){
				$deleted_alert_in_region1=array();
				foreach ($deleted_alert_in_region as $deleted_alert_in_region){
					$deleted_alert_in_region1[]=$deleted_alert_in_region->alert_in_region_id;
				}
				$response['deleted_alert_in_region']=$deleted_alert_in_region1;
			}*/
			
			$files_to_exclude_from_cloude_sync = array();
			foreach ($fish_images as $fishimage){
				if($fishimage->fish_image!=""  && is_file(FCPATH.'assets/fish/'.$fishimage->fish_image)){
					$files_to_exclude_from_cloude_sync[] = $fishimage->fish_image;
				}
			}

            foreach ($fish_gallery as $fish_gallery_item){
				if($fish_gallery_item->fish_image!=""  && is_file(FCPATH.'assets/fish/'.$fish_gallery_item->fish_image)){
					$files_to_exclude_from_cloude_sync[] = $fish_gallery_item->fish_image;
				}
			}
			if(count($fish_type)>0){
				foreach ($fish_type as $fishtype){
					if($fishtype->video_file!=""  && is_file(FCPATH.'assets/fish_type/'.$fishtype->video_file)){
						$files_to_exclude_from_cloude_sync[] = $fishtype->video_file;
					}
				}
			}
			
			if(strlen($dv)>0){
				foreach ($closures_images as $closuresimages){
					if($closuresimages->image_url!=""  && is_file(FCPATH.'assets/closure_images/'.$closuresimages->image_url)){
						$files_to_exclude_from_cloude_sync[] = 'closure_images/'.$closuresimages->image_url;
					}
				}
			}

			if(strlen($ta)>0){
				foreach ($toxin_alert_images as $toxin_alert_image){
					if($toxin_alert_image->image_url!=""  && is_file(FCPATH.'assets/closure_images/'.$toxin_alert_image->image_url)){
						$files_to_exclude_from_cloude_sync[] = 'toxin_alerts_images/'.$toxin_alert_image->image_url;
					}
				}
			}
			
			if(count($files_to_exclude_from_cloude_sync)>0){
				$response['files_to_exclude_from_cloud']=$files_to_exclude_from_cloude_sync;
			}
			
			$filename = 'api_exports/'.time().".txt";
			$string = json_encode($response);
			$fp = fopen(FCPATH.$filename, "a");
	        fwrite($fp, $string);
			fclose($fp);
			$this->load->helper('createzipfile');
	        $createzipfile = new createzipfile_helper();
	        $createzipfile->addFile(file_get_contents(FCPATH.$filename), "response.txt");



            $duplicate = [];
	        foreach ($fish_images as $fishimage){
	        	if($fishimage->fish_image!=""  && is_file(FCPATH.'assets/fish/'.$fishimage->fish_image)){
	        		$path=FCPATH.'assets/fish/'.$fishimage->fish_image;
                    if(!in_array($fishimage->fish_image,$duplicate)) {
                        $createzipfile->addFile(file_get_contents($path), $fishimage->fish_image);
                        $duplicate[] = $fishimage->fish_image;
                    }
	        	}
	        }

            $duplicate = [];
            foreach ($fish_gallery as $fish_gallery_item){
                if($fish_gallery_item->fish_image!=""  && is_file(FCPATH.'assets/fish/'.$fish_gallery_item->fish_image)){
                    $path=FCPATH.'assets/fish/'.$fish_gallery_item->fish_image;
                    if(!in_array($fish_gallery_item->fish_image,$duplicate)) {
                        $createzipfile->addFile(file_get_contents($path), $fish_gallery_item->fish_image);
                        $duplicate[] = $fish_gallery_item->fish_image;
                    }
                }
            }

            $duplicate = [];
	        if(count($fish_type)>0){
		        foreach ($fish_type as $fishtype){
		        	if($fishtype->video_file!=""  && is_file(FCPATH.'assets/fish_type/'.$fishtype->video_file)){
		        		$video_path=FCPATH.'assets/fish_type/'.$fishtype->video_file;
                        if(!in_array($fishtype->video_file,$duplicate)) {
                            $createzipfile->addFile(file_get_contents($video_path), $fishtype->video_file);
                            $duplicate[] = $fishtype->video_file;
                        }
		        	}
		        }
	        }
	        /*if(strlen($dv)>0){
		        foreach ($restricted_images as $restrictedimages){
		        	if($restrictedimages->restricted_image!=""){
		        		$path=FCPATH.'assets/rest/'.$restrictedimages->restricted_image;
		        		$createzipfile->addFile(file_get_contents($path), $restrictedimages->restricted_image);
		        	}
		        }
	        }*/
            $duplicate = [];
	        if(strlen($dv)>0){
	        	foreach ($closures_images as $closuresimages){
	        		if($closuresimages->image_url!=""  && is_file(FCPATH.'assets/closure_images/'.$closuresimages->image_url)){
	        			$path=FCPATH.'assets/closure_images/'.$closuresimages->image_url;
                        if(!in_array('closure_images/'.$closuresimages->image_url,$duplicate)) {
                            $createzipfile->addFile(file_get_contents($path), 'closure_images/'.$closuresimages->image_url);
                            $duplicate[] = 'closure_images/'.$closuresimages->image_url;
                        }
	        		}
	        	}
	        }

            $duplicate = [];
			if(strlen($ta)>0){
				foreach ($toxin_alert_images as $toxin_alert_image){
					if($toxin_alert_image->image_url!=""  && is_file(FCPATH.'assets/closure_images/'.$toxin_alert_image->image_url)){
						$path=FCPATH.'assets/closure_images/'.$toxin_alert_image->image_url;
                        if(!in_array('toxin_alerts_images/'.$toxin_alert_image->image_url,$duplicate)) {
                            $createzipfile->addFile(file_get_contents($path), 'toxin_alerts_images/'.$toxin_alert_image->image_url);
                            $duplicate[] = 'toxin_alerts_images/'.$toxin_alert_image->image_url;
                        }
					}
				}
			}
	        
	        $rand = rand(0, 9999);
		    $zipName = 'api_exports/'.md5(time().$rand).".zip";
	        $fd=fopen($zipName, "wb");
	        $out=fwrite($fd,$createzipfile->getZippedfile());
	        fclose($fd);
	        $createzipfile->forceDownload($zipName);
	        @unlink($zipName);
	        @unlink(FCPATH.$filename);
	        exit;
		}else{
			echo "Error: API Username and password is invalid";
		}
	}
	
	function exportMasterFish(){
		$filename="api_exports/FishMaster.csv";
		$handler=fopen(FCPATH.$filename, "a+");
		fputcsv($handler, array('Keyname','Keyword','Common Alternative Names','Fish Type','Maori name','Scientific Name','National Size','National Daily','Accumulation','Fish image'));
		$fishMastersData=$this->db->get('fish_master')->result();
		foreach($fishMastersData as $fish){
            $fishForType=$this->db->get_where('fish_type',array('id'=>$fish->fish_type_id))->row();
            $fishType = $fishForType ? $fishForType->fish_type_name : "";
			fputcsv($handler, array($fish->keyname,$fish->keyword,$fish->common_alt_name,$fishType,$fish->mori_name,$fish->scientific_name,$fish->national_size,$fish->national_daily,$fish->accumulation,$fish->fish_image));
				
		}
		fclose($handler);
		$this->load->helper('createzipfile');
		$createzipfile = new createzipfile_helper();
		$createzipfile->addFile(file_get_contents(FCPATH.$filename),"FishMaster.csv");
		$rand = rand(0, 9999);
		$zipName = md5(time().$rand).".zip";
		$fd=fopen($zipName, "wb");
		$out=fwrite($fd,$createzipfile->getZippedfile());
		fclose($fd);
		$createzipfile->forceDownload($zipName);
		@unlink($zipName);
		@unlink(FCPATH.$filename);
		exit;
	}
	
	function exportRegionFish(){
		$regions=$this->db->get('region')->result();
		$this->load->helper('createzipfile');
		$createzipfile = new createzipfile_helper();
		foreach($regions as $region){
			$filename='api_exports/'.$region->name.".csv";
			$FishInRegionQuery=$this->db->query("SELECT fish_in_region.* FROM fish_in_region join fish_master ON fish_in_region.fish_id=fish_master.id WHERE fish_in_region.region_id=".$region->id." ORDER BY fish_in_region.fish_id ASC");
			$handler=fopen(FCPATH.$filename, "a+");
			fputcsv($handler, array('Keyword',"Size","Daily","Bag",'Part Of Area Bag Limit'));
			if($FishInRegionQuery->num_rows()>0){
				foreach($FishInRegionQuery->result() as $FishInRegion){
					$fils=$this->db->get_where('fish_master',array('id'=>$FishInRegion->fish_id))->row();
                    $filsKeyword = $fils ? $fils->keyword : '';
					fputcsv($handler, array($filsKeyword,$FishInRegion->size,$FishInRegion->daily,$FishInRegion->bag,$FishInRegion->part_of_area_bag_limit));	
				}
			}
			fclose($handler);
			$createzipfile->addFile(file_get_contents(FCPATH.$filename),$region->name.".csv");
            @unlink(FCPATH.$filename);
		}
		$rand = rand(0, 9999);
		$zipName = 'api_exports/'.md5(time().$rand).".zip";
		$fd=fopen($zipName, "wb");
		$out=fwrite($fd,$createzipfile->getZippedfile());
		fclose($fd);
		$createzipfile->forceDownload($zipName);
		@unlink($zipName);
		exit;
	}

    public function exportAllImage()   // menu button get all images
    {

        try {

            $dir_name = FCPATH.'assets/closure_images/';
            $images = glob($dir_name.'*');

            $duplicate = [];

            $this->load->helper('createzipfile');

            $createzipfile = new createzipfile_helper();
            foreach($images as $image) {
                $img_name=explode('assets/closure_images/',$image)[1];

                if(is_file(FCPATH.'assets/closure_images/'.$img_name)){
                    $path=FCPATH.'assets/closure_images/'.$img_name;
                    if(!in_array($img_name,$duplicate)) {
                        $createzipfile->addFile(file_get_contents($path), $img_name);
                        $duplicate[] = $img_name;
                    }
                }

            }
            $rand = rand(0, 9999);
            $zipName = 'api_exports/'.md5(time().$rand).".zip";
            $fd=fopen($zipName, "wb");
            $out=fwrite($fd,$createzipfile->getZippedfile());
            fclose($fd);
            $createzipfile->forceDownload($zipName);
            @unlink($zipName);

            redirect('fish');

        } catch (Exception $exception) {

            $this->session->set_flashdata('error-message', 'Something went wrong.');

            redirect('fish');

        }
    }

}
