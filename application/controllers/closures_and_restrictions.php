<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Closures_And_Restrictions extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('closures_and_restrictions_model', 'restricted_area');
    }

    //fish in region page
    public function index($id = 1, $row = 0)
    {
        $header_data['title'] = 'NZFishing';
        $footer_data['devloped_by'] = "Tech Xperts";
        $region_array = array();
        $region_array[''] = 'Select Region';
        $region_name = $this->restricted_area->get_region();
        foreach ($region_name as $region) {
            $region_array[$region->id] = $region->name;
        }
        $data['region'] = $region_array;
        $this->load->view('header', $header_data);
        $this->load->view('navigation');
        $this->load->view('closures_and_restrictions', $data);
        $this->load->view('footer', $footer_data);
    }

    // Edit fish in region
    public function edit($id = 0)
    {
        $this->form_validation->set_rules('part_of_area_bag_limit', '', 'callback_bag_check');
        if ($this->form_validation->run() == FALSE) {
            $header_data['title'] = 'NZFishing';
            $footer_data['devloped_by'] = "Tech Xperts";
            $fish_in_region = $this->region_fish->get_one_fish_in_region($id);
            $fish = $this->region_fish->get_fish_detail($fish_in_region->fish_id);
            $region = $this->region_fish->get_region($fish_in_region->region_id);
            $data['fish'] = $fish;
            $data['region'] = $region;
            $data['fish_in_region'] = $fish_in_region;
            $this->load->view('header', $header_data);
            $this->load->view('navigation');
            $this->load->view('fish_in_region_edit', $data);
            $this->load->view('footer', $footer_data);
        } else {
            if ($this->region_fish->fish_in_region_update() == true) {
                $this->session->set_flashdata('flash-message', 'Fish detail is updated');
            } else {
                $this->session->set_flashdata('error-message', 'Fish detail Not update please try again..!');
            }
            redirect('fish_in_region/edit/' . $id);
        }
    }

    public function bag_check($part)
    {
        if ($part == 1 && ($this->input->post('bag') == "" || $this->input->post('bag') == 0)) {
            $this->form_validation->set_message('bag_check', 'Please enter bag limit');
            return false;

        } else {
            return true;
        }

    }

    // delete fish in region
    function delete($id = 0)
    {
        if ($this->region_fish->delete_fish_in_region($id) == true) {
            $this->session->set_flashdata('flash-message', 'Fish detail deleted successfully');
            redirect('fish_in_region');
        } else {
            $this->session->set_flashdata('error-message', 'Fish detail not deleted');
            redirect('fish_in_region');
        }
    }

    // add fish in region
    public function add_image()
    {
        //$region=$this->uri->segment(3);
        //$fish=$this->uri->segment(4);
        $cdate = date('Y-m-d H:i:s');
        $region = $this->input->post('region');
        $fishid = $this->input->post('fishid');
        if ($region == "" || $fishid == "") {
            $msg = '3';
            echo $msg;
            exit;
        }
        if (!isset($_FILES["restricted_image"]["name"])) {
            $msg = '0';
            echo $msg;
            exit;
        }


        $config['upload_path'] = 'assets/rest/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1536';
        $config['max_width'] = '1600';
        $config['max_height'] = '0';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('restricted_image') && $_FILES['restricted_image']['name'] != "") {
            $msg = '1';
            echo $msg;
            exit;
            return FALSE;
        } else {
            $data = $this->upload->data();
            $image_small = $data['file_name'];
            if ($data['image_width'] < 1600 && $data['image_height'] < 1600) {
            } else {
                /*$config=array();  //for resize image comment 26 7 2014
                $config['image_library'] = 'gd2';
                $config['source_image']	= FCPATH.'assets/rest/'.$data['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['create_thumb']=FALSE;
                $config['width'] = 510;
                $config['height'] = 240;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $this->image_lib->clear();*/
            }
        }

        if ($lid = $this->restricted_area->add_restricted_fish_in_region_img($region, $fishid, $image_small, $cdate)) {
            $res = '<li id="' . $lid . '" class="liitem">';
            $res .= '<span style="float: right;">';
            $res .= '<a href="#" style="float: right;" class="remove_rest" id="' . $lid . '"><img alt="Remove" src="' . ADMIN_URL . 'assets/images/action4.gif"></a>';
            $res .= '<a href="#" style="float: right; padding-right: 6px;" class="edit_rest_image" id="' . $lid . '"><img alt="Edit" src="' . ADMIN_URL . 'assets/images/edit_action.gif"></a>';
            $res .= '</span>';
            $res .= '<span class="item" style="display:block;"><span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span><img src="' . ADMIN_URL . '/assets/rest/' . $image_small . '"></span>';
            $res .= '<div class="clearfix" id="restricted_imageupdatediv" style="display:none;">';
            $res .= '<form method="post" action="' . ADMIN_URL . 'restricted_fish_in_region/update_image" name="restrictedimageupdateform"  id="restrictedimageupdateform_' . $lid . '"  class="restrictedimageupdateform form has-validation" enctype="multipart/form-data">';
            $res .= '<input type="hidden" value="' . $lid . '" name="restricted_updateimageid" id="restricted_updateimageid" />';
            $res .= '<input type="hidden" value="' . $image_small . '" name="oldimage" id="oldimage" />';
            $res .= '<input type="file" name="restricted_updateimage" value=""><br/>';
            $res .= '<small><em>Allowed maximmum size 2 MB</em></small><br/>';
            $res .= '<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Upload" data-icon-primary="ui-icon-circle-check">Upload</button>';
            $res .= '<button class="button" type="button" name="cancelbutton" id="cancelbutton" value="Save" data-icon-primary="ui-icon-circle-check">Cancel</button>';
            $res .= '</form>';
            $res .= '</div>';
            echo $res;
            exit;
        } else {
            echo "1";
            exit;
        }
    }

    public function update_image()
    {
        $id = $this->input->post('restricted_updateimageid');
        $oldimage = $this->input->post('oldimage');
        if (!isset($_FILES["restricted_updateimage"]["name"])) {
            $msg = '0';
            echo $msg;
            exit;
        }

        $config['upload_path'] = 'assets/rest/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1536';
        $config['max_width'] = '1600';
        $config['max_height'] = '0';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('restricted_updateimage') && $_FILES['restricted_updateimage']['name'] != "") {
            $msg = '1';
            echo $msg;
            exit;
        } else {
            $data = $this->upload->data();
            $image_small = $data['file_name'];
            if ($data['image_width'] < 1600 && $data['image_height'] < 1600) {
            } else {
                /*$config=array(); //for resize image 26 7 2014
                $config['image_library'] = 'gd2';
                $config['source_image']	= FCPATH.'assets/rest/'.$data['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['create_thumb']=FALSE;
                $config['width'] = 510;
                $config['height'] = 240;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $this->image_lib->clear();*/
            }
        }
        if ($this->restricted_area->update_restricted_fish_in_region_img($id, $image_small, $oldimage) == true) {
            $res = '<img src="' . ADMIN_URL . '/assets/rest/' . $image_small . '">';
            echo $res;
            exit;
        } else {
            echo "3";
            exit;
        }
    }

    public function check_file($str)
    {

    }

    public function restorder()
    {
        $region = $this->uri->segment(3);
        $fishid = $this->uri->segment(4);
        $sortables = $_POST['sortables'];
        $size = count($sortables);
        for ($i = 0; $i < $size; $i++) {
            $this->restricted_area->update_restricted_fish_in_region_ordering($i, $sortables[$i]);
        }
        exit;
    }

    public function res_delete($id = 0)
    {
        if ($this->restricted_area->restricted_fish_in_region_delete($id) == true) {
            echo "1";
            exit;
        } else {
            echo "0";
            exit;
        }
    }

    public function add_text()
    {
        $region = $this->uri->segment(3);
        $fish = $this->uri->segment(4);
        $cdate = date('Y-m-d H:i:s');
        $restricted_msg = $this->input->get("msg");
        $is_restricted_text_bold = $this->input->get("is_restricted_text_bold");
        if ($lid = $this->restricted_area->add_restricted_fish_in_region($region, $fish, $restricted_msg, $is_restricted_text_bold, $cdate)) {
            $res = '<li id="' . $lid . '" class="liitem">';
            $res .= '<span style="float: right;">';
            $res .= '<a href="#" style="float: right;" class="remove_rest" id="' . $lid . '"><img alt="Remove" src="' . ADMIN_URL . 'assets/images/action4.gif"></a>';
            $res .= '<a href="#" style="float: right; padding-right: 6px;" class="edit_rest_text"><img alt="Edit" src="' . ADMIN_URL . 'assets/images/edit_action.gif"></a>';
            $res .= '</span>';
            if ($is_restricted_text_bold == 1) {
                $res .= '<spam class="item" style="display:block;"><span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span><b>' . nl2br($restricted_msg) . '</b></spam>';
            } else {
                $res .= '<spam class="item" style="display:block;"><span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span>' . nl2br($restricted_msg) . '</spam>';
            }
            $res .= '<div class="clearfix" id="restricted_textupdatediv" style="display:none;">';
            $res .= '<form method="post" action="#" name="restrictedtextupdateform"  id="restrictedtextupdateform"  class="form has-validation" enctype="multipart/form-data">';
            $res .= '<input type="hidden" value="' . $lid . '" name="restricted_updatemsgid" id="restricted_updatemsgid" />';
            $res .= '<textarea name="restricted_updatemsg" id="restricted_updatemsg" cols="40" rows="3">' . $restricted_msg . '</textarea><br/>';
            if ($is_restricted_text_bold == 1) {
                $res .= '<input type="checkbox" name="is_restricted_text_bold" id="is_restricted_text_bold" checked="checked"> : Is bold<br/>';
            } else {
                $res .= '<input type="checkbox" name="is_restricted_text_bold" id="is_restricted_text_bold"> : Is bold<br/>';
            }
            $res .= '<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Save" data-icon-primary="ui-icon-circle-check">Save</button>';
            $res .= '<button class="button" type="button" name="cancelbutton" id="cancelbutton" value="Save" data-icon-primary="ui-icon-circle-check">Cancel</button>';
            $res .= '</form>';
            $res .= '</div>';
            echo $res;
            exit;
        } else {
            echo "1";
            exit;
        }
    }

    public function res_text_update()
    {
        $id = $this->uri->segment(3);
        $restricted_msg = $this->input->get("msg");
        $is_restricted_text_bold = $this->input->get("is_restricted_text_bold");
        if ($this->restricted_area->update_restricted_fish_in_region($id, $restricted_msg, $is_restricted_text_bold) == true) {
            echo "1";
            exit;
        } else {
            echo "0";
            exit;
        }
    }

    public function restricted_detail()
    {
        $region = $this->uri->segment(3);
        $fishid = $this->uri->segment(4);
        $data['rest'] = $this->restricted_area->get_restricted_fish_detail($region, $fishid);
        $this->load->view('res_detail', $data);
    }

    // check fish in region
    public function check_fish($id = 0)
    {
        $fish_in_region = $this->restricted_area->res_fish_in_region($id);
        if (count($fish_in_region) > 0) {
            $fish = array();
            $fish[''] = "Select Fish";
            foreach ($fish_in_region as $fish_in_region) {
                $fish[$fish_in_region->fish_id] = $fish_in_region->keyword;
            }
            $data['fish'] = $fish;
            $data['regionid'] = $id;
            $this->load->view('closures_fish_combo', $data);
        } else {
            $this->load->view('fish_combo_error');
        }
    }

    //fish detail
    public function fish_detail()
    {
        $region = $this->uri->segment(3);
        $fishid = $this->uri->segment(4);
        $fish = $this->restricted_area->get_res_fish_detail($fishid);
        if (count($fish) > 0) {
            $data['fish'] = $fish;
            $this->load->view('fish_detail', $data);
        }
    }

    /*6 9 2014*/
    public function add_closures()
    {
        $region = $this->uri->segment(3);
        $cdate = date('Y-m-d H:i:s');
        $areas = $this->input->post("areas");
        $title = $this->input->post("title");
        $date_added = $this->input->post("date_added");
        if ($date_added) {
            $date_added = date('Y-m-d', strtotime($this->input->post("date_added")));
        } else {
            $date_added = null;
        }
        $key_points = $this->input->post("key_points");
        $content = $this->input->post("content");

        if ($lid = $this->restricted_area->add_closures_restrictions($region, $areas, $title, $date_added, $key_points, $content, $cdate)) {
            $res = '<li id="' . $lid . '" class="liitem">';
            $res .= '<span style="float: right;">';
            $res .= '<a href="#" style="float: right;" class="remove_closure" id="' . $lid . '"><img alt="Remove" src="' . ADMIN_URL . 'assets/images/action4.gif"></a>';
            $res .= '<a href="#" style="float: right; padding-right: 6px;" class="edit_closure"><img alt="Edit" src="' . ADMIN_URL . 'assets/images/edit_action.gif"></a>';
            $res .= '</span>';
            $res .= '<spam class="item" style="display:block;"><span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span>' . nl2br($title . ' ' . $areas) . '</spam>';
            $res .= '<div class="clearfix" id="closures_restrictionsupdatediv" style="display:none;">';
            $res .= '<form method="post" action="#" name="closures_restrictionsupdateform"  id="closures_restrictionsupdateform"  class="form has-validation" enctype="multipart/form-data">';
            $res .= '<input type="hidden" value="' . $lid . '" name="closure_updateid" id="closure_updateid" />';
            $res .= '<table>';
            $res .= '<tr style="display: none;">';
            $res .= '<td style="vertical-align:middle;"><label for="areas" class="form-label">Areas :</label></td>';
            $res .= '<td><input type="text" name="areas" id="areas" class="areas" style="width:100%;" value="' . $areas . '"/></td>';
            $res .= '</tr>';
            $res .= '<tr>';
            $res .= '<td style="vertical-align: middle;"><label for="title" class="form-label">Title :</label></td>';
            $res .= '<td><input type="text" name="title" id="title" style="width:100%;" value="' . $title . '"/></td>';
            $res .= '</tr>';
            $res .= '<tr style="display: none;">';
            $res .= '<td style="vertical-align: middle;"><label for="title" class="form-label">Key points :</label></td>';
            $res .= '<td><input type="text" name="key_points" id="key_points" style="width:100%;" value="' . $key_points . '"/></td>';
            $res .= '</tr>';
            $res .= '<tr>';
            $res .= '<td style="vertical-align: middle;"><label for="restricted_msg" class="form-label">Description :</label></td>';
            $res .= '<td>';
            $res .= '<textarea name="content" id="content_' . $lid . '" class="closure_content" style="width: 100%; height: 200px">' . $content . '</textarea>';
            $res .= '</td>';
            $res .= '</tr>';
            $res .= '<tr>';
            $res .= '<td>';
            $res .= '</td>';
            $res .= '<td>';
            $res .= '<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Save" data-icon-primary="ui-icon-circle-check">Save</button>';
            $res .= '<button class="button" type="button" name="cancelbutton" id="cancelbutton" value="Save" data-icon-primary="ui-icon-circle-check">Cancel</button>';
            $res .= '</td>';
            $res .= '</tr>';
            $res .= '</table>';
            $res .= '</form>';
            $res .= '</div>';
            echo $res;
            exit;
        } else {
            echo "1";
            exit;
        }
    }

    public function closures_detail()
    {
        $region = $this->uri->segment(3);
        $data['closuresdata'] = $this->restricted_area->get_region_closures_detail($region);
        $this->load->view('closures_detail', $data);
    }

    public function closure_combo_list()
    {
        $closure_array = array();
        $closure_array[''] = 'Select Closure';
        $closures_name = $this->restricted_area->get_clouser();
        foreach ($closures_name as $closure) {
            //$closure_array[$closure->id]=$closure->type.' '.$closure->title;
            $closure_array[$closure->id] = $closure->title;
        }
        $data['closure'] = $closure_array;
        $this->load->view('closures_region_combo', $data);
    }

    public function closuresrestorder()
    {
        $region = $this->uri->segment(3);
        $sortables = $_POST['sortables'];
        $size = count($sortables);
        for ($i = 0; $i < $size; $i++) {
            $this->restricted_area->update_closures_ordering($i, $sortables[$i]);
        }
        exit;
    }

    public function closure_delete($id = 0)
    {
        if ($this->restricted_area->closure_in_region_delete($id) == true) {
            echo "1";
            exit;
        } else {
            echo "0";
            exit;
        }
    }

    public function closure_update()
    {
        $id = $this->uri->segment(3);
        $areas = $this->input->post("areas");
        $title = $this->input->post("title");
        $date_added = $this->input->post("date_added");
        if ($date_added) {
            $date_added = date('Y-m-d', strtotime($this->input->post("date_added")));
        } else {
            $date_added = null;
        }
        $key_points = $this->input->post("key_points");
        $content = $this->input->post("content");
        $data['closuresdata'] = $this->restricted_area->get_single_closures_detail($id);
        $oldcontent = $data['closuresdata']->content;
        if ($this->restricted_area->update_closure_restriction($id, $areas, $title, $date_added, $key_points, $content, $oldcontent) == true) {
            echo "1";
            exit;
        } else {
            echo "0";
            exit;
        }
    }

    public function closures_imageurl($id = 0)
    {
        $closures_images = $this->restricted_area->get_images_region($id);
        $data['closures_images'] = $closures_images;
        $data['regionid'] = $id;
        $this->load->view('closures_image_list', $data);

    }

    public function add_closure_image()
    {
        //$region=$this->uri->segment(3);
        //$fish=$this->uri->segment(4);
        $cdate = date('Y-m-d H:i:s');
        $region = $this->input->post('region');
        if ($region == "") {
            $msg = '3';
            echo $msg;
            exit;
        }
        if (!isset($_FILES["closure_image"]["name"])) {
            $msg = '0';
            echo $msg;
            exit;
        }

        $config['upload_path'] = 'assets/closures/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '1536';
        $config['max_width'] = '1600';
        $config['max_height'] = '0';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('closure_image') && $_FILES['closure_image']['name'] != "") {
            $msg = '1';
            echo $msg;
            exit;
            return FALSE;
        } else {
            $data = $this->upload->data();
            $image_small = $data['file_name'];
        }

        if ($lid = $this->restricted_area->add_restricted_fish_in_region_img($region, $image_small, $cdate)) {
            $res = '<li id="' . $lid . '" class="liitem">';
            $res .= '<span style="float: right;">';
            $res .= '<a href="#" style="float: right;" class="remove_rest" id="' . $lid . '"><img alt="Remove" src="' . ADMIN_URL . 'assets/images/action4.gif"></a>';
            $res .= '<a href="#" style="float: right; padding-right: 6px;" class="edit_rest_image" id="' . $lid . '"><img alt="Edit" src="' . ADMIN_URL . 'assets/images/edit_action.gif"></a>';
            $res .= '</span>';
            $res .= '<span class="item" style="display:block;"><span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span><img src="' . ADMIN_URL . '/assets/rest/' . $image_small . '"></span>';
            $res .= '<div class="clearfix" id="closure_imageupdatediv" style="display:none;">';
            $res .= '<form method="post" action="' . ADMIN_URL . 'restricted_fish_in_region/update_image" name="restrictedimageupdateform"  id="restrictedimageupdateform_' . $lid . '"  class="restrictedimageupdateform form has-validation" enctype="multipart/form-data">';
            $res .= '<input type="hidden" value="' . $lid . '" name="restricted_updateimageid" id="restricted_updateimageid" />';
            $res .= '<input type="hidden" value="' . $image_small . '" name="oldimage" id="oldimage" />';
            $res .= '<input type="file" name="restricted_updateimage" value=""><br/>';
            $res .= '<small><em>Allowed maximmum size 2 MB</em></small><br/>';
            $res .= '<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Upload" data-icon-primary="ui-icon-circle-check">Upload</button>';
            $res .= '<button class="button" type="button" name="cancelbutton" id="cancelbutton" value="Save" data-icon-primary="ui-icon-circle-check">Cancel</button>';
            $res .= '</form>';
            $res .= '</div>';
            echo $res;
            exit;
        } else {
            echo "1";
            exit;
        }
    }

    public function closure_list($id)
    {
        if ($id != '' || $id > 0) {
            $header_data['title'] = 'NZFishing';
            $footer_data['devloped_by'] = "Tech Xperts";
            $region_array = array();
            $region_array[''] = 'Select Region';
            $region_name = $this->restricted_area->get_region();
            foreach ($region_name as $region) {
                $region_array[$region->id] = $region->name;
            }
            $data['region'] = $region_array;
            $data['regionid'] = $id;
            $searchTerm = !empty($this->input->get('search')) ? $this->input->get('search') : "";
            $data['closuresdata'] = $this->restricted_area->get_region_closures_detail($id, $searchTerm);
            $this->load->view('header', $header_data);
            $this->load->view('navigation');
            $this->load->view('closures_detail', $data);
            $this->load->view('footer', $footer_data);
        } else {
            redirect('/closures_and_restrictions');
        }
    }

    public function get_areas()
    {
        if (isset($_GET['term'])) {
            $area = strtolower($_GET['term']);
            $this->restricted_area->get_area($area);
        }
    }

    public function edit_closure($id)
    {
        $details = $this->restricted_area->get_single_closures_detail($id);
        if (!empty($details)) {
            $html = $this->load->view('closures_detail_edit', ['details' => $details],true);
            header('Content-Type:application/json');
            echo json_encode([
                'status' => 'success',
                'response' => [
                    'html' => $html
                ],
            ]);
            exit();
        } else {
            header('Content-Type:application/json');
            echo json_encode([
                'status' => 'error',
                'message' => 'Something went wrong.',
            ]);
            exit();
        }
    }

    /**
     * Method used to export csv for data
     * @return void
     */
    public function export()
    {
        try {
            $closureAndRestrictions = $this->restricted_area->getAllClosureAndRestrictionsToExport();
            if (count($closureAndRestrictions) > 0) {
                $tempData = [];
                foreach ($closureAndRestrictions as $key => $closureAndRestriction) {
                    $closureAndRestriction->date_added = !empty($closureAndRestriction->date_added) ? date('d F, Y', strtotime($closureAndRestriction->date_added)) : '';
                    $tempData[] = $closureAndRestriction;
                }
                generateCsv($tempData, 'closure-and-restrictions.csv');
                exit();
            } else {
                $this->session->set_flashdata('error-message', 'Unable to find data to export.');
                redirect('closures_and_restrictions');
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error-message', 'Something went wrong.');
            redirect('closures_and_restrictions');
        }
    }
}
