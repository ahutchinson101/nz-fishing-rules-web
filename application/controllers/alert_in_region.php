<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Alert_In_Region extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('alert_in_region_model', 'region_alert');
    }

    //fish in region page
    public function index()
    {
        $alert_in_region = $this->region_alert->get_region_alert();
        $header_data['title'] = 'NZFishing';
        $footer_data['devloped_by'] = "Tech Xperts";
        $data['alert_in_region'] = $alert_in_region;
        $region_array = array();
        $region_array[''] = 'Select Region';
        $region_name = $this->region_alert->get_region();
        foreach ($region_name as $region) {
            $region_array[$region->id] = $region->name;
        }
        $data['region'] = $region_array;
        $this->load->view('header', $header_data);
        $this->load->view('navigation');
        $this->load->view('alert_in_region', $data);
        $this->load->view('footer', $footer_data);
    }

    // Edit fish in region
    public function edit($id = 0)
    {
        $msg = $this->input->post('region_msg');
        $alert_url = $this->input->post('alert_url');
        if ($this->region_alert->alert_in_region_update($id, $msg, $alert_url) == true) {
            $this->session->set_flashdata('flash-message', 'Alert Message detail is updated');
        } else {
            $this->session->set_flashdata('error-message', 'Alert Message detail Not update please try again..!');
        }
        redirect('alert_in_region');
    }

    // delete fish in region
    function delete($id = 0)
    {
        if ($this->region_alert->delete_alert_in_region($id) == true) {
            $this->session->set_flashdata('flash-message', 'Alert Message detail deleted successfully');
            redirect('alert_in_region');
        } else {
            $this->session->set_flashdata('error-message', 'Alert Message detail not deleted');
            redirect('alert_in_region');
        }
    }

    // add fish in region
    public function add()
    {
        $this->form_validation->set_rules('region', 'Region', 'trim|required');
        $this->form_validation->set_rules('region_msg', 'Region Message', 'trim|required');
        $this->form_validation->set_error_delimiters('<p>', '</p>');
        if ($this->form_validation->run() == FALSE) {
            $header_data['title'] = 'NZFishing';
            $footer_data['devloped_by'] = "Tech Xperts";
            $region_array = array();
            $region_array[''] = 'Select Region';
            $region_name = $this->region_alert->get_region();
            foreach ($region_name as $region) {
                $region_array[$region->id] = $region->name;
            }
            $data['region'] = $region_array;
            $this->load->view('header', $header_data);
            $this->load->view('navigation');
            $this->load->view('alert_in_region_add', $data);
            $this->load->view('footer', $footer_data);
        } else {
            if ($this->region_alert->add_alert_in_region() == true) {
                $this->session->set_flashdata('flash-message', 'Alert in region add successfully');
                redirect('alert_in_region');
                exit;
            } else {
                $this->session->set_flashdata('error-message', 'Alert in region not added');
                redirect('alert_in_region');
                exit;
            }
        }

    }

    // check fish in region
    public function check_fish($id = 0)
    {
        $fish_master = $this->region_fish->not_inserted_fish_in_region($id);
        if (count($fish_master) > 0) {
            $fish = array();
            $fish[''] = "Select Fish";
            foreach ($fish_master as $fish_master) {
                $fish[$fish_master->id] = $fish_master->keyword;
            }
            $data['fish'] = $fish;
            $this->load->view('fish_combo', $data);
        } else {
            $this->load->view('fish_combo_error');
        }
    }

    //fish detail
    public function fish_detail($id = 0)
    {
        $fish = $this->region_fish->get_fish_detail($id);
        if (count($fish) > 0) {
            $data['fish'] = $fish;
            $this->load->view('fish_detail', $data);
        }
    }

    /**
     * Method used to export csv for data
     * @return void
     */
    public function export()
    {
        try {
            $alerts = $this->region_alert->getAllAlertsToExport();
            if (count($alerts) > 0) {
                generateCsv($alerts, 'alerts-in-region.csv');
                exit();
            } else {
                $this->session->set_flashdata('error-message', 'Unable to find data to export.');
                redirect('alert_in_region');
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error-message', 'Something went wrong.');
            redirect('alert_in_region');
        }
    }
}
