<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account extends CI_Controller{
	public function __construct(){
		parent::__construct();
		is_login();
		$this->load->model('account_model');
	}
	
	//Account page
	public function index(){
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|matches[passwordagain]');
		$this->form_validation->set_rules('passwordagain', 'Password (Again)', 'trim|matches[password]');
		$this->form_validation->set_error_delimiters('<li class="red"><span class="ico"></span><strong class="system_title">', '</strong></li>');
		if ($this->form_validation->run() == FALSE)
		{
			$users=$this->account_model->get_logged_user($this->session->userdata('user_id'));
			$header_data['title']='NZFishing';
			$footer_data['devloped_by']="Tech Xperts";
			$data['user']=$users;
			$this->load->view('header',$header_data);
			$this->load->view('navigation');
			$this->load->view('account',$data);
			$this->load->view('footer',$footer_data);
		}else{
			if($this->account_model->upadate_accont()==true){
				$this->session->set_flashdata('flash-message','Account update successfully');
				redirect('account');
				exit;
			}else{
				$this->session->set_flashdata('error-message','Account not update');
				redirect('account');
				exit;
			}
		}
	}
}
