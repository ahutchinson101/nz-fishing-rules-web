<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fish_Type extends CI_Controller{
	public function __construct(){
		parent::__construct();
		is_login();
		$this->load->model('fish_type_model');
	}
	//fish type page
	public function index(){
		$this->form_validation->set_rules('multiaction', 'Multiple Action', 'trim|required');
		$this->form_validation->set_rules('multicontent[]', 'Checkbox', 'trim|required');
		$this->form_validation->set_error_delimiters('<li class="red"><span class="ico"></span><strong class="system_title">', '</strong></li>');
		if($this->form_validation->run() == FALSE) {
			$fish_type=$this->fish_type_model->get_fish_type();
			$header_data['title']='NZFishing';
			$footer_data['devloped_by']="Tech Xperts";
			$data['all_fish_type']=$fish_type;
			$this->load->view('header',$header_data);
			$this->load->view('navigation');
			$this->load->view('fish_type',$data);
			$this->load->view('footer',$footer_data);
		} else{
			$multiaction = $this->input->post('multiaction',TRUE);
			$multicontent = $this->input->post('multicontent');
			$this->fish_type_model->Multi_action($multiaction,$multicontent);
			$this->session->set_flashdata('flash-message','Delete Successfully');
			redirect('fish_type');
		}
	}
	
	//delete fish type
	public function delete($id=0){
		if($this->fish_type_model->delete_fish_type($id)==true){
			$this->session->set_flashdata('flash-message','Fish Type deleted successfully');
			redirect('fish_type');
		}else{
			$this->session->set_flashdata('error-message','Fish Type not deleted');
			redirect('fish_type');
		}
	}
	
	//edit fish type
	public function edit($id=0){
		$this->form_validation->set_rules('fish_type_name', 'Fish Type Name', 'trim|required');
		$this->form_validation->set_error_delimiters('<p>', '</p>');
		if($this->form_validation->run() == FALSE)
		{
			$fish_type=$this->fish_type_model->get_one_fish_type($id);
			$header_data['title']='NZFishing';
			$footer_data['devloped_by']="Tech Xperts";
			$data['fish_type']=$fish_type;
			$this->load->view('header',$header_data);
			$this->load->view('navigation');
			$this->load->view('edit_fish_type',$data);
			$this->load->view('footer',$footer_data);
		}else{
			if($this->fish_type_model->update_fish_type($id)==true){
				$this->session->set_flashdata('flash-message','Fish Type update successfully');
				redirect('fish_type/edit/'.$id);
				exit;
			}else{
				$this->session->set_flashdata('error-message','Fish Type not update');
				redirect('fish_type/edit/'.$id);
				exit;
			}
		}
	}
	
	// add fish type
	public function add(){
		$this->form_validation->set_rules('fish_type_name', 'Fish Type Name', 'trim|required|callback_unique_fish_type');
		$this->form_validation->set_error_delimiters('<p>', '</p>');
		if($this->form_validation->run() == FALSE)
		{
			$header_data['title']='NZFishing';
			$footer_data['devloped_by']="Tech Xperts";
			$this->load->view('header',$header_data);
			$this->load->view('navigation');
			$this->load->view('add_fish_type');
			$this->load->view('footer',$footer_data);
		}else{
			if($this->fish_type_model->add_fish_type()==true){
				$this->session->set_flashdata('flash-message','Fish Type add successfully');
				redirect('fish_type');
				exit;
			}else{
				$this->session->set_flashdata('error-message','Fish Type not added');
				redirect('fish_type');
				exit;
			}
		}
	}
	
	// check unique fish
	function unique_fish_type($fish_type_name){
		if($this->fish_type_model->get_unique_fish_type($fish_type_name)==false){
			$this->form_validation->set_message('unique_fish_type','This Fish Type name already xists...!');
			return false;
		}
	}
}
	