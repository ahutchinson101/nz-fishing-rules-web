<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fish_In_Region extends CI_Controller{
	public function __construct(){
		parent::__construct();
		is_login();
		$this->load->model('fish_in_region_model','region_fish');
	}
	
	//fish in region page
    public function index($id = 1, $row = 0)
    {
        $row = !empty($this->input->get('per_page')) ? $this->input->get('per_page') : $row;
        $this->form_validation->set_rules('multiaction', 'Multiple Action', 'trim|required');
        $this->form_validation->set_rules('multicontent[]', 'Checkbox', 'trim|required');
        $this->form_validation->set_error_delimiters('<li class="red"><span class="ico"></span><strong class="system_title">', '</strong></li>');
        if ($this->form_validation->run() == FALSE) {
            $this->config->load('paging', TRUE);
            $records_per_page = RECORD_PER_PAGE;
            $searchTerm = !empty($this->input->get('search')) ? $this->input->get('search') : "";
            $fish_in_region = $this->region_fish->get_region_fish($id, $row, $records_per_page, $searchTerm);
            $this->load->library('pagination');
            $config['base_url'] = ADMIN_URL . 'fish_in_region/index/' . $id . '?search='.$searchTerm;
            $config['page_query_string'] = TRUE;
            $tot_p = $this->region_fish->get_fish_in_region_count($id, $searchTerm);
            $config['total_rows'] = $tot_p;
            $config['per_page'] = $records_per_page;
            $config['uri_segment'] = 4;
            $config['full_tag_open'] = $this->config->item('full_tag_open', 'paging');
            $config['full_tag_close'] = $this->config->item('full_tag_close', 'paging');
            $config['first_link'] = $this->config->item('first_link', 'paging');
            $config['first_tag_open'] = $this->config->item('first_tag_open', 'paging');
            $config['first_tag_close'] = $this->config->item('first_tag_close', 'paging');
            $config['last_link'] = $this->config->item('last_link', 'paging');
            $config['last_tag_open'] = $this->config->item('last_tag_open', 'paging');
            $config['last_tag_close'] = $this->config->item('last_tag_close', 'paging');
            $config['next_link'] = $this->config->item('next_link', 'paging');
            $config['next_tag_open'] = $this->config->item('next_tag_open', 'paging');
            $config['next_tag_close'] = $this->config->item('next_tag_close', 'paging');
            $config['prev_link'] = $this->config->item('prev_link', 'paging');
            $config['prev_tag_open'] = $this->config->item('prev_tag_open', 'paging');
            $config['prev_tag_close'] = $this->config->item('prev_tag_close', 'paging');
            $config['cur_tag_open'] = $this->config->item('cur_tag_open', 'paging');
            $config['cur_tag_close'] = $this->config->item('cur_tag_close', 'paging');
            $config['num_tag_open'] = $this->config->item('num_tag_open', 'paging');
            $config['num_tag_close'] = $this->config->item('num_tag_close', 'paging');
            $this->pagination->initialize($config);
            $header_data['title'] = 'NZFishing';
            $footer_data['devloped_by'] = "Tech Xperts";
            $data['fish_in_region'] = $fish_in_region;
            $region_array = array();
            $region_array[''] = 'Select Region';
            $region_name = $this->region_fish->get_region();
            foreach ($region_name as $region) {
                $region_array[$region->id] = $region->name;
            }
            $data['region'] = $region_array;
            $data['region_id'] = $id;
            $this->load->view('header', $header_data);
            $this->load->view('navigation');
            $this->load->view('fish_in_region', $data);
            $this->load->view('footer', $footer_data);
        } else {
            $multiaction = $this->input->post('multiaction', TRUE);
            $multicontent = $this->input->post('multicontent');
            $this->region_fish->Multi_action($multiaction, $multicontent);
            $this->session->set_flashdata('flash-message', 'Delete Successfully');
            redirect('fish_in_region');
        }
    }
	
	// Edit fish in region
	public function edit($id=0)
    {
        $this->form_validation->set_rules('regions[]', 'Region', 'trim|required');
		$this->form_validation->set_rules('part_of_area_bag_limit', '', 'callback_bag_check');
		if($this->form_validation->run() == FALSE)
		{
			$header_data['title']='NZFishing';
			$footer_data['devloped_by']="Tech Xperts";
			$fish_in_region=$this->region_fish->get_one_fish_in_region($id);
			$fish=$this->region_fish->get_fish_detail($fish_in_region->fish_id);
			$region=$this->region_fish->get_region($fish_in_region->region_id);
            $region_name=$this->region_fish->get_region();

            $group = $this->region_fish->get_group($fish_in_region->group_id);
            //print_r($group);exit;
            if($group)
            {
                $fish_in_region_ids = explode(",",$group->fish_in_region_ids);
                $region_ids = $this->region_fish->get_region_ids($fish_in_region_ids);
            } else {
                $region_ids = [$fish_in_region->region_id];
            }
			$data['regions'] = $region_name;
			$data['fish'] = $fish;
			$data['region'] = $region;
			$data['region_ids'] = $region_ids;
			$data['fish_in_region'] = $fish_in_region;
			$this->load->view('header',$header_data);
			$this->load->view('navigation');
			$this->load->view('fish_in_region_edit',$data);
			$this->load->view('footer',$footer_data);
		}else{
			if($this->region_fish->fish_in_region_update() == true){
				$this->session->set_flashdata('flash-message','Fish detail is updated');
			}else{
				$this->session->set_flashdata('error-message','Fish detail Not update please try again..!');
			}
			redirect('fish_in_region');
		}
	}

	function bag_check($part){
		if($part==1 && ($this->input->post('bag')=="" || $this->input->post('bag')==0) ){
			$this->form_validation->set_message('bag_check','Please enter bag limit');
			return false;

		}else{
			return true;
		}

	}

	// delete fish in region
	function delete($id=0){
		if($this->region_fish->delete_fish_in_region($id)==true){
			$this->session->set_flashdata('flash-message','Fish detail deleted successfully');
			redirect('fish_in_region');
		}else{
			$this->session->set_flashdata('error-message','Fish detail not deleted');
			redirect('fish_in_region');
		}
	}

	// add fish in region
	public function add(){
		$this->form_validation->set_rules('regions[]', 'Region', 'trim|required');
		$this->form_validation->set_rules('fish', 'Fish', 'trim|required');
		$this->form_validation->set_error_delimiters('<p>', '</p>');
		if($this->form_validation->run() == FALSE)
		{
			$header_data['title']='NZFishing';
			$footer_data['devloped_by']="Tech Xperts";
			//$region_array = array();
			//$region_array[''] = 'Select Region';
			$region_name=$this->region_fish->get_region();
			/*foreach ($region_name as $region) {
				$region_array[$region->id] = $region->name;
			}*/
			$data['region'] = $region_name;
			$this->load->view('header',$header_data);
			$this->load->view('navigation');
			$this->load->view('fish_in_region_add',$data);
			$this->load->view('footer',$footer_data);
		} else {
			if($this->region_fish->add_fish_in_region()==true) {
				$this->session->set_flashdata('flash-message','Fish in region add successfully');
				redirect('fish_in_region/index/'.$this->input->post('region'));
				exit;
			} else {
				$this->session->set_flashdata('error-message','Fish in region not added');
				redirect('fish_in_region');
				exit;
			}
		}
	}
	// check fish in region
	public function check_fish_new()
    {
        $regions = $this->input->post('regions');
        $fish_master=$this->region_fish->not_inserted_fish_in_region_new($regions);
        if(count($fish_master)>0){
            $fish=array();
            $fish['']="Select Fish";
            foreach ($fish_master as $fish_master){
                $fish[$fish_master->id]=$fish_master->keyword;
            }
            $data['fish']=$fish;
            $this->load->view('fish_combo',$data);
        }else{
            $this->load->view('fish_combo_error');
        }
    }

	public function check_fish($id=0){
		$fish_master=$this->region_fish->not_inserted_fish_in_region($id);
		if(count($fish_master)>0){
			$fish=array();
			$fish['']="Select Fish";
			foreach ($fish_master as $fish_master){
				$fish[$fish_master->id]=$fish_master->keyword;
			}
			$data['fish']=$fish;
			$this->load->view('fish_combo',$data);
		}else{
			$this->load->view('fish_combo_error');
		}
	}
	
	//fish detail
	public function fish_detail($id=0){
		$fish=$this->region_fish->get_fish_detail($id);
		if($fish){
			$data['fish']=$fish;
			$this->load->view('fish_detail',$data);
		}
	}

    /**
     * Method used to export csv for data
     * @return void
     */
    public function export()
    {
        try {
            $fishInRegion = $this->region_fish->getAllFishInRegionToExport();
            if (count($fishInRegion) > 0) {
                generateCsv($fishInRegion, 'fish-in-region.csv');
                exit();
            } else {
                $this->session->set_flashdata('error-message', 'Unable to find data to export.');
                redirect('fish_in_region');
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error-message', 'Something went wrong.');
            redirect('fish_in_region');
        }
    }
	
}
