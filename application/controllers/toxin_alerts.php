<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Toxin_Alerts extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_login();
        $this->load->model('toxin_alerts_model', 'toxin_alerts');
    }

    public function index($id = 1, $row = 0)
    {
        $header_data['title'] = 'NZFishing';
        $footer_data['devloped_by'] = "Tech Xperts";
        $region_array = array();
        $region_array[''] = 'Select Region';
        $region_name = $this->toxin_alerts->get_region();
        foreach ($region_name as $region) {
            $region_array[$region->id] = $region->name;
        }
        $data['region'] = $region_array;
        $this->load->view('header', $header_data);
        $this->load->view('navigation');
        $this->load->view('toxin_alerts', $data);
        $this->load->view('footer', $footer_data);
    }

    public function alert_list($id)
    {
        if ($id || $id > 0) {
            $header_data['title'] = 'NZFishing';
            $footer_data['devloped_by'] = "Tech Xperts";
            $region_array = array();
            $region_array[''] = 'Select Region';
            $region_name = $this->toxin_alerts->get_region();
            foreach ($region_name as $region) {
                $region_array[$region->id] = $region->name;
            }
            $data['region'] = $region_array;
            $data['regionid'] = $id;
            $searchTerm = !empty($this->input->get('search')) ? $this->input->get('search') : '';
            $data['alertsdata'] = $this->toxin_alerts->get_region_toxin_alerts_detail($id, $searchTerm);
            $this->load->view('header', $header_data);
            $this->load->view('navigation');
            $this->load->view('toxin_alerts_detail', $data);
            $this->load->view('footer', $footer_data);
        } else {
            redirect('/toxin_alerts');
        }
    }

    public function alertsrestorder()
    {
        $region = $this->uri->segment(3);
        $sortables = $_POST['sortables'];
        $size = count($sortables);
        for ($i = 0; $i < $size; $i++) {
            $this->toxin_alerts->update_toxin_alerts_ordering($i, $sortables[$i]);
        }
        exit;
    }

    // check fish in region
    public function check_fish($id = 0)
    {
        $fish_in_region = $this->toxin_alerts->res_fish_in_region($id);
        if (count($fish_in_region) > 0) {
            $fish = array();
            $fish[''] = "Select Fish";
            foreach ($fish_in_region as $fish_in_region) {
                $fish[$fish_in_region->fish_id] = $fish_in_region->keyword;
            }
            $data['fish'] = $fish;
            $data['regionid'] = $id;
            $this->load->view('closures_fish_combo', $data);
        } else {
            $this->load->view('fish_combo_error');
        }
    }

    public function alert_combo_list()
    {
        $toxin_alerts_array = array();
        $toxin_alerts_array[''] = 'Select Toxin Alert';
        $toxin_alerts_name = $this->toxin_alerts->get_toxin_alerts();
        foreach ($toxin_alerts_name as $toxin_alert) {
            $toxin_alerts_array[$toxin_alert->id] = $toxin_alert->type . ' ' . $toxin_alert->title;
        }
        $data['toxin_alerts'] = $toxin_alerts_array;
        $this->load->view('toxin_alerts_combo', $data);
    }

    public function alert_delete($id = 0)
    {
        if ($this->toxin_alerts->toxin_alert_delete($id) == true) {
            echo "1";
            exit;
        } else {
            echo "0";
            exit;
        }
    }

    public function add_toxin_alerts()
    {
        $region = $this->uri->segment(3);
        $cdate = date('Y-m-d H:i:s');
        $areas = $this->input->post("areas");
        $title = $this->input->post("title");
        $date_added = $this->input->post("date_added");
        if ($date_added) {
            $date_added = date('Y-m-d', strtotime($this->input->post("date_added")));
        } else {
            $date_added = null;
        }
        $key_points = $this->input->post("key_points");
        $content = $this->input->post("content");

        if ($lid = $this->toxin_alerts->add_toxin_alerts($region, $areas, $title, $date_added, $key_points, $content, $cdate)) {
            $res = '<li id="' . $lid . '" class="liitem">';
            $res .= '<span style="float: right;">';
            $res .= '<a href="#" style="float: right;" class="remove_toxin_alert" id="' . $lid . '"><img alt="Remove" src="' . ADMIN_URL . 'assets/images/action4.gif"></a>';
            $res .= '<a href="#" style="float: right; padding-right: 6px;" class="edit_toxin_alert"><img alt="Edit" src="' . ADMIN_URL . 'assets/images/edit_action.gif"></a>';
            $res .= '</span>';
            $res .= '<spam class="item" style="display:block;"><span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span>' . nl2br($title . ' ' . $areas) . '</spam>';
            $res .= '<div class="clearfix" id="toxin_alertsupdatediv" style="display:none;">';
            $res .= '<form method="post" action="#" name="toxin_alertsupdateform"  id="toxin_alertsupdateform"  class="form has-validation" enctype="multipart/form-data">';
            $res .= '<input type="hidden" value="' . $lid . '" name="toxin_alert_id" id="toxin_alert_id" />';
            $res .= '<table>';
            $res .= '<tr style="display: none;">';
            $res .= '<td style="vertical-align:middle;"><label for="areas" class="form-label">Areas :</label></td>';
            $res .= '<td><input type="text" name="areas" id="areas" class="areas" style="width:100%;" value="' . $areas . '"/></td>';
            $res .= '</tr>';
            $res .= '<tr>';
            $res .= '<td style="vertical-align: middle;"><label for="title" class="form-label">Title :</label></td>';
            $res .= '<td><input type="text" name="title" id="title" style="width:100%;" value="' . $title . '"/></td>';
            $res .= '</tr>';
            $res .= '<tr style="display: none;">';
            $res .= '<td style="vertical-align: middle;"><label for="title" class="form-label">Key points :</label></td>';
            $res .= '<td><input type="text" name="key_points" id="key_points" style="width:100%;" value="' . $key_points . '"/></td>';
            $res .= '</tr>';
            $res .= '<tr>';
            $res .= '<td style="vertical-align: middle;"><label for="restricted_msg" class="form-label">Description :</label></td>';
            $res .= '<td>';
            $res .= '<textarea name="content" id="content_' . $lid . '" class="toxin_alert_content" style="width: 100%; height: 200px">' . $content . '</textarea>';
            $res .= '</td>';
            $res .= '</tr>';
            $res .= '<tr>';
            $res .= '<td>';
            $res .= '</td>';
            $res .= '<td>';
            $res .= '<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Save" data-icon-primary="ui-icon-circle-check">Save</button>';
            $res .= '<button class="button" type="button" name="toxin_alert_cancelbutton" id="toxin_alert_cancelbutton" value="Save" data-icon-primary="ui-icon-circle-check">Cancel</button>';
            $res .= '</td>';
            $res .= '</tr>';
            $res .= '</table>';
            $res .= '</form>';
            $res .= '</div>';
            echo $res;
            exit;
        } else {
            echo "1";
            exit;
        }
    }

    public function alert_update()
    {
        $id = $this->uri->segment(3);
        $areas = $this->input->post("areas");
        $title = $this->input->post("title");
        $date_added = $this->input->post("date_added");
        if ($date_added) {
            $date_added = date('Y-m-d', strtotime($this->input->post("date_added")));
        } else {
            $date_added = null;
        }
        $key_points = $this->input->post("key_points");
        $content = $this->input->post("content");
        $toxin_alert = $this->toxin_alerts->get_single_toxin_alert_detail($id);
        $oldcontent = $toxin_alert->content;
        if ($this->toxin_alerts->update_toxin_alert($id, $areas, $title, $date_added, $key_points, $content, $oldcontent) == true) {
            echo "1";
            exit;
        } else {
            echo "0";
            exit;
        }
    }

    public function edit_toxin_alert($id)
    {
        $alert = $this->toxin_alerts->get_single_toxin_alert_detail($id);
        if (!empty($alert)) {
            $html = $this->load->view('toxin_alerts_detail_edit', ['alert' => $alert],true);
            header('Content-Type:application/json');
            echo json_encode([
                'status' => 'success',
                'response' => [
                    'html' => $html
                ],
            ]);
            exit();
        } else {
            header('Content-Type:application/json');
            echo json_encode([
                'status' => 'error',
                'message' => 'Something went wrong.',
            ]);
            exit();
        }
    }

    /**
     * Method used to export csv for data
     * @return void
     */
    public function export()
    {
        try {
            $toxinAlerts = $this->toxin_alerts->getAllToxinAlertsToExport();
            if (count($toxinAlerts) > 0) {
                $tempData = [];
                foreach ($toxinAlerts as $key => $toxinAlert) {
                    $toxinAlert->date_added = !empty($toxinAlert->date_added) ? date('d F, Y', strtotime($toxinAlert->date_added)) : '';
                    $tempData[] = $toxinAlert;
                }
                generateCsv($tempData, 'toxin-alerts.csv');
                exit();
            } else {
                $this->session->set_flashdata('error-message', 'Unable to find data to export.');
                redirect('closures_and_restrictions');
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error-message', 'Something went wrong.');
            redirect('closures_and_restrictions');
        }
    }
}
