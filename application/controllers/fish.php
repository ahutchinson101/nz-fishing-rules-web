<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fish extends CI_Controller{
	static  $image_small='';
	static $image_small_update=false;
	public function __construct(){
		parent::__construct();
		is_login();
		$this->load->model('fish_model');
	}
	//fish page
    public function index($row = 0)
    {
        $row = !empty($this->input->get('per_page')) ? $this->input->get('per_page') : $row;
        $this->form_validation->set_rules('multiaction', 'Multiple Action', 'trim|required');
        $this->form_validation->set_rules('multicontent[]', 'Checkbox', 'trim|required');
        $this->form_validation->set_error_delimiters('<li class="red"><span class="ico"></span><strong class="system_title">', '</strong></li>');
        if ($this->form_validation->run() == FALSE) {
            $this->config->load('paging', TRUE);
            $records_per_page = RECORD_PER_PAGE;
            $searchTerm = !empty($this->input->get('search')) ? $this->input->get('search') : "";
            $fish = $this->fish_model->get_fish($row, $records_per_page, $searchTerm);

            $this->load->library('pagination');
            $config['base_url'] = ADMIN_URL . 'fish/index/?search='.$searchTerm;
            $config['page_query_string'] = TRUE;
            $tot_p = $this->fish_model->get_fish_count($searchTerm);
            $config['total_rows'] = $tot_p;
            $config['per_page'] = $records_per_page;
            $config['uri_segment'] = 3;
            $config['full_tag_open'] = $this->config->item('full_tag_open', 'paging');
            $config['full_tag_close'] = $this->config->item('full_tag_close', 'paging');
            $config['first_link'] = $this->config->item('first_link', 'paging');
            $config['first_tag_open'] = $this->config->item('first_tag_open', 'paging');
            $config['first_tag_close'] = $this->config->item('first_tag_close', 'paging');
            $config['last_link'] = $this->config->item('last_link', 'paging');
            $config['last_tag_open'] = $this->config->item('last_tag_open', 'paging');
            $config['last_tag_close'] = $this->config->item('last_tag_close', 'paging');
            $config['next_link'] = $this->config->item('next_link', 'paging');
            $config['next_tag_open'] = $this->config->item('next_tag_open', 'paging');
            $config['next_tag_close'] = $this->config->item('next_tag_close', 'paging');
            $config['prev_link'] = $this->config->item('prev_link', 'paging');
            $config['prev_tag_open'] = $this->config->item('prev_tag_open', 'paging');
            $config['prev_tag_close'] = $this->config->item('prev_tag_close', 'paging');
            $config['cur_tag_open'] = $this->config->item('cur_tag_open', 'paging');
            $config['cur_tag_close'] = $this->config->item('cur_tag_close', 'paging');
            $config['num_tag_open'] = $this->config->item('num_tag_open', 'paging');
            $config['num_tag_close'] = $this->config->item('num_tag_close', 'paging');
            $this->pagination->initialize($config);

            $header_data['title'] = 'NZFishing';
            $footer_data['devloped_by'] = "Tech Xperts";
            $data['all_fish'] = $fish;
            $this->load->view('header', $header_data);
            $this->load->view('navigation');
            $this->load->view('fish', $data);
            $this->load->view('footer', $footer_data);
        } else {
            $multiaction = $this->input->post('multiaction', TRUE);
            $multicontent = $this->input->post('multicontent');
            $this->fish_model->Multi_action($multiaction, $multicontent);
            $this->session->set_flashdata('flash-message', 'Delete Successfully');
            redirect('fish');
        }
    }
	
	//delete fish
	public function delete($id=0){
		if($this->fish_model->delete_fish($id)==true){
			$this->session->set_flashdata('flash-message','Fish deleted successfully');
			redirect('fish');
			exit;
		}else{
			$this->session->set_flashdata('error-message','Fish not deleted');
			redirect('fish');
			exit;
		}
	}
	//edit fish
	public function edit($id=0)
    {

		$this->form_validation->set_rules('keyname', 'Keyname', 'trim|required|callback_check_file_edit');
		$this->form_validation->set_rules('keyword', 'Keyword', 'trim|required');
		$this->form_validation->set_rules('fish_type', 'Fish Type', 'trim|required');
		$this->form_validation->set_error_delimiters('<p>', '</p>');
		if($this->form_validation->run() == FALSE)
		{
			$fish=$this->fish_model->get_one_fish($id);
            $fish_images=$this->fish_model->get_fish_images($id);
			$fish_type=$this->fish_model->get_fish_type();
			$ftype=array();
			$ftype['']='Select Fish Type';
			foreach ($fish_type as $fish_type)
			{
				$ftype[$fish_type->id] = $fish_type->fish_type_name;
			}

			$header_data['title']=' NZFishing';
			$footer_data['devloped_by']="Tech Xperts";
			$data['fish'] = $fish;
			$data['fish_images'] = $fish_images;
			$data['fish_type'] = $ftype;
			$this->load->view('header',$header_data);
			$this->load->view('navigation');
			$this->load->view('edit_fish',$data);
			$this->load->view('footer',$footer_data);
		}else{
			$data=array(
					'fish_type_id'=>$this->input->post('fish_type',true),
					'keyname'=>$this->input->post('keyname',true),
					'keyword'=>$this->input->post('keyword',true),
					'common_alt_name'=>$this->input->post('common_alt_name',true),
					'mori_name'=>$this->input->post('maori_name',true),
					'scientific_name'=>$this->input->post('scientific_name',true),
					'national_size'=>$this->input->post('national_size',true),
					'national_daily'=>$this->input->post('national_daily',true),
					'accumulation'=>$this->input->post('accumulation',true),
					'updated_date'=>date('Y-m-d H:i:s')
			);
			if(self::$image_small_update) {
				$data['fish_image_updated'] = date('Y-m-d H:i:s');
			}
			if(self::$image_small_update){
				$data['fish_image']=self::$image_small;
			}
			if($this->fish_model->update_fish($id,$data)==true) {

                $fish_images = $this->session->userdata('fish_images');
                foreach ($fish_images as $fish_image) {
                    $this->fish_model->add_fish_image(array('fish_master_id' => $id,'fish_image'=>$fish_image,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')));
                }
                $this->session->unset_userdata('fish_images');

				$this->session->set_flashdata('flash-message','Fish update successfully');
				redirect('fish/edit/'.$id);
				exit;
			} else {
				$this->session->set_flashdata('error-message','Fish not update');
				redirect('fish/edit/'.$id);
				exit;
			}
		}
	}

	// add fish
	public function fileUpload()
    {
        $config['upload_path'] = 'assets/fish/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '500';
        $config['max_width'] = '800';
        $config['max_height'] = '800';
        $this->load->library('upload', $config);
        if($this->upload->do_upload('file')) {
            $data =  $this->upload->data();
            //self::$image_small = $data['file_name'];
            if($data['image_width']< 510 && $data['image_height']< 240)
            {

            } else {
                $config=array();
                $config['image_library'] = 'gd2';
                $config['source_image']	= FCPATH.'assets/fish/'.$data['file_name'];
                $config['maintain_ratio'] = TRUE;
                $config['create_thumb']=FALSE;
                $config['width'] = 510;
                $config['height'] = 240;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $this->image_lib->clear();
            }
            $fish_images = $this->session->userdata('fish_images');
            if(!$fish_images) {
                $fish_images = array();
            }
            array_push($fish_images,$data['file_name']);
            $this->session->set_userdata(array('fish_images' => $fish_images));
            //self::$image_small = $data['file_name'];
            header("Content-Type:application/json");
            echo json_encode($data);exit;
        } else {
            header("Status:400");
            echo $this->upload->display_errors('','');exit;
        }
    }

	public function deleteImage()
    {
        $fish_image_id = $this->input->post('fish_image_id',true);
        if(intval($fish_image_id)) {
            $this->fish_model->delete_fish_image($fish_image_id);
        } else {

            $file_name = $this->input->post('file_name',true);

            if($file_name!="" && is_file(FCPATH.'assets/fish/'.$file_name)){
                @unlink(FCPATH.'assets/fish/'.$file_name);
            }

            $fish_images = $this->session->userdata('fish_images');
            foreach ($fish_images as $key => $fish_image) {
                if($file_name == $fish_image) {
                    unset($fish_images[$key]);
                }
            }
            $this->session->set_userdata(array('fish_images' => $fish_images));
        }

        header("Content-Type:application/json");
        echo json_encode(['success' => true]);exit;
    }

	public function add()
    {
		$this->form_validation->set_rules('keyname', 'Keyname', 'trim|required|callback_check_file');
		$this->form_validation->set_rules('keyword', 'Keyword', 'trim|required|callback_unique_fish');
		$this->form_validation->set_rules('fish_type', 'Fish Type', 'trim|required');
		$this->form_validation->set_error_delimiters('<p>', '</p>');
		if($this->form_validation->run() == FALSE)
		{
			$fish_type=$this->fish_model->get_fish_type();
			$ftype=array();
			$ftype['']='Select Fish Type';
			foreach ($fish_type as $fish_type)
			{
				$ftype[$fish_type->id]=$fish_type->fish_type_name;
			}
            $fish_images = $this->session->userdata('fish_images');
			$header_data['title']='NZFishing';
			$footer_data['devloped_by']="Tech Xperts";
			$data['fish_type']=$ftype;
			$data['fish_images']=$fish_images;
			$this->load->view('header',$header_data);
			$this->load->view('navigation');
			$this->load->view('add_fish',$data);
			$this->load->view('footer',$footer_data);
		}else{
			$data=array(
					'fish_type_id'=>$this->input->post('fish_type',true),
					'keyname'=>$this->input->post('keyname',true),
					'keyword'=>$this->input->post('keyword',true),
					'common_alt_name'=>$this->input->post('common_alt_name',true),
					'mori_name'=>$this->input->post('maori_name',true),
					'scientific_name'=>$this->input->post('scientific_name',true),
					'national_size'=>$this->input->post('national_size',true),
					'national_daily'=>$this->input->post('national_daily',true),
					'accumulation'=>$this->input->post('accumulation',true),
					'updated_date'=>date('Y-m-d H:i:s'),
					'fish_image_updated'=>date('Y-m-d H:i:s'),
					'fish_image'=>self::$image_small
			);
			if($insert_id = $this->fish_model->add_fish($data))
            {
                $fish_images = $this->session->userdata('fish_images');
                foreach ($fish_images as $fish_image) {
                    $this->fish_model->add_fish_image(array('fish_master_id' => $insert_id,'fish_image'=>$fish_image,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')));
                }
                $this->session->unset_userdata('fish_images');
				$this->session->set_flashdata('flash-message','Fish add successfully');
				redirect('fish');
				exit;
			} else {
				$this->session->set_flashdata('error-message','Fish not added');
				redirect('fish');
				exit;
			}
		}
	}
	// check unique fish
	function unique_fish($fishname){
		if($this->fish_model->get_unique_fish($fishname)==false){
			$this->form_validation->set_message('unique_fish','This fish name already xists...!');
			return false;
		}
	}
	
	// add fish image
	function check_file($str){
		$config['upload_path'] = 'assets/fish/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '500';
		$config['max_width'] = '800';
		$config['max_height'] = '800';
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('fish_image') && $_FILES['fish_image']['name']!=""){
			$this->form_validation->set_message('check_file',  $this->upload->display_errors());
			return FALSE;
		}else{
			$data =  $this->upload->data();
			self::$image_small = $data['file_name'];
			if($data['image_width']< 510 && $data['image_height']< 240){
			}else{
				$config=array();
				$config['image_library'] = 'gd2';
				$config['source_image']	= FCPATH.'assets/fish/'.$data['file_name'];
				$config['maintain_ratio'] = TRUE;
				$config['create_thumb']=FALSE;
				$config['width'] = 510;
				$config['height'] = 240;
				$this->load->library('image_lib', $config); 
				$this->image_lib->resize();
				$this->image_lib->clear();
			}
		}
	}
	
	// edit fish image
	function check_file_edit($str){
		$config['upload_path'] = 'assets/fish/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '500';
		$config['max_width'] = '800';
		$config['max_height'] = '800';
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('fish_image') && $_FILES['fish_image']['name']!=""){
			$this->form_validation->set_message('check_file_edit',  $this->upload->display_errors());
			return FALSE;
		}else{
			if($_FILES['fish_image']['name']!=""){
				$old_image_name=$this->input->post('old_small_image',TRUE);
				if(is_file(FCPATH.'assets/fish/'.$old_image_name)){
					@unlink(FCPATH.'assets/fish/'.$old_image_name);
				}
				$data =  $this->upload->data();
				
				self::$image_small = $data['file_name'];
				self::$image_small_update=true;
				if($data['image_width']< 510 && $data['image_height']< 240){
				}else{
				$config=array();
				$config['image_library'] = 'gd2';
				$config['source_image']	= FCPATH.'assets/'.$data['file_name'];
				$config['create_thumb']=FALSE;
				$config['maintain_ratio'] = TRUE;
				$config['width']	= 510;
				$config['height']	= 240;
				$this->load->library('image_lib', $config); 
				$this->image_lib->resize();
				$this->image_lib->clear();
				}
			}
		}
	}

    /**
     * Method used to export csv for data
     * @return void
     */
    public function export()
    {
        try {
            $fish = $this->fish_model->getAllFishToExport();
            if (count($fish) > 0) {
                generateCsv($fish, 'fish.csv');
                exit();
            } else {
                $this->session->set_flashdata('error-message', 'Unable to find data to export.');
                redirect('fish');
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error-message', 'Something went wrong.');
            redirect('fish');
        }
    }

    public function exportImage() // table fish image download
    {
        try {
            $fish = $this->fish_model->get_all_fish_images();

            $this->load->helper('createzipfile');

            $createzipfile = new createzipfile_helper();

            $duplicate = [];

            foreach ($fish as $fishimage){
                if($fishimage->fish_image !=""  && is_file(FCPATH.'assets/fish/'.$fishimage->fish_image)){
                    $path=FCPATH.'assets/fish/'.$fishimage->fish_image;
                    if(!in_array($fishimage->fish_image,$duplicate)) {
                        $createzipfile->addFile(file_get_contents($path), $fishimage->fish_image);
                        $duplicate[] = $fishimage->fish_image;
                    }
                }
            }

            $rand = rand(0, 9999);
            $zipName = 'api_exports/'.md5(time().$rand).".zip";
            $fd=fopen($zipName, "wb");
            $out=fwrite($fd,$createzipfile->getZippedfile());
            fclose($fd);
            $createzipfile->forceDownload($zipName);
            @unlink($zipName);

            redirect('fish');

        } catch (Exception $exception) {

            $this->session->set_flashdata('error-message', 'Something went wrong.');

            redirect('fish');

        }

    }

}
