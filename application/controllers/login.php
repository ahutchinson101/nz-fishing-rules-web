<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('login_model');
	}
	
	//login page
	public function index(){
		if($this->logged_in()){
			redirect('fish_in_region');
		}else{
			$this->login_form();
		}
	}
	
	// login form
	public function login_form(){
		
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<div class="error_inner"><strong>', '</strong></div>');
		if($this->form_validation->run() == FALSE){
			$data=array();
			$data['title']='Fish Admin Panel';
			$this->load->view('login',$data);
		}else{
			if($this->login_model->login()==true){
				redirect('fish_in_region');
			}else{
				$this->session->set_flashdata('login-error', 'Invalid Username or Password');
				redirect('login/login_form');
			}
		}
	}
	
	// check user login or not
	public function logged_in(){
		if($this->session->userdata('logged_in') == TRUE){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	//logout
	function logout(){
		$datame=array();
		$datame['last_login'] = date('Y-m-d h:i:s');
		$datame['ip_address'] = $_SERVER['REMOTE_ADDR'];
		$this->db->where('username',$this->session->userdata('username'));
		$this->db->update('admin',$datame);
		$this->session->sess_destroy();
		redirect('login');
	}
}
