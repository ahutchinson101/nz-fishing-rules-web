<?php  if(!defined('BASEPATH')) exit('No direct script access allowed');


/*
|--------------------------------------------------------------------------
| Paging Constants
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
| Adding Enclosing Markup
|--------------------------------------------------------------------------
*/

$config['full_tag_open'] = '<ul class="pagination clearfix">';
$config['full_tag_close'] = '</ul>';

/*
|--------------------------------------------------------------------------
| Customizing the First Link
|--------------------------------------------------------------------------
*/

$config['first_link'] = 'FIRST';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';


/*
|--------------------------------------------------------------------------
| Customizing the Last Link
|--------------------------------------------------------------------------
*/
$config['last_link'] = 'LAST';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';

/*
|--------------------------------------------------------------------------
| Customizing the "Next" Link
|--------------------------------------------------------------------------
*/
$config['next_link'] = 'NEXT';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';


/*
|--------------------------------------------------------------------------
| Customizing the "Previous" Link
|--------------------------------------------------------------------------
*/
$config['prev_link'] = 'PREVIOUS';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';



/*
|--------------------------------------------------------------------------
| Customizing the "Current Page" Link
|--------------------------------------------------------------------------
*/
$config['cur_tag_open'] = '<li class="current"><a href="#" >';
$config['cur_tag_close'] = '</a></li>';

/*
|--------------------------------------------------------------------------
| Customizing the "Digit" Link
|--------------------------------------------------------------------------
*/

$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';


/* End of file paging.php */
/* Location: ./system/application/config/paging.php */