<?php
class Restricted_Fish_In_Region_Model extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function get_region_fish($id,$rows,$limit){
		$sql="SELECT fr.id as fish_in_region_id,fr.region_id,fr.fish_id,fr.size,fr.daily,fr.bag,fr.order,fr.part_of_area_bag_limit,fr.ordering,fm.*,ft.id as ft_id,ft.fish_type_name,ft.video_file,r.id as rid,r.name FROM fish_in_region fr LEFT JOIN fish_master fm ON fm.id=fr.fish_id LEFT JOIN fish_type ft ON ft.id=fm.fish_type_id LEFT JOIN region r ON r.id=fr.region_id WHERE fr.region_id='".$id."' ORDER BY fr.order,fm.keyword ASC LIMIT ".$rows.",".$limit;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function get_fish_in_region_count($id){
		$sql="SELECT count(fr.id) as cnt FROM fish_in_region fr LEFT JOIN fish_master fm ON fm.id=fr.id LEFT JOIN fish_type ft ON ft.id=fm.fish_type_id LEFT JOIN region r ON r.id=fr.region_id WHERE fr.region_id='".$id."' ORDER BY fr.ordering ASC";
		$query=$this->db->query($sql);
		$row = $query->row();
        return $row->cnt;
	}

	public function get_one_fish_in_region($id){
		$query=$this->db->get_where('fish_in_region',array('id'=>$id));
		return $query->row();
	}

	public function fish_in_region_update(){
		if($this->input->post('part_of_area_bag_limit')==1){
			$part_of_area_bag_limit='Yes';
		}else{
			$part_of_area_bag_limit='No';
		}
		$fish_data=array(
				'size'=>$this->input->post('size'),
				'daily'=>$this->input->post('daily'),
				'bag'=>$this->input->post('bag'),
				'order'=>$this->input->post('order'),
				'part_of_area_bag_limit'=>$part_of_area_bag_limit,
				'updated_date'=>date('Y-m-d H:i:s')
				);
		$this->db->where('id',$this->input->post('fish_in_region_id'));
		$this->db->where('region_id',$this->input->post('region_id'));
		$this->db->update('fish_in_region',$fish_data);
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}

	public function add_rest_region_fish($data){
		$this->db->insert('restricted_fish_in_region',$data);
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}

	public function add_restricted_fish_in_region($region,$fish,$restricted_msg,$is_restricted_text_bold,$cdate){
		$restricted_data=array(
				'fish_id'=>$fish,
				'region_id'=>$region,
				'restricted_text'=>$restricted_msg,
				'is_restricted_text_bold'=>$is_restricted_text_bold,
				'created_date'=>$cdate
			);
		$this->db->insert('restricted_fish_in_region',$restricted_data);
		$lid=$this->db->insert_id();
		if($this->db->affected_rows()>0){
			$lid=$this->db->insert_id();
			$update_ordering=array(
					'ordering'=>$lid
			);
			$this->db->where('id',$lid);
			$this->db->update('restricted_fish_in_region',$update_ordering);
			return $lid;
		}else{
			return false;
		}
	}

	public function update_restricted_fish_in_region($id,$restricted_msg,$is_restricted_text_bold)
	{
		$update_data=array(
				'restricted_text'=>$restricted_msg,
				'is_restricted_text_bold'=>$is_restricted_text_bold,
				'updated_date'=>date('Y-m-d H:i:s')
		);
		$this->db->where('id',$id);
		$this->db->update('restricted_fish_in_region',$update_data);
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}

	public function update_restricted_fish_in_region_img($id,$restricted_image,$oldimage)
	{
		$update_data=array(
				'restricted_image'=>$restricted_image,
				'res_image_updated'=>date('Y-m-d H:i:s')
		);
		$this->db->where('id',$id);
		$this->db->update('restricted_fish_in_region',$update_data);
		if($this->db->affected_rows()>0){
			if(is_file(FCPATH.'assets/rest/'.$oldimage)){
				@unlink(FCPATH.'assets/rest/'.$oldimage);
			}
			return true;
		}else{
			return false;
		}
	}

	public function add_restricted_fish_in_region_img($region,$fish,$img,$cdate){
		$restricted_data=array(
				'fish_id'=>$fish,
				'region_id'=>$region,
				'restricted_image'=>$img,
				'created_date'=>$cdate
		);
		$this->db->insert('restricted_fish_in_region',$restricted_data);
		$lid=$this->db->insert_id();
		if($this->db->affected_rows()>0){
			$lid=$this->db->insert_id();
			$update_ordering=array(
					'ordering'=>$lid
			);
			$this->db->where('id',$lid);
			$this->db->update('restricted_fish_in_region',$update_ordering);
			return $lid;
		}else{
			return false;
		}
	}

	public function update_restricted_fish_in_region_ordering($key,$id){
		$update_ordering=array(
					'ordering'=>$key,
					'updated_date'=>date('Y-m-d H:i:s')
		);
		$this->db->where('id',$id);
		$this->db->update('restricted_fish_in_region',$update_ordering);
	}

	public function restricted_fish_in_region_delete($id){
		if($id){
			$query=$this->db->get_where("restricted_fish_in_region",array('id'=>$id));
			if($query->num_rows()>0){
				$row=$query->row();
				$data=array('restricted_fish_in_region_id'=>$row->id,'deleted_date'=>date('Y-m-d H:i:s'));
				$this->db->insert('deleted_restricted_fish_in_region',$data);
				$this->db->delete('restricted_fish_in_region', array('id' => $id));
				if($row->restricted_image!="" && is_file(FCPATH.'assets/rest/'.$row->restricted_image)){
					@unlink(FCPATH.'assets/rest/'.$row->restricted_image);
				}
				return true;
			}else{
				return false;
			}
				
			return true;
		}else{
			return false;
		}
	}

	public function get_region($id=0){
		if($id){
			$query=$this->db->get_where('region' ,array('id'=>$id));
			return $query->row();
		}else{
			$query=$this->db->query("SELECT * FROM `region`");
			return $query->result();
		}
	}

	public function delete_fish_in_region($id){
		if($id){
			$query=$this->db->get_where("fish_in_region",array('id'=>$id));
			if($query->num_rows()>0){
				$row=$query->row();
				$data=array('fish_in_region_id'=>$row->id,'deleted_date'=>date('Y-m-d H:i:s'));
				$this->db->insert('deleted_fish_in_region',$data);
				$this->db->delete('fish_in_region', array('id' => $id));
				return true;
			}else{
				return false;
			}
			
			return true;
		}else{
			return false;
		}
	}

	public function not_inserted_fish_in_region($id){
		$sql="SELECT * FROM `fish_master` where id NOT IN(SELECT fish_id from fish_in_region where region_id=".$id.")";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function res_fish_in_region($id){
		$sql="SELECT fir.*,fm.keyword FROM fish_in_region fir LEFT JOIN fish_master fm ON fm.id=fir.fish_id where region_id=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function get_res_fish_detail($fish){
		$sql="select fm.*,ft.fish_type_name from fish_master fm LEFT JOIN fish_type ft ON ft.id=fish_type_id WHERE fm.id=".$fish;
		$query=$this->db->query($sql);
		return $query->row();
	}

	public function get_restricted_fish_detail($region,$fish){
		$sql="select * from restricted_fish_in_region WHERE fish_id='".$fish."' and region_id='".$region."' order by ordering";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function Multi_action($multiaction,$multicontent)
	{
		$ids = $multicontent;
		if($multiaction=='Delete') {
			foreach($ids as $id) {
				$this->delete_fish_in_region($id);
			}
		}
	}

}