<?php
class Alert_In_Region_Model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}

	public function getAllAlertsToExport(){
		$sql="SELECT 
            r.name,
            ar.alert_msg,
            ar.alert_url
                FROM alert_in_region ar LEFT JOIN region r ON r.id=ar.region_id 
                ORDER BY ar.id";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function get_region_alert(){
		$sql="SELECT ar.id as alert_in_region_id,ar.region_id,ar.alert_msg,ar.alert_url,r.id as rid,r.name FROM alert_in_region ar LEFT JOIN region r ON r.id=ar.region_id ORDER BY ar.id";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function get_alert_in_region_count($id){
		$sql="SELECT count(ar.id) as cnt FROM alert_in_region ar LEFT JOIN region r ON r.id=ar.region_id WHERE ar.region_id='".$id."' ORDER BY ar.id ASC";
		$query=$this->db->query($sql);
		$row = $query->row();
        return $row->cnt;
	}

	public function get_one_alert_in_region($id){
		$query=$this->db->get_where('alert_in_region',array('id'=>$id));
		return $query->row();
	}

	public function alert_in_region_update($id,$msg,$alert_url){
		$alert_data=array(
            'alert_msg'=>$msg,
            'alert_url'=>$alert_url,
            'updated_date'=>date('Y-m-d H:i:s')
        );
		$this->db->where('id',$id);
		$this->db->update('alert_in_region',$alert_data);
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}

    public function add_alert_in_region(){
	    $alert_data=array(
            'region_id'=>$this->input->post('region'),
            'alert_msg'=>$this->input->post('region_msg')
        );
		$this->db->insert('alert_in_region',$alert_data);
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}

	public function get_region($id=0){
		if($id){
			$query=$this->db->get_where('region' ,array('id'=>$id));
			return $query->row();
		}else{
			$query=$this->db->query("SELECT * FROM `region`");
			return $query->result();
		}
	}

	public function delete_alert_in_region($id){
		if($id){
			$query=$this->db->get_where("alert_in_region",array('id'=>$id));
			if($query->num_rows()>0){
				$row=$query->row();
				$data=array('alert_in_region_id'=>$row->id,'deleted_date'=>date('Y-m-d H:i:s'));
				$this->db->insert('deleted_alert_in_region',$data);
				$this->db->delete('alert_in_region', array('id' => $id));
				return true;
			}else{
				return false;
			}
			return true;
		}else{
			return false;
		}
	}

	public function not_inserted_fish_in_region($id){
		$sql="SELECT * FROM `fish_master` where id NOT IN(SELECT fish_id from fish_in_region where region_id=".$id.")";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function get_alert_detail($id){
		$sql="select alert_msg from alert_in_region WHERE id=".$id;
		$query=$this->db->query($sql);
		return $query->row();
	}

    public function Multi_action($multiaction,$multicontent)
	{
		$ids = $multicontent;
		if($multiaction=='Delete')
		{
			foreach($ids as $id)
			{
				$this->delete_alert_in_region($id);
			}
		}
	}

}
