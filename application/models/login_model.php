<?php
class Login_Model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}

	public function login(){
		$username=$this->input->post('username',true);
		$password=$this->input->post('password',true);
		$query=$this->db->get_where('admin',array('username'=>$username,'password'=>md5($password),'active'=>1));
		if($query->num_rows()>0){
			$row=$query->row();
			$data=array('user_id'=>$row->id,'email'=>$row->email,'logged_in'=>TRUE,'username'=>$row->username);
			$this->session->set_userdata($data);
			return true;
		}else{
			
			return false;
		}
	}
}