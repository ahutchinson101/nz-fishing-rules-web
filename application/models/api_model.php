<?php
class Api_Model extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

	public function get_fish_type($last_requested_date){
		if($last_requested_date){
			$sql="SELECT * FROM `fish_type` WHERE (`updated_date`>'".$last_requested_date."' OR `created_date`>'".$last_requested_date."')  ORDER BY `ordering` ASC";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT * FROM `fish_type` ORDER BY `ordering` ASC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}

    public function get_fish($last_requested_date){
        if($last_requested_date){
            $sql="SELECT * FROM `fish_master` WHERE (`updated_date`>'".$last_requested_date."' OR `created_date`>'".$last_requested_date."') ORDER BY `id` desc";
            $query=$this->db->query($sql);
        }else{
            $sql="SELECT * FROM `fish_master` ORDER BY `id` DESC";
            $query=$this->db->query($sql);
        }
        return $query->result();
    }

	public function get_fish_gallery($last_requested_date)
    {
		if($last_requested_date){
			$sql="SELECT * FROM `fish_images` WHERE (`updated_at`>'".$last_requested_date."' OR `created_at`>'".$last_requested_date."') ORDER BY `id` desc";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT * FROM `fish_images` ORDER BY `id` DESC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}

	public function get_fish_in_region($last_requested_date){
		if($last_requested_date){
			$sql="SELECT * FROM `fish_in_region` WHERE (`updated_date`>'".$last_requested_date."' OR `created_date`>'".$last_requested_date."') ORDER BY `order` ASC";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT * FROM `fish_in_region` ORDER BY `order` ASC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}

	public function get_fish_in_region_old_dv($last_requested_date){
		if($last_requested_date){
			$sql="SELECT `id`,`fish_id`,`region_id`,`size`,`daily`,`bag`,`order`,`part_of_area_bag_limit`,`created_date`,`updated_date`,`ordering` FROM `fish_in_region` WHERE (`updated_date`>'".$last_requested_date."' OR `created_date`>'".$last_requested_date."') ORDER BY `order` ASC";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT `id`,`fish_id`,`region_id`,`size`,`daily`,`bag`,`order`,`part_of_area_bag_limit`,`created_date`,`updated_date`,`ordering` FROM `fish_in_region` ORDER BY `order` ASC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}
	
	public function get_alert_in_region($last_requested_date){
		if($last_requested_date){
			$sql="SELECT * FROM `alert_in_region` WHERE (`updated_date`>'".$last_requested_date."' OR `created_date`>'".$last_requested_date."') ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT * FROM `alert_in_region` ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}
	
	public function get_restricted_fish_in_region($last_requested_date){
		if($last_requested_date){
			$sql="SELECT * FROM `restricted_fish_in_region` WHERE (`updated_date`>'".$last_requested_date."' OR `created_date`>'".$last_requested_date."') ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT * FROM `restricted_fish_in_region` ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}
	
	public function get_fish_images($last_requested_date){
		if($last_requested_date){
			$sql="SELECT `fish_image` FROM `fish_master` WHERE (`fish_image_updated`>'".$last_requested_date."' OR `created_date`>'".$last_requested_date."') ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT `fish_image` FROM `fish_master` ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}
	
	public function get_restricted_images($last_requested_date){
		if($last_requested_date){
			$sql="SELECT `restricted_image` FROM `restricted_fish_in_region` WHERE (`res_image_updated`>'".$last_requested_date."' OR `created_date`>'".$last_requested_date."') ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT `restricted_image` FROM `restricted_fish_in_region` ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}

	public function get_toxin_alerts($last_requested_date){
		if($last_requested_date){
			$sql="SELECT * FROM `toxin_alerts` WHERE (`updated_date`>'".$last_requested_date."' OR `created_date`>'".$last_requested_date."') ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT * FROM `toxin_alerts` ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}

	public function get_toxin_alerts_images($last_requested_date){
		if($last_requested_date){
			$sql="SELECT `image_url` FROM `toxin_alerts_images` WHERE (`created_date`>'".$last_requested_date."') ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT `image_url` FROM `toxin_alerts_images` ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}

	public function get_closures_and_restrictions($last_requested_date){
		if($last_requested_date){
			$sql="SELECT * FROM `closures_and_restrictions` WHERE (`updated_date`>'".$last_requested_date."' OR `created_date`>'".$last_requested_date."') ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT * FROM `closures_and_restrictions` ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}

	public function get_closures_images($last_requested_date)
    {
		if($last_requested_date){
			$sql="SELECT `image_url` FROM `closures_images` WHERE (`created_date`>'".$last_requested_date."') ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}else{
			$sql="SELECT `image_url` FROM `closures_images` ORDER BY `id` ASC";
			$query=$this->db->query($sql);
		}
		return $query->result();
	}
	
	public function get_api($apiuser,$apipassword){
		if($apiuser && $apipassword){
			$query=$this->db->query("SELECT * FROM `api_users` WHERE `username`='".$apiuser."' AND `password`='".md5($apipassword)."'");
			if($query->num_rows()>0){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function get_deleted_fish_type($last_requested_date){
		if($last_requested_date){
			$query=$this->db->query("SELECT `fish_type_id` FROM deleted_fish_type WHERE `deleted_date`>'".$last_requested_date."'");
			return $query->result();
		}
		return [];
	}

	public function get_deleted_fish($last_requested_date){
		if($last_requested_date){
			$query=$this->db->query("SELECT `fish_id` FROM deleted_fish WHERE `deleted_date`>'".$last_requested_date."'");
			return $query->result();
		}
        return [];
	}

    public function get_deleted_fish_gallery($last_requested_date){
		if($last_requested_date){
			$query=$this->db->query("SELECT `fish_image_id` FROM deleted_fish_image WHERE `deleted_date`>'".$last_requested_date."'");
			return $query->result();
		}
        return [];
	}

    public function get_deleted_fish_in_region($last_requested_date){
		if($last_requested_date){
			$query=$this->db->query("SELECT `fish_in_region_id` FROM deleted_fish_in_region WHERE `deleted_date`>'".$last_requested_date."'");
			return $query->result();
		}
        return [];
	}

    public function get_deleted_alert_in_region($last_requested_date){
		if($last_requested_date){
			$query=$this->db->query("SELECT `alert_in_region_id` FROM deleted_alert_in_region WHERE `deleted_date`>'".$last_requested_date."'");
			return $query->result();
		}
        return [];
	}

    public function get_deleted_restricted_fish_in_region($last_requested_date){
		if($last_requested_date){
			$query=$this->db->query("SELECT `restricted_fish_in_region_id` FROM deleted_restricted_fish_in_region WHERE `deleted_date`>'".$last_requested_date."'");
			return $query->result();
		}
        return [];
	}

    public function get_deleted_closures_and_restrictions($last_requested_date){
		if($last_requested_date){
			$query=$this->db->query("SELECT `closures_and_restrictions_id` FROM deleted_closures_and_restrictions WHERE `deleted_date`>'".$last_requested_date."'");
			return $query->result();
		}
        return [];
	}

    public function get_deleted_closures_images($last_requested_date){
		if($last_requested_date){
			$query=$this->db->query("SELECT `deleted_closures_image` FROM delete_closures_images WHERE `deleted_date`>'".$last_requested_date."'");
			return $query->result();
		}
        return [];
	}

    public function get_deleted_toxin_alerts($last_requested_date){
		if($last_requested_date){
			$query=$this->db->query("SELECT `toxin_alert_id` FROM deleted_toxin_alerts WHERE `deleted_date`>'".$last_requested_date."'");
			return $query->result();
		}
        return [];
	}

    public function get_deleted_toxin_alerts_images($last_requested_date){
		if($last_requested_date){
			$query=$this->db->query("SELECT `deleted_toxin_alerts_image` FROM delete_toxin_alerts_images WHERE `deleted_date`>'".$last_requested_date."'");
			return $query->result();
		}
        return [];
	}
}
