<?php
class Fish_Type_Model extends CI_Model{

	public function __construct(){
		parent::__construct();
		$config['upload_path'] = 'assets/fish_type/';
		$config['allowed_types'] = 'mp4|3gp';
		$config['max_size']	= '10240';
		$this->load->library('upload', $config);
	}
	public function get_fish_type(){
		$query=$this->db->query(" SELECT * FROM fish_type ORDER BY `id` ASC");
		return $query->result();
	}
	public function delete_fish_type($id){
		if($id){
			$fish_type=$this->db->get_where('fish_type',array('id'=>$id));
			if($fish_type->num_rows()>0){
				
				$row=$fish_type->row();
				$data=array('fish_type_id'=>$row->id,'deleted_date'=>date('Y-m-d H:i:s'));
				$this->db->insert('deleted_fish_type',$data);
				
				$fish_master=$this->db->get_where("fish_master",array('fish_type_id'=>$id));
				if($fish_master->num_rows()>0){
					$fm=$fish_master->result();
					foreach ($fm as $fish_m){
						
						if($fish_m->fish_image!="" && is_file(FCPATH.'assets/fish/'.$fish_m->fish_image)){
							@unlink(FCPATH.'assets/fish/'.$fish_m->fish_image);
						}
						//$fm_data=array('fish_id'=>$fish_m->id,'deleted_date'=>date('Y-m-d H:i:s'));
						//$this->db->insert('deleted_fish',$fm_data);
						
						$fish_in_r=$this->db->get_where('fish_in_region',array('fish_id'=>$fish_m->id));
						$fish_in_region=$fish_in_r->result();
						foreach ($fish_in_region as $fin){
							//$fr_data=array('fish_in_region_id'=>$fin->id,'deleted_date'=>date('Y-m-d H:i:s'));
							//$this->db->insert('deleted_fish_in_region',$fr_data);
							$this->db->delete('fish_in_region',array('id'=>$fin->id));
						}
						$this->db->delete('fish_master',array('id'=>$fish_m->id));
					}
				}
				if($row->video_file!="" && is_file(FCPATH.'assets/fish_type/'.$row->video_file)){
					@unlink(FCPATH.'assets/fish_type/'.$row->video_file);
				}
				$this->db->delete('fish_type',array('id'=>$row->id));
				return true;
			}else{
				return false;
			}
			
		}else{
			return false;
		}
	}
	function Multi_action($multiaction,$multicontent)
	{
		$ids = $multicontent;
		if($multiaction=='Delete'){
			foreach($ids as $id){
				$this->delete_fish_type($id);
			}
		}
	}
	public function get_one_fish_type($id){
		$query=$this->db->get_where('fish_type',array('id'=>$id));
		return $query->row();
	}
	public function update_fish_type($id){
		if($id){
			$query=$this->db->get_where('fish_type',array('id'=>$id));
			$fish_type=$query->row();
			if($this->input->post('video_checkbox')=='file'){
				if ( ! $this->upload->do_upload('video_file')){
					$error = array('error' => $this->upload->display_errors());
				}else{
					$video_file = array('upload_data' => $this->upload->data());
					$filename=$video_file['upload_data']['file_name'];
					@unlink(FCPATH.'assets/fish_type/'.$fish_type->video_file);
					$this->db->where('id',$id);
					$this->db->update('fish_type',array('video_url'=>'','video_file'=>$filename,'updated_date'=>date('Y-m-d H:i:s')));
					$update=1;
				}
			}
			$data=array(
				'fish_type_name'=>$this->input->post('fish_type_name',true),
				'major_type'=>$this->input->post('major_type',true),
				'video_title'=>$this->input->post('video_title',true),
				'updated_date'=>date('Y-m-d H:i:s')
			);
			if($this->input->post('video_checkbox')=='url'){
				if(is_file(FCPATH.'assets/fish_type/'.$fish_type->video_file))
				@unlink(FCPATH.'assets/fish_type/'.$fish_type->video_file);
				$data['video_url']=$this->input->post('video_url',true);
				$data['video_file']="";
			}
			$this->db->where('id',$id);
			$this->db->update('fish_type',$data);
			
			if($this->db->affected_rows()>0 || $update==1){
				return true;
			}else{
				return false;
			}
			
		}else{
			return false;
		}
	}
	public function add_fish_type(){
		$filename="";
		if($this->input->post('video_checkbox')=='file'){
			if ( ! $this->upload->do_upload('video_file')){
				$error = array('error' => $this->upload->display_errors());
				$filename="";
			}else{
				$video_file = array('upload_data' => $this->upload->data());
				$filename=$video_file['upload_data']['file_name'];
			}
		}
		
		$data=array(
				'fish_type_name'=>$this->input->post('fish_type_name',true),
				'major_type'=>$this->input->post('major_type',true),
				'video_title'=>$this->input->post('video_title',true),
				'video_file'=>$filename,
				'video_url'=>$this->input->post('video_url',true)
			);
		$this->db->insert('fish_type',$data);
		
		if($this->db->affected_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	public function get_unique_fish_type($fish_type_name){
		$query=$this->db->query("SELECT * FROM `fish_type` where `fish_type_name`='".$fish_type_name."'");
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	}
}