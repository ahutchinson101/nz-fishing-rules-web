<?php
class Fish_In_Region_Model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
	}

	public function getAllFishInRegionToExport(){
		$sql="SELECT fish.keyname, fish.keyword, fish_type.fish_type_name as fish_type, fish.scientific_name, fish.national_size, fish.national_daily, fish_in_region.size, fish_in_region.daily, fish_in_region.bag, fish_in_region.order, fish_in_region.part_of_area_bag_limit, fish_in_region.ordering, fish_in_region.minimum_set_net_mesh_size, fish.common_alt_name, fish.mori_name, region.name as region_name, fish.accumulation FROM fish_in_region JOIN fish_master as fish ON fish.id=fish_in_region.fish_id JOIN region ON region.id=fish_in_region.region_id JOIN fish_type ON fish_type.id=fish.fish_type_id ORDER BY fish_in_region.order, fish.keyword ASC";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function get_region_fish($id,$rows,$limit, $searchTerm = ''){
		$sql="SELECT 
            fr.id as fish_in_region_id,
            fr.region_id,
            fr.fish_id,
            fr.size,
            fr.daily,
            fr.bag,
            fr.order,
            fr.part_of_area_bag_limit,
            fr.ordering,
            fm.*,
            ft.id as ft_id,
            ft.fish_type_name,
            ft.video_file,
            r.id as rid,r.name 
            FROM 
                fish_in_region fr LEFT JOIN fish_master fm ON fm.id=fr.fish_id
                    LEFT JOIN fish_type ft ON ft.id=fm.fish_type_id
                    LEFT JOIN region r ON r.id=fr.region_id 
            WHERE 
                fr.region_id='".$id."'";
        if (!empty($searchTerm)) {
            $sql .= ' AND ( ';
            $sql .= ' fm.keyname LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.keyword LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR ft.fish_type_name LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.scientific_name LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.national_size LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.national_daily LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.size LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.daily LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.bag LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.order LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' ) ';
        }
        $sql .= "ORDER BY fr.order,fm.keyword ASC LIMIT ".$rows.",".$limit;
        $query=$this->db->query($sql);
		return $query->result();
	}

	public function get_fish_in_region_count($id, $searchTerm = ''){
        $sql="SELECT 
            count(fr.id) as cnt
            FROM 
                fish_in_region fr LEFT JOIN fish_master fm ON fm.id=fr.fish_id
                    LEFT JOIN fish_type ft ON ft.id=fm.fish_type_id
                    LEFT JOIN region r ON r.id=fr.region_id 
            WHERE 
                fr.region_id='".$id."'";
        if (!empty($searchTerm)) {
            $sql .= ' AND ( ';
            $sql .= ' fm.keyname LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.keyword LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR ft.fish_type_name LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.scientific_name LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.national_size LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.national_daily LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.size LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.daily LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.bag LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.order LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' ) ';
        }
        $sql .= "ORDER BY fr.order,fm.keyword ASC";
        /*$sql="SELECT
            count(fr.id) as cnt
            FROM fish_in_region fr LEFT JOIN fish_master fm ON fm.id=fr.id
                LEFT JOIN fish_type ft ON ft.id=fm.fish_type_id
                LEFT JOIN region r ON r.id=fr.region_id
            WHERE
                fr.region_id='".$id."'";
        if (!empty($searchTerm)) {
            $sql .= ' AND ( ';
            $sql .= ' fm.keyname LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.keyword LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR ft.fish_type_name LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.scientific_name LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.national_size LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fm.national_daily LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.size LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.daily LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.bag LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' OR fr.order LIKE "%%'.$searchTerm.'%%" ';
            $sql .= ' ) ';
        }
        $sql .= "ORDER BY fr.order,fm.keyword ASC";*/
		$query=$this->db->query($sql);
		$row = $query->row();
        return $row->cnt;
	}

	public function get_one_fish_in_region($id){
		$query=$this->db->get_where('fish_in_region',array('id'=>$id));
		return $query->row();
	}

	public function fish_in_region_update()
    {
        $fish_in_region = $this->get_one_fish_in_region($this->input->post('fish_in_region_id'));

        $regions = $this->input->post('regions');

        $fish_in_region_ids = [];

        $add_new_group = false;
        if(intval($fish_in_region->group_id)) {
            $query = $this->db->get_where('fish_in_region_group',array('id' => $fish_in_region->group_id));
            if($row = $query->row())
            {
                $fish_in_region_ids_db = explode(",",$row->fish_in_region_ids);
                $fish_in_region_ids_found = [];
                foreach ($regions as $region) {
                    $query1 = $this->db->get_where('fish_in_region',array('region_id' => $region,'fish_id' => $fish_in_region->fish_id));
                    if($row1 = $query1->row()) {
                        $fish_in_region_ids_found[] = $row1->id;
                    }
                }
                $to_be_deleted = array_diff($fish_in_region_ids_db, $fish_in_region_ids_found);
                if(count($to_be_deleted)) {
                    foreach ($to_be_deleted as $to_be_deleted_id) {
                        $this->db->delete('fish_in_region', array('id' => $to_be_deleted_id));
                    }
                }
            }
        } else {
            $add_new_group = true;
        }

        //$this->input->post('size')

        foreach ($regions as $region) {
            //$fish_in_region = $this->get_one_fish_in_region($this->input->post('fish_in_region_id'));
            $query = $this->db->get_where('fish_in_region',array('region_id' => $region,'fish_id' => $fish_in_region->fish_id));
            if($row = $query->row()) {

                $fish_data=array(
                    'size' => $this->input->post('size'),
                    'daily' => $this->input->post('daily'),
                    'bag' => (integer) $this->input->post('bag'),
                    'order' => (integer) $this->input->post('order'),
                    'part_of_area_bag_limit' => $this->input->post('part_of_area_bag_limit')==1 ? 'Yes' : 'No',
                    'minimum_set_net_mesh_size' => $this->input->post('minimum_set_net_mesh_size'),
                    'extra_note' => $this->input->post('extra_note'),
                    'updated_date' => date('Y-m-d H:i:s')
                );

                if($fish_in_region->group_id) {
                    $fish_data['group_id'] = $fish_in_region->group_id;
                }

                $this->db->where('id',$row->id);
                $this->db->update('fish_in_region',$fish_data);
                $fish_in_region_ids[] = $row->id;
            } else {
                $fish_data = array(
                    'fish_id' => $fish_in_region->fish_id,
                    'region_id' => $region,
                    'size' => $this->input->post('size'),
                    'daily' => $this->input->post('daily'),
                    'bag' => (integer) $this->input->post('bag'),
                    'order' => (integer) $this->input->post('order'),
                    'part_of_area_bag_limit' => $this->input->post('part_of_area_bag_limit')==1 ? 'Yes' : 'No',
                    'minimum_set_net_mesh_size' => $this->input->post('minimum_set_net_mesh_size'),
                    'extra_note' => $this->input->post('extra_note'),
                    'ordering' => 0,
                    'created_date' => date('Y-m-d H:i:s'),
                    'updated_date' => date('Y-m-d H:i:s')
                );
                if($fish_in_region->group_id) {
                    $fish_data['group_id'] = $fish_in_region->group_id;
                }
                $this->db->insert('fish_in_region',$fish_data);
                $fish_in_region_ids[] = $this->db->insert_id();
            }
        }

        if($add_new_group) {
            $this->db->insert('fish_in_region_group',array('fish_in_region_ids'=>implode(",",$fish_in_region_ids)));
            $group_id = $this->db->insert_id();
            foreach ($fish_in_region_ids as $fish_in_region_id) {
                $this->db->where('id',$fish_in_region_id);
                $this->db->update('fish_in_region',array('group_id' => $group_id));
            }
        } else {
            $this->db->where('id',$fish_in_region->group_id);
            $this->db->update('fish_in_region_group',array('fish_in_region_ids' => implode(",",$fish_in_region_ids)));
        }


		/*$fish_data=array(
            'size' => $this->input->post('size'),
            'daily' => $this->input->post('daily'),
            'bag' => (integer) $this->input->post('bag'),
            'order' => (integer) $this->input->post('order'),
            'part_of_area_bag_limit' => $this->input->post('part_of_area_bag_limit')==1 ? 'Yes' : 'No',
            'minimum_set_net_mesh_size' => $this->input->post('minimum_set_net_mesh_size'),
            'extra_note' => $this->input->post('extra_note'),
            'updated_date' => date('Y-m-d H:i:s')
        );

		$this->db->where('id',$this->input->post('fish_in_region_id'));
		$this->db->where('region_id',$this->input->post('region_id'));
		$this->db->update('fish_in_region',$fish_data);*/
		/*if($this->db->affected_rows()>0){*/
        return true;
		/*}else{
			return false;
		}*/
	}

    public function add_fish_in_region()
    {
        $regions = $this->input->post('regions');
        $fish_in_region_ids = [];
        foreach ($regions as $region)
        {
            $fish_data = array(
                'fish_id' => $this->input->post('fish'),
                'region_id' => $region,
                'size' => $this->input->post('size'),
                'daily' => $this->input->post('daily'),
                'bag' => (integer) $this->input->post('bag'),
                'order' => (integer) $this->input->post('order'),
                'part_of_area_bag_limit' => $this->input->post('part_of_area_bag_limit')==1 ? 'Yes' : 'No',
                'minimum_set_net_mesh_size' => $this->input->post('minimum_set_net_mesh_size'),
                'extra_note' => $this->input->post('extra_note'),
                'ordering' => 0,
                'created_date' => date('Y-m-d H:i:s'),
                'updated_date' => date('Y-m-d H:i:s')
            );
            $this->db->insert('fish_in_region',$fish_data);
            $fish_in_region_ids[] = $this->db->insert_id();
        }

        $this->db->insert('fish_in_region_group',array('fish_in_region_ids'=>implode(",",$fish_in_region_ids)));
        $group_id = $this->db->insert_id();
        foreach ($fish_in_region_ids as $fish_in_region_id) {
            $this->db->where('id',$fish_in_region_id);
            $this->db->update('fish_in_region',array('group_id' => $group_id));
        }
		return $this->db->affected_rows() > 0 ? true : false;
	}

    public function get_region_ids($fish_in_region_ids)
    {
        $fish_in_region_ids = implode("','",$fish_in_region_ids);
        $query=$this->db->query("SELECT region_id FROM `fish_in_region` where id in ('".$fish_in_region_ids."') ");
        $results = $query->result();
        $results_ids = [];
        foreach ($results as $result) {
            $results_ids[] = $result->region_id;
        }
        return $results_ids;
    }

    public function get_group($id)
    {
        $query = $this->db->get_where('fish_in_region_group' ,array('id' => $id));
        return $query->row();
    }

	public function get_region($id=0){
		if($id){
			$query=$this->db->get_where('region' ,array('id'=>$id));
			return $query->row();
		}else{
			$query=$this->db->query("SELECT * FROM `region`");
			return $query->result();
		}
	}

	public function delete_fish_in_region($id){
		if($id){
			$query=$this->db->get_where("fish_in_region",array('id'=>$id));
			if($query->num_rows()>0){
				$row=$query->row();
				$data=array('fish_in_region_id'=>$row->id,'deleted_date'=>date('Y-m-d H:i:s'));
				$this->db->insert('deleted_fish_in_region',$data);
				$this->db->delete('fish_in_region', array('id' => $id));
				return true;
			}
		}
        return false;
	}

	public function not_inserted_fish_in_region($id){
		$sql="SELECT * FROM `fish_master` where id NOT IN(SELECT fish_id from fish_in_region where region_id=".$id.")";
		$query=$this->db->query($sql);
		return $query->result();
	}

    public function not_inserted_fish_in_region_new($regions){
        $regions = implode("','",$regions);
		$sql="SELECT * FROM `fish_master` where id NOT IN(SELECT fish_id from fish_in_region where region_id in ('".$regions."') )";
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function get_fish_detail($id){
		$sql="select fm.*,ft.fish_type_name from fish_master fm LEFT JOIN fish_type ft ON ft.id=fish_type_id WHERE fm.id='".$id."'";
		$query=$this->db->query($sql);
		return $query->row();
	}

    public function Multi_action($multiaction,$multicontent)
	{
		$ids = $multicontent;
		if($multiaction=='Delete') {
			foreach($ids as $id) {
				$this->delete_fish_in_region($id);
			}
		}
	}

}
