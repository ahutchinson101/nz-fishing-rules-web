<?php
class Fish_Model extends CI_Model{

	public function __construct(){
		parent::__construct();
	}

    public function getAllFishToExport(){
        $sql="SELECT fish.keyname, fish.keyword, fish_type.fish_type_name as fish_type, fish.mori_name, fish.scientific_name, fish.national_size, fish.national_daily, fish.common_alt_name, fish.accumulation FROM fish_master as fish JOIN fish_type ON fish_type.id=fish.fish_type_id ORDER BY fish.keyword ASC";
        $query=$this->db->query($sql);
        return $query->result();
    }

	public function get_fish($rows,$limit, $searchTerm = ''){
		//$query=$this->db->query(" SELECT * FROM fish_master ORDER BY `id` ASC LIMIT ".$rows.",".$limit);
        $sql = "SELECT fm.*,ft.fish_type_name FROM fish_master fm LEFT JOIN fish_type ft ON ft.id=fm.fish_type_id ";
        if (!empty($searchTerm)) {
            $sql .= " WHERE
                fm.keyname LIKE '%%".$searchTerm."%%' OR
                fm.keyword LIKE '%%".$searchTerm."%%' OR
                ft.fish_type_name LIKE '%%".$searchTerm."%%' OR
                fm.mori_name LIKE '%%".$searchTerm."%%' OR
                fm.scientific_name LIKE '%%".$searchTerm."%%' OR
                fm.national_size LIKE '%%".$searchTerm."%%' OR
                fm.national_daily LIKE '%%".$searchTerm."%%' ";
        }
        $sql .= " ORDER BY `id` ASC LIMIT ".$rows.",".$limit;
		$query=$this->db->query($sql);
		return $query->result();
	}

    public function get_fish_images($id)
    {
        $sql="SELECT * from fish_images where fish_master_id = '".$id."' ";
        $query=$this->db->query($sql);
        return $query->result();
    }

    public function get_all_fish_images()
    {
        $sql="SELECT `fish_image` FROM `fish_master`";
        $query=$this->db->query($sql);
        return $query->result();
    }

	public function get_fish_count($searchTerm = ''){
        $sql = "SELECT count(fm.id) as cnt FROM fish_master fm LEFT JOIN fish_type ft ON ft.id=fm.fish_type_id ";
        if (!empty($searchTerm)) {
            $sql .= " WHERE
                fm.keyname LIKE '%%".$searchTerm."%%' OR
                fm.keyword LIKE '%%".$searchTerm."%%' OR
                ft.fish_type_name LIKE '%%".$searchTerm."%%' OR
                fm.mori_name LIKE '%%".$searchTerm."%%' OR
                fm.scientific_name LIKE '%%".$searchTerm."%%' OR
                fm.national_size LIKE '%%".$searchTerm."%%' OR
                fm.national_daily LIKE '%%".$searchTerm."%%' ";
        }
        $sql .= " ORDER BY fm.id ASC ";
		$query=$this->db->query($sql);
		$row=$query->row();
		return $row->cnt;
	}

	public function get_fish_type(){
		$query=$this->db->query('SELECT * FROM fish_type');
		return $query->result();
	}

	public function delete_fish_image($id)
    {
        $query=$this->db->get_where("fish_images",array('id'=>$id));
        if($query->num_rows()>0){
            $row=$query->row();

            $data=array('fish_image_id'=>$row->id,'deleted_date'=>date('Y-m-d H:i:s'));
            $this->db->insert('deleted_fish_image',$data);

            if($row->fish_image!="" && is_file(FCPATH.'assets/fish/'.$row->fish_image)){
                @unlink(FCPATH.'assets/fish/'.$row->fish_image);
            }
            $this->db->delete('fish_images', array('id' => $id));
            return true;
        } else {
            return false;
        }
    }

	public function delete_fish($id){
		if($id){
			$query=$this->db->get_where("fish_master",array('id'=>$id));
			if($query->num_rows()>0){
				$row=$query->row();
				$data=array('fish_id'=>$row->id,'deleted_date'=>date('Y-m-d H:i:s'));
				$this->db->insert('deleted_fish',$data);
				
				$fish_in_r=$this->db->get_where('fish_in_region',array('fish_id'=>$row->id));
				$fish_in_region=$fish_in_r->result();
				foreach ($fish_in_region as $fin){
					$fr_data=array(
							'fish_in_region_id'=>$fin->id,
							'deleted_date'=>date('Y-m-d H:i:s')
							);
					$this->db->insert('deleted_fish_in_region',$fr_data);
				}
				if($row->fish_image!="" && is_file(FCPATH.'assets/fish/'.$row->fish_image)){
					@unlink(FCPATH.'assets/fish/'.$row->fish_image);
				}

                $fish_images_q = $this->db->get_where('fish_images',array('fish_master_id'=>$row->id));
                $fish_images = $fish_images_q->result();
                foreach ($fish_images as $fin)
                {
                    $fr_data=array(
                        'fish_image_id'=>$fin->id,
                        'deleted_date'=>date('Y-m-d H:i:s')
                    );
                    $this->db->insert('deleted_fish_image',$fr_data);
                }

				$this->db->delete('fish_images', array('fish_master_id' => $id));
				$this->db->delete('fish_master', array('id' => $id));
				$this->db->delete('fish_in_region', array('fish_id' => $id));
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

    public function Multi_action($multiaction,$multicontent)
	{
		$ids = $multicontent;
		if($multiaction=='Delete')
		{
			foreach($ids as $id)
			{
				$this->delete_fish($id);
			}
		}
	}

	public function get_one_fish($id){
		$query=$this->db->get_where('fish_master',array('id'=>$id));
		return $query->row();
	}

	public function update_fish($id,$data){
		if($id){
			$this->db->where('id',$id);
			$this->db->update('fish_master',$data);
			if($this->db->affected_rows()>0){
				return true;
			}
		}
        return false;
	}

	public function add_fish_image($data){
        $this->db->insert('fish_images',$data);
        $insert_id = $this->db->insert_id();
        if($this->db->affected_rows()>0){
            return $insert_id;
        }else{
            return false;
        }
    }
	public function add_fish($data){
		$this->db->insert('fish_master',$data);
        $insert_id = $this->db->insert_id();
		if($data['fish_image']==""){
			$image_name=$data['keyname'].'.jpg';
			copy(FCPATH.'assets/images/default.jpg', FCPATH.'assets/fish/'.$image_name);
			$this->db->where('id',$insert_id);
			$this->db->update('fish_master',array('fish_image'=>$image_name));
		}
		if($this->db->affected_rows()>0){
			return $insert_id;
		}else{
			return false;
		}
	}

	public function get_unique_fish($fishname){
		$query=$this->db->query("SELECT * FROM `fish_master` where `keyword`='".$fishname."'");
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	}

}
