<?php
class Toxin_Alerts_Model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

    public function getAllToxinAlertsToExport(){
        $sql="SELECT  r.name as region_name, toxin_alerts.type, toxin_alerts.title, toxin_alerts.content, toxin_alerts.date_added, toxin_alerts.key_points FROM toxin_alerts LEFT JOIN region r ON r.id=toxin_alerts.region_id  ORDER BY toxin_alerts.id";
        $query=$this->db->query($sql);
        return $query->result();
    }

	public function get_region($id = 0){
		if($id){
            $query=$this->db->get_where('region' ,array('id'=>$id));
            return $query->row();
        }else{
			$query=$this->db->query("SELECT * FROM `region`");
			return $query->result();
		}
	}

	public function get_region_toxin_alerts_detail($region, $searchTerm){
		$sql="select * from toxin_alerts WHERE region_id='".$region."' ";
        if (!empty($searchTerm)) {
            $sql .= " AND (
                type LIKE '%%".$searchTerm."%%' OR
                title LIKE '%%".$searchTerm."%%' OR
                content LIKE '%%".$searchTerm."%%'
            ) ";
        }
        $sql .= " order by ordering";
		$query=$this->db->query($sql);
		return $query->result();
	}

	function add_toxin_alerts($region,$areas,$title,$date_added,$key_points,$content,$cdate){
		$isinsert=0;
		$alerts_data=array(
			'region_id'=>$region,
			'type'=>$areas,
			'title'=>$title,
			'date_added'=>$date_added,
			'key_points'=>$key_points,
			'content'=>$content,
			'created_date'=>$cdate
		);
		$this->db->insert('toxin_alerts',$alerts_data);
		$lid=$this->db->insert_id();
		if($this->db->affected_rows()>0){
			$lid=$this->db->insert_id();
			$update_ordering=array(
				'ordering'=>$lid
			);
			$this->db->where('id',$lid);
			$this->db->update('toxin_alerts',$update_ordering);
			$isinsert=1;
		}else{
			return false;
		}

		if($isinsert==1){
			if($content!=''){
				$doc = new DOMDocument();
				$doc->loadHTML($content);
				$tags = $doc->getElementsByTagName('img');
				foreach ($tags as $tag) {
					$imgsrc=$tag->getAttribute('src');
					$img_array=explode('/',$imgsrc);
					$img_url=$img_array[count($img_array)-1];

					$query=$this->db->get_where("toxin_alerts_images",array('image_url'=>$img_url));
					if($query->num_rows()>0){
						$row=$query->row();
						$img_count=$row->img_count;
						$data=array('img_count'=>$img_count+1);
						$this->db->where('id',$row->id);
						$this->db->update('toxin_alerts_images',$data);
					}else{
						$data=array('image_url'=>$img_url,'img_count'=>'1','created_date'=>$cdate);
						$this->db->insert('toxin_alerts_images',$data);
					}
				}
			}
		}
		return $lid;
	}

	public function toxin_alert_delete($id){
		if($id){
			$query=$this->db->get_where("toxin_alerts",array('id'=>$id));
			if($query->num_rows()>0){
				$row=$query->row();
				$data=array('toxin_alert_id'=>$row->id,'deleted_date'=>date('Y-m-d H:i:s'));
				$this->db->insert('deleted_toxin_alerts',$data);
				$this->db->delete('toxin_alerts', array('id' => $id));

				if($row->content!=''){
					$doc = new DOMDocument();
					$doc->loadHTML($row->content);
					$tags = $doc->getElementsByTagName('img');
				}else{
					$tags=array();
				}

				foreach ($tags as $tag) {
					$imgsrc=$tag->getAttribute('src');
					$img_array=explode('/',$imgsrc);
					$img_url=$img_array[count($img_array)-1];
					$query=$this->db->get_where("toxin_alerts_images",array('image_url'=>$img_url));
					if($query->num_rows()>0){
						$row=$query->row();
						$img_count=$row->img_count;
						if($img_count>1){
							$data=array('img_count'=>$img_count-1);
							$this->db->where('id',$row->id);
							$this->db->update('toxin_alerts_images',$data);
						}else{
							$data1=array('deleted_toxin_alerts_image'=>$row->image_url,'deleted_date'=>date('Y-m-d H:i:s'));
							$this->db->insert('delete_toxin_alerts_images',$data1);
							$this->db->delete('toxin_alerts_images', array('id' => $row->id));
							//if($row->image_url!="" && is_file(FCPATH.'assets/closure_images/'.$row->image_url)){
							//@unlink(FCPATH.'assets/closure_images/'.$row->image_url);
							//}
						}
					}
				}
				//if($row->restricted_image!="" && is_file(FCPATH.'assets/rest/'.$row->restricted_image)){
				//@unlink(FCPATH.'assets/rest/'.$row->restricted_image);
				//}
				return true;
			}else{
				return false;
			}

			return true;
		}else{
			return false;
		}
	}

	public function update_toxin_alert($id,$areas,$title,$date_added,$key_points,$content,$oldcontent)
	{
		$isupdate=0;
		$update_data=array(
			'type'=>$areas,
			'title'=>$title,
			'date_added'=>$date_added,
			'key_points'=>$key_points,
			'content'=>$content,
			'updated_date'=>date('Y-m-d H:i:s')
		);
		$this->db->where('id',$id);
		$this->db->update('toxin_alerts',$update_data);
		if($this->db->affected_rows()>0){
			$isupdate=1;
		}else{
			return false;
		}

		if($isupdate==1){
			if($oldcontent!=''){
				$doc = new DOMDocument();
				$doc->loadHTML($oldcontent);
				$tags = $doc->getElementsByTagName('img');
			}else{
				$tags=array();
			}
			if($content!=''){
				$doc1 = new DOMDocument();
				$doc1->loadHTML($content);
				$tags1 = $doc1->getElementsByTagName('img');
			}else{
				$tags1=array();
			}

			foreach ($tags as $tag) {
				$imgsrc=$tag->getAttribute('src');
				$flg=0;
				foreach ($tags1 as $tag1) {
					$imgsrc1=$tag1->getAttribute('src');
					if($imgsrc==$imgsrc1){
						$flg=1;
					}
				}
				if($flg!=1){
					$img_array=explode('/',$imgsrc);
					$img_url=$img_array[count($img_array)-1];
					$query=$this->db->get_where("toxin_alerts_images",array('image_url'=>$img_url));
					if($query->num_rows()>0){
						$row=$query->row();
						$img_count=$row->img_count;
						if($img_count>1){
							$data=array('img_count'=>$img_count-1);
							$this->db->where('id',$row->id);
							$this->db->update('toxin_alerts_images',$data);
						}else{
							$data1=array('deleted_toxin_alerts_image'=>$row->image_url,'deleted_date'=>date('Y-m-d H:i:s'));
							$this->db->insert('delete_toxin_alerts_images',$data1);
							$this->db->delete('toxin_alerts_images', array('id' => $row->id));
							//if($row->image_url!="" && is_file(FCPATH.'assets/closure_images/'.$row->image_url)){
							//@unlink(FCPATH.'assets/closure_images/'.$row->image_url);
							//}
						}
					}
				}
			}

			foreach ($tags1 as $tag1) {
				$imgsrc1=$tag1->getAttribute('src');
				$flg=0;
				foreach ($tags as $tag) {
					$imgsrc=$tag->getAttribute('src');
					if($imgsrc1==$imgsrc){
						$flg=1;
					}
				}
				if($flg!=1){
					$img_array=explode('/',$imgsrc1);
					$img_url=$img_array[count($img_array)-1];
					$query=$this->db->get_where("toxin_alerts_images",array('image_url'=>$img_url));
					if($query->num_rows()>0){
						$row=$query->row();
						$img_count=$row->img_count;
						$data=array('img_count'=>$img_count+1);
						$this->db->where('id',$row->id);
						$this->db->update('toxin_alerts_images',$data);
					}else{
						$data=array('image_url'=>$img_url,'img_count'=>'1','created_date'=>date('Y-m-d H:i:s'));
						$this->db->insert('toxin_alerts_images',$data);
					}
				}
			}
		}
		return true;
	}

	public function get_single_toxin_alert_detail($id){
		$sql="select * from toxin_alerts WHERE id='".$id."'";
		$query=$this->db->query($sql);
		return $query->row();
	}

	public function res_fish_in_region($id){
		$sql="SELECT fir.*,fm.keyword FROM fish_in_region fir LEFT JOIN fish_master fm ON fm.id=fir.fish_id where region_id=".$id;
		$query=$this->db->query($sql);
		return $query->result();
	}

	public function get_toxin_alerts(){
		$query=$this->db->query("SELECT * FROM `toxin_alerts`");
		return $query->result();
	}

	public function update_toxin_alerts_ordering($key,$id){
		$update_ordering=array(
			'ordering'=>$key,
			'updated_date'=>date('Y-m-d H:i:s')
		);
		$this->db->where('id',$id);
		$this->db->update('toxin_alerts',$update_ordering);
	}
}
