<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('generateCsv')) {
    function generateCsv($data, $fileName)
    {
        try {
            if (count($data) > 0) {
                $data = json_decode(json_encode($data), true);
                $fields = [];
                $tempFields = array_keys($data[0]);
                foreach ($tempFields as $field) {
                    $fields[] = str_replace('_', ' ', $field);
                }
                array_unshift($data, $fields);

                header("Content-type: text/csv");
                header("Content-Disposition: attachment; filename=".$fileName);
                header("Pragma: no-cache");
                header("Expires: 0");

                $handle = fopen('php://output', 'w+');
                foreach ($data as $values) {
                    $tempValues = array_map(
                        function ($value) {
                            return !empty(trim($value)) ? $value : '-';
                        },
                        array_values($values)
                    );
                    fputcsv($handle, $tempValues);
                }
                fclose($handle);
                return true;
            } else {
                return false;
            }
        } catch (Exception $exception) {
            return false;
        }
    }
}

?>
