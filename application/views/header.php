<!DOCTYPE html>
<!--[if IE 7 ]>   <html lang="en" class="ie7 lte8"> <![endif]--> 
<!--[if IE 8 ]>   <html lang="en" class="ie8 lte8"> <![endif]--> 
<!--[if IE 9 ]>   <html lang="en" class="ie9"> <![endif]--> 
<!--[if gt IE 9]> <html lang="en"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<!--[if lte IE 9 ]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<!-- iPad Settings -->
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" /> 
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, width=device-width">
<!-- iPad End -->
<title><?php echo $title;?> Admin</title>
<link rel="shortcut icon" href="favicon.ico">
<!-- iOS ICONS -->
<link rel="apple-touch-icon" href="touch-icon-iphone.png" />
<link rel="apple-touch-icon" sizes="72x72" href="touch-icon-ipad.png" />
<link rel="apple-touch-icon" sizes="114x114" href="touch-icon-iphone4.png" />
<link rel="apple-touch-startup-image" href="touch-startup-image.png">
<!-- iOS ICONS END -->
<!-- STYLESHEETS -->
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/reset.css" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/grids.css" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/style.css?v=2" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/forms.css" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/jquery.uniform.css" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/css/themes/lightblue/style.css?v=2" />
<!-- jQplot CSS -->
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/lib/jqplot/jquery.jqplot.min.css" />
<link rel="stylesheet" media="screen" href="<?php echo INCLUDE_URL;?>assets/lib/datatables/css/cleanslate.css" />
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />

    <!-- jQplot CSS END -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<style type = "text/css">
    #loading-container {position: absolute; top:50%; left:50%;}
    #loading-content {width:800px; text-align:center; margin-left: -400px; height:50px; margin-top:-25px; line-height: 50px;}
    #loading-content {font-family: "Helvetica", "Arial", sans-serif; font-size: 18px; color: black; text-shadow: 0px 1px 0px white; }
    #loading-graphic {margin-right: 0.2em; margin-bottom:-2px;}
    #loading {background-color: #eeeeee; height:100%; width:100%; overflow:hidden; position: absolute; left: 0; top: 0; z-index: 99999;}
    #sortable { list-style-type: none; margin: 0; padding: 0; }
</style>
<!-- STYLESHEETS END -->
<!--[if lt IE 9]>
<script src="<?php echo INCLUDE_URL;?>assets/js/html5.js"></script>
<script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/js/selectivizr.js"></script>
<![endif]-->
</head>
<body style="overflow: hidden;">
    <div id="loading"> 
        <script type = "text/javascript"> 
            document.write("<div id='loading-container'><p id='loading-content'>" +
                           "<img id='loading-graphic' width='16' height='16' src='<?php echo INCLUDE_URL;?>assets/images/ajax-loader-eeeeee.gif' /> " +
                           "Loading...</p></div>");
        </script> 
    </div> 
    <div id="wrapper">
        <header class="for-header-bg">
            <h1><a href="#"><img src="<?php echo  INCLUDE_URL;?>assets/images/logo.png"></a></h1>
            <nav>
				<ul id="main-navigation" class="clearfix">
					<li class="dropdown <?php if($this->uri->segment(1)=='fish_in_region'){ echo 'active';}?>"> 
						<a href="<?php echo ADMIN_URL?>fish_in_region">Fish In Region</a> 
						<ul><li class="current"><a href="<?php echo ADMIN_URL?>fish_in_region/add">Add New</a></li></ul> 
					</li> 
					<?php /*?><li class="dropdown <?php if($this->uri->segment(1)=='restricted_fish_in_region'){ echo 'active';}?>"> 
						<a href="<?php echo ADMIN_URL?>restricted_fish_in_region">Restricted Area</a> 
					</li><?php */?>
					<li class="dropdown <?php if($this->uri->segment(1)=='fish'){ echo 'active';}?>"> 
						<a href="<?php echo ADMIN_URL?>fish">Fish</a> 
						<ul><li class="current"><a href="<?php echo ADMIN_URL?>fish/add">Add New</a></li></ul> 
					</li> 
					<li class="dropdown <?php if($this->uri->segment(1)=='fish_type'){ echo 'active';}?>"> 
						<a href="<?php echo ADMIN_URL?>fish_type">Fish Type</a> 
						<ul><li class="current"><a href="<?php echo ADMIN_URL?>fish_type/add">Add New</a></li></ul> 
					</li>
					<li class="dropdown <?php if($this->uri->segment(1)=='alert_in_region'){ echo 'active';}?>"> 
						<a href="<?php echo ADMIN_URL?>alert_in_region">Alert In Region</a> 
					</li>
					<li class="dropdown <?php if($this->uri->segment(1)=='closures_and_restrictions'){ echo 'active';}?>"> 
						<a href="<?php echo ADMIN_URL?>closures_and_restrictions">Closures And Restrictions</a> 
					</li>
                    <li class="dropdown <?php if($this->uri->segment(1)=='toxin_alerts'){ echo 'active';}?>">
                        <a href="<?php echo ADMIN_URL?>toxin_alerts">Toxin Alerts</a>
                    </li>

					<li class="fr dropdown">
                        <a href="<?php echo ADMIN_URL?>account" class="with-profile-image"><span><img src="<?php echo INCLUDE_URL;?>assets/images/profile-image.png" /></span>Administrator</a>
                        <ul>
                            <li><a href="<?php echo ADMIN_URL?>account">My Account</a></li>
                            <li><a href="<?php echo ADMIN_URL?>login/logout">Signout</a></li>
                        </ul>
                    </li>
				</ul>
            </nav>
        </header>
        <section>
