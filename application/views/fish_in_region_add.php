<section>
	<header class="container_12 clearfix"><div class="grid_12"><h1>ADD Fish In Region</h1></div></header>
    <section class="container_12 clearfix">
    	<?php if(validation_errors()):?>
			<div class="message error"><h3>Error!</h3><?php echo validation_errors(); ?></div>
		<?php endif;?>
		 <?php if($this->session->flashdata('error-message')):?>
                <div class="message error"> 
                	<h3>Error!</h3> 
                       <p><?php echo $this->session->flashdata('error-message'); ?></p>
                </div>
            <?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"><h3>Success!</h3><p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
       	<?php endif;?>
		<div class="clear"></div>
	   	<form method="post" action="" name="contentform"  id="contentform"  class="form has-validation">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'fish_in_region/check_fish_new';?>" id="check_fish_new_url">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'fish_in_region/check_fish/';?>" id="baseurl">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'fish_in_region/fish_detail/';?>" id="baseurl_fish">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'restricted_fish_in_region/restricted_detail/';?>" id="baseurl_rest">
	   		<div class="portlet grid_12">
			    <header>
			        <h2>Add Fish In Region</h2>
			    </header>
       		<section>

                <div class="clearfix">
                    <label class="form-label">Regions:</label>
                    <div class="form-input">
                        <?php foreach ($region as $region_item): ?>
                        <label><?php echo $region_item->name;?></label>
                        <input type="checkbox" name="regions[]" value="<?php echo $region_item->id;?>">
                        <?php endforeach; ?>
                    </div>
                </div>

				 <div class="clearfix" id="fish_combo" style="display:none;">
					
				</div>
				 <div class="clearfix" id="fish_detail" style="display:none;">
					
				</div>
       		 	<div class="clearfix">
					<label for="size" class="form-label">Size :</label>
					<div class="form-input">
                      	<input type="text" name="size" id="size" value="">
                    </div>
				</div>
				<div class="clearfix">
					<label for="daily" class="form-label">Daily :</label>
					<div class="form-input">
                      	<input type="text" name="daily" id="daily" value="">
                    </div>
				</div>
				<div class="clearfix">
					<label for="order" class="form-label">Order :</label>
					<div class="form-input">
                      	<input type="text" name="order" id="order" value="">
                    </div>
				</div>
				<div class="clearfix">
					<label for="part_of_area_bag_limit" class="form-label">Part of area bag limit:</label>
					<div class="form-input">
                      	<input type="checkbox" name="part_of_area_bag_limit" id="part_of_area_bag_limit" value="1">
                    </div>
				</div>
				<div class="clearfix">
					<label for="bag" class="form-label">Bag:</label>
					<div class="form-input">
                      	<input type="text" name="bag" id="bag" value="">
                    </div>
				</div>
				<div class="clearfix">
					<label for="minimum_set_net_mesh_size" class="form-label">Minimum Set Net Mesh Size :</label>
					<div class="form-input">
                      	<input type="text" name="minimum_set_net_mesh_size" id="minimum_set_net_mesh_size" value="">
                    </div>
				</div>
				<div class="clearfix">
					<label for="extra_note" class="form-label">Notes/Extra:</label>
					<div class="form-input">
						<textarea rows="7" cols="" name="extra_note" id="extra_note"><div style="font-family: Poppins, sans-serif; font-size: 20px; color: #343f45;"><p></p></div></textarea>
                    </div>
				</div>
				<div class="form-action clearfix">
					<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Add New" data-icon-primary="ui-icon-circle-check">Add New</button>
					<button class="button" type="reset">Reset</button>
					<a href="<?php echo ADMIN_URL;?>fish_in_region" class="button approve"><span><span>Back</span></span></a>
				</div>
			</section>
			</div>
		</form>
	</section>
