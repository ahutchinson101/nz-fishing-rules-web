<form method="post" action="#"
      name="closures_restrictionsupdateform"
      id="closures_restrictionsupdateform"
      class="form has-validation" enctype="multipart/form-data">
    <label for="restricted_updatemsg" class="form-label"></label>
    <input type="hidden" value="<?php echo !empty($details) ? $details->id : '' ?>" name="closure_updateid" id="closure_updateid"/>
    <table style="width: 100%">
        <tr style="display: none;">
            <td style="vertical-align:middle;">
                <label for="areas" class="form-label">Areas :</label>
            </td>
            <td>
                <input
                        type="text"
                        name="areas"
                        id="areas"
                        class="areas"
                        style="width: 100%"
                        value="<?php echo !empty($details->type) ? $details->type : '' ?>"/>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: middle;">
                <label for="date_added" class="form-label">Date on :</label>
            </td>
            <td>
                <input
                        type="date"
                        name="date_added"
                        id="date_added"
                        style="width: 100%"
                        value="<?php echo !empty($details->date_added) ? $details->date_added : '' ?>"
                />
            </td>
        </tr>
        <tr>
            <td style="vertical-align: middle;">
                <label for="title" class="form-label">Title :</label>
            </td>
            <td>
                <input
                        type="text"
                        name="title"
                        id="title"
                        style="width: 100%"
                        value="<?php echo !empty($details->title) ? $details->title : '' ?>"
                />
            </td>
        </tr>
        <tr style="display: none;">
            <td style="vertical-align: middle;">
                <label for="title" class="form-label">Key points :</label>
            </td>
            <td>
                <input
                        type="text"
                        name="key_points"
                        id="key_points"
                        style="width: 100%"
                        value="<?php echo !empty($details->key_points) ? $details->key_points : '' ?>"
                />
            </td>
        </tr>
        <tr>
            <td style="vertical-align: middle;">
                <label for="restricted_msg" class="form-label">Description :</label></td>
            <td>
                                                <textarea name="content"
                                                          id="edit-closure-content"
                                                          style="width: 100%; height: 200px"><?php echo !empty($details->content) ? $details->content : '' ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <button class="button" type="submit"
                        name="submitbutton" id="submitbutton"
                        value="Save"
                        data-icon-primary="ui-icon-circle-check">
                    Save
                </button>
                <button class="button close-dialog" type="button" name="close_dialog" value="cancel"
                        data-icon-primary="ui-icon-circle-check">Cancel
                </button>
            </td>
        </tr>
    </table>
</form>
