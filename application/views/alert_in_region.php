<section>
	<header class="container_12 clearfix">
        <div class="grid_10"><h1>Alert Message In Region</h1></div>
        <div class="grid_2" style="text-align: right;">
            <a href="<?php echo ADMIN_URL; ?>alert_in_region/export" class="button add_new"><span><span>Export All</span></span></a>
        </div>
    </header>
    <section class="container_12 clearfix">
    	<div class="clear"></div>
    	<div class="clear" style="height:10px;">&nbsp;</div>
        <?php if(validation_errors()):?>
			<div class="message error"><h3>Error!</h3><?php echo validation_errors(); ?></div>
		<?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"><h3>Success!</h3><p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
       	<?php endif;?>
        <div class="clear"></div>
        	<div class="grid_12">
        		<table class="display" id="example"> 
                   <thead> 
                        <tr>
                            <th align="left" width="20%">Region</th>
							<th align="left" width="50%">Alert Message</th>
							<th align="left" width="30%">Alert URL</th>
							<th align="left">Action</th>
                    	</tr> 
                	</thead> 
                </table>
        		<?php
					$i=1; 
					foreach ($alert_in_region as $alertmsg){
					if($i%2==0){
						$class="even";
					}else{
						$class="odd";
					}?>
					<form method="post" action="<?php echo ADMIN_URL;?>alert_in_region/edit/<?php echo $alertmsg->alert_in_region_id;?>" name="contentform"  id="contentform"  class="form has-validation">
						<table class="display" id="example">
                            <tr class="gradeA <?php echo $class;?>">
                                <td align="left" width="20%"><?php echo $alertmsg->name;?></td>
                                <td align="left" width="50%"><textarea rows="5" cols="60" name="region_msg" id="region_msg_<?php echo $alertmsg->alert_in_region_id;?>"><?php echo $alertmsg->alert_msg;?></textarea></td>
                                <td align="left" width="30%"><textarea rows="2" cols="20" name="alert_url" id="alert_url"><?php echo $alertmsg->alert_url;?></textarea></td>
                                <td style="width:140px;">
                                    <div class="actions_menu">
                                        <button class="button" type="submit" name="submitbutton" id="submitbutton" value="Save" data-icon-primary="ui-icon-circle-check">Save</button>
                                    </div>
                                </td>
                            </tr>
						</table>
					</form>
				<?php $i++;}?>
          	</div>
            <div class="clear"></div>
      	</section>
        <script type="text/javascript">
			//<![CDATA[
			function select_all(){
		        checkboxes = document.getElementsByTagName("input");
				state = true;
	    	    for (i=0; i<checkboxes.length ; i++){
	          		if (checkboxes[i].type == "checkbox"){
	        			checkboxes[i].checked=state;
	        		 $.uniform.update(checkboxes[i]);
			  		}
	       		}
			}
			//]]>
		</script>
        <script type="text/javascript">
			//<![CDATA[
			function unselect_all(){
		        checkboxes = document.getElementsByTagName("input");
    			state = false;
	        	for (i=0; i<checkboxes.length ; i++){
	          		if (checkboxes[i].type == "checkbox"){
	        			checkboxes[i].checked=state;
	        	 		$.uniform.update(checkboxes[i]);
			  		}
	        	}
			}
			//]]>
		</script>
