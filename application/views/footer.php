<footer class="container_12 clearfix">
    <div class="grid_12">Copyright &copy; <?php echo date('Y') ?>.</div>
</footer>
</section>
<!-- Main Section End -->
</section>
</div>
<!-- MAIN JAVASCRIPTS -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src='<?php echo INCLUDE_URL;?>assets/js/jquery.min.js'>\x3C/script>")</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tools/1.2.7/jquery.tools.min.js"></script>
<!--  <script type="text/javascript" src="<?php echo INCLUDE_URL; ?>assets/js/jquery.tools.min.js"></script>-->
<script type="text/javascript" src="<?php echo INCLUDE_URL; ?>assets/js/jquery.uniform.min.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="<?php echo INCLUDE_URL; ?>assets/js/PIE.js"></script>
<script type="text/javascript" src="<?php echo INCLUDE_URL; ?>assets/js/ie.js"></script>
<![endif]-->
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script src="//malsup.github.io/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo INCLUDE_URL; ?>assets/js/global.js"></script>


<script src="<?php echo INCLUDE_URL; ?>assets/tinymce/tinymce.min.js" type="text/javascript"></script>
<script src="<?php echo INCLUDE_URL; ?>assets/tinymce/jquery.tinymce.min.js" type="text/javascript"></script>
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>

<script type="text/javascript" src="<?php echo INCLUDE_URL; ?>assets/js/custom.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function (e) {

        jQuery('.toxin_alert_content').tinymce({
            mode: "none",
            selector: "textarea",
            fontsize_formats: "8px 9px 10px 11px 12px 14px 26px 36px",
            font_formats: "Poppins='Poppins', sans-serif;Open Sans=Open Sans,sans-serif;Andale Mono=andale mono,times;" +
                "Arial=arial,helvetica,sans-serif;" +
                "Arial Black=arial black,avant garde;" +
                "Book Antiqua=book antiqua,palatino;" +
                "Comic Sans MS=comic sans ms,sans-serif;" +
                "Courier New=courier new,courier;" +
                "Georgia=georgia,palatino;" +
                "Helvetica=helvetica;" +
                "Impact=impact,chicago;" +
                "Symbol=symbol;" +
                "Tahoma=tahoma,arial,helvetica,sans-serif;" +
                "Terminal=terminal,monaco;" +
                "Times New Roman=times new roman,times;" +
                "Trebuchet MS=trebuchet ms,geneva;" +
                "Verdana=verdana,geneva;" +
                "Webdings=webdings;" +
                "Wingdings=wingdings,zapf dingbats",
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern select_fish_toxin select_toxin_alerts filemanager"
            ],
            toolbar1: "fontselect |  fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "forecolor | select_fish_toxin select_toxin_alerts filemanager",
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],
            external_filemanager_path: "<?php echo INCLUDE_URL;?>assets/tinymce/plugins/filemanager/",
            filemanager_title: "Responsive Filemanager",
            external_plugins: {"filemanager": "<?php echo INCLUDE_URL;?>assets/tinymce/plugins/filemanager/plugin.min.js"},
            file_picker_callback: function (cb, value, meta) {
                var width = window.innerWidth - 30;
                var height = window.innerHeight - 60;
                if (width > 1800) width = 1800;
                if (height > 1200) height = 1200;
                if (width > 600) {
                    var width_reduce = (width - 20) % 138;
                    width = width - width_reduce + 10;
                }
                var urltype = 2;
                if (meta.filetype == 'image') {
                    urltype = 1;
                }
                if (meta.filetype == 'media') {
                    urltype = 3;
                }
                var title = "RESPONSIVE FileManager";
                if (typeof this.settings.filemanager_title !== "undefined" && this.settings.filemanager_title) {
                    title = this.settings.filemanager_title;
                }
                var akey = "key";
                if (typeof this.settings.filemanager_access_key !== "undefined" && this.settings.filemanager_access_key) {
                    akey = this.settings.filemanager_access_key;
                }
                var sort_by = "";
                if (typeof this.settings.filemanager_sort_by !== "undefined" && this.settings.filemanager_sort_by) {
                    sort_by = "&sort_by=" + this.settings.filemanager_sort_by;
                }
                var descending = "false";
                if (typeof this.settings.filemanager_descending !== "undefined" && this.settings.filemanager_descending) {
                    descending = this.settings.filemanager_descending;
                }
                var fldr = "";
                if (typeof this.settings.filemanager_subfolder !== "undefined" && this.settings.filemanager_subfolder) {
                    fldr = "&fldr=" + this.settings.filemanager_subfolder;
                }
                var crossdomain = "";
                if (typeof this.settings.filemanager_crossdomain !== "undefined" && this.settings.filemanager_crossdomain) {
                    crossdomain = "&crossdomain=1";
                    if (window.addEventListener) {
                        window.addEventListener('message', filemanager_onMessage, false);
                    } else {
                        window.attachEvent('onmessage', filemanager_onMessage);
                    }
                }
                tinymce.activeEditor.windowManager.open({
                    title: title,
                    file: this.settings.external_filemanager_path + 'dialog.php?type=' + urltype + '&descending=' + descending + sort_by + fldr + crossdomain + '&lang=' + this.settings.language + '&akey=' + akey,
                    width: width,
                    height: height,
                    resizable: true,
                    maximizable: true,
                    inline: 1
                }, {
                    setUrl: function (url) {
                        cb(url);
                    }
                });
            }
        });


        jQuery('.closure_content').tinymce({
            mode: "none",
            selector: "textarea",
            fontsize_formats: "8px 9px 10px 11px 12px 14px 26px 36px",
            font_formats: "Poppins='Poppins', sans-serif;Open Sans=Open Sans,sans-serif;Andale Mono=andale mono,times;" +
                "Arial=arial,helvetica,sans-serif;" +
                "Arial Black=arial black,avant garde;" +
                "Book Antiqua=book antiqua,palatino;" +
                "Comic Sans MS=comic sans ms,sans-serif;" +
                "Courier New=courier new,courier;" +
                "Georgia=georgia,palatino;" +
                "Helvetica=helvetica;" +
                "Impact=impact,chicago;" +
                "Symbol=symbol;" +
                "Tahoma=tahoma,arial,helvetica,sans-serif;" +
                "Terminal=terminal,monaco;" +
                "Times New Roman=times new roman,times;" +
                "Trebuchet MS=trebuchet ms,geneva;" +
                "Verdana=verdana,geneva;" +
                "Webdings=webdings;" +
                "Wingdings=wingdings,zapf dingbats",
            theme: "modern",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern selectfish selectregion filemanager"
            ],
            toolbar1: "fontselect |  fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "forecolor | selectfish selectregion filemanager",
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ],
            external_filemanager_path: "<?php echo INCLUDE_URL;?>assets/tinymce/plugins/filemanager/",
            filemanager_title: "Responsive Filemanager",
            external_plugins: {"filemanager": "<?php echo INCLUDE_URL;?>assets/tinymce/plugins/filemanager/plugin.min.js"},
            file_picker_callback: function (cb, value, meta) {
                var width = window.innerWidth - 30;
                var height = window.innerHeight - 60;
                if (width > 1800) width = 1800;
                if (height > 1200) height = 1200;
                if (width > 600) {
                    var width_reduce = (width - 20) % 138;
                    width = width - width_reduce + 10;
                }
                var urltype = 2;
                if (meta.filetype == 'image') {
                    urltype = 1;
                }
                if (meta.filetype == 'media') {
                    urltype = 3;
                }
                var title = "RESPONSIVE FileManager";
                if (typeof this.settings.filemanager_title !== "undefined" && this.settings.filemanager_title) {
                    title = this.settings.filemanager_title;
                }
                var akey = "key";
                if (typeof this.settings.filemanager_access_key !== "undefined" && this.settings.filemanager_access_key) {
                    akey = this.settings.filemanager_access_key;
                }
                var sort_by = "";
                if (typeof this.settings.filemanager_sort_by !== "undefined" && this.settings.filemanager_sort_by) {
                    sort_by = "&sort_by=" + this.settings.filemanager_sort_by;
                }
                var descending = "false";
                if (typeof this.settings.filemanager_descending !== "undefined" && this.settings.filemanager_descending) {
                    descending = this.settings.filemanager_descending;
                }
                var fldr = "";
                if (typeof this.settings.filemanager_subfolder !== "undefined" && this.settings.filemanager_subfolder) {
                    fldr = "&fldr=" + this.settings.filemanager_subfolder;
                }
                var crossdomain = "";
                if (typeof this.settings.filemanager_crossdomain !== "undefined" && this.settings.filemanager_crossdomain) {
                    crossdomain = "&crossdomain=1";
                    if (window.addEventListener) {
                        window.addEventListener('message', filemanager_onMessage, false);
                    } else {
                        window.attachEvent('onmessage', filemanager_onMessage);
                    }
                }
                tinymce.activeEditor.windowManager.open({
                    title: title,
                    file: this.settings.external_filemanager_path + 'dialog.php?type=' + urltype + '&descending=' + descending + sort_by + fldr + crossdomain + '&lang=' + this.settings.language + '&akey=' + akey,
                    width: width,
                    height: height,
                    resizable: true,
                    maximizable: true,
                    inline: 1
                }, {
                    setUrl: function (url) {
                        cb(url);
                    }
                });
            }
        });

        var generateTinymceEditor = jQuery('.generate-tinymce');
        if (generateTinymceEditor.length > 0) {
            generateTinymceEditor.tinymce({
                mode: "none",
                selector: "textarea",
                fontsize_formats: "8px 9px 10px 11px 12px 14px 26px 36px",
                font_formats: "Poppins='Poppins', sans-serif;Open Sans=Open Sans,sans-serif;Andale Mono=andale mono,times;" +
                    "Arial=arial,helvetica,sans-serif;" +
                    "Arial Black=arial black,avant garde;" +
                    "Book Antiqua=book antiqua,palatino;" +
                    "Comic Sans MS=comic sans ms,sans-serif;" +
                    "Courier New=courier new,courier;" +
                    "Georgia=georgia,palatino;" +
                    "Helvetica=helvetica;" +
                    "Impact=impact,chicago;" +
                    "Symbol=symbol;" +
                    "Tahoma=tahoma,arial,helvetica,sans-serif;" +
                    "Terminal=terminal,monaco;" +
                    "Times New Roman=times new roman,times;" +
                    "Trebuchet MS=trebuchet ms,geneva;" +
                    "Verdana=verdana,geneva;" +
                    "Webdings=webdings;" +
                    "Wingdings=wingdings,zapf dingbats",
                theme: "modern",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern selectfish selectregion filemanager"
                ],
                toolbar1: "fontselect |  fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "forecolor | selectfish selectregion filemanager",
                image_advtab: true,
                templates: [
                    {title: 'Test template 1', content: 'Test 1'},
                    {title: 'Test template 2', content: 'Test 2'}
                ],
                external_filemanager_path: "<?php echo INCLUDE_URL;?>assets/tinymce/plugins/filemanager/",
                filemanager_title: "Responsive Filemanager",
                external_plugins: {"filemanager": "<?php echo INCLUDE_URL;?>assets/tinymce/plugins/filemanager/plugin.min.js"},
                file_picker_callback: function (cb, value, meta) {
                    var width = window.innerWidth - 30;
                    var height = window.innerHeight - 60;
                    if (width > 1800) width = 1800;
                    if (height > 1200) height = 1200;
                    if (width > 600) {
                        var width_reduce = (width - 20) % 138;
                        width = width - width_reduce + 10;
                    }
                    var urltype = 2;
                    if (meta.filetype == 'image') {
                        urltype = 1;
                    }
                    if (meta.filetype == 'media') {
                        urltype = 3;
                    }
                    var title = "RESPONSIVE FileManager";
                    if (typeof this.settings.filemanager_title !== "undefined" && this.settings.filemanager_title) {
                        title = this.settings.filemanager_title;
                    }
                    var akey = "key";
                    if (typeof this.settings.filemanager_access_key !== "undefined" && this.settings.filemanager_access_key) {
                        akey = this.settings.filemanager_access_key;
                    }
                    var sort_by = "";
                    if (typeof this.settings.filemanager_sort_by !== "undefined" && this.settings.filemanager_sort_by) {
                        sort_by = "&sort_by=" + this.settings.filemanager_sort_by;
                    }
                    var descending = "false";
                    if (typeof this.settings.filemanager_descending !== "undefined" && this.settings.filemanager_descending) {
                        descending = this.settings.filemanager_descending;
                    }
                    var fldr = "";
                    if (typeof this.settings.filemanager_subfolder !== "undefined" && this.settings.filemanager_subfolder) {
                        fldr = "&fldr=" + this.settings.filemanager_subfolder;
                    }
                    var crossdomain = "";
                    if (typeof this.settings.filemanager_crossdomain !== "undefined" && this.settings.filemanager_crossdomain) {
                        crossdomain = "&crossdomain=1";
                        if (window.addEventListener) {
                            window.addEventListener('message', filemanager_onMessage, false);
                        } else {
                            window.attachEvent('onmessage', filemanager_onMessage);
                        }
                    }
                    tinymce.activeEditor.windowManager.open({
                        title: title,
                        file: this.settings.external_filemanager_path + 'dialog.php?type=' + urltype + '&descending=' + descending + sort_by + fldr + crossdomain + '&lang=' + this.settings.language + '&akey=' + akey,
                        width: width,
                        height: height,
                        resizable: true,
                        maximizable: true,
                        inline: 1
                    }, {
                        setUrl: function (url) {
                            cb(url);
                        }
                    });
                }
            });
        }

        jQuery(".areas").autocomplete({
            source: "<?php echo INCLUDE_URL;?>closures_and_restrictions/get_areas" // path to the get_birds method
        });

        jQuery(document).on('focusin', function (e) {
            if (jQuery(e.target).closest(".mce-window").length) {
                e.stopImmediatePropagation();
            }
        });
    });

</script>
<!--

//-->
</script>
<!-- MAIN JAVASCRIPTS END -->
<!-- LOADING SCRIPT -->
<script>
    $(window).load(function () {
        $("#loading").fadeOut(function () {
            $(this).remove();
            $('body').removeAttr('style');
        });
    });
</script>
<!-- LOADING SCRIPT -->
<!-- jQplot SETUP -->
<!--[if lt IE 9]>
    <script type="text/javascript" src="<?php echo INCLUDE_URL; ?>assets/lib/jqplot/excanvas.js"></script>
    <![endif]-->
<!-- jQplot SETUP END -->
</body>
</html>
