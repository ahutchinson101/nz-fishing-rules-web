<section>
	<header class="container_12 clearfix"><div class="grid_12"><h1>ADD Restricted Areas Fish In Region</h1></div></header>
	<section class="container_12 clearfix">
		<div class="message error" style="display: none"><h3>Error!</h3></div>
    	 <?php if($this->session->flashdata('error-message')):?>
                <div class="message error"> 
                	<h3>Error!</h3> 
                       <p><?php echo $this->session->flashdata('error-message'); ?></p>
                </div>
            <?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"><h3>Success!</h3><p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
       	<?php endif;?>
		<div class="clear"></div>
		<div class="form">
			<input type="hidden" value="<?php echo ADMIN_URL.'restricted_fish_in_region/add_text/';?>" id="baseurltext">
			<input type="hidden" value="<?php echo ADMIN_URL.'restricted_fish_in_region/add_image/';?>" id="baseurlimage">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'restricted_fish_in_region/check_fish/';?>" id="baseurl">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'restricted_fish_in_region/fish_detail/';?>" id="baseurl_fish">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'restricted_fish_in_region/restricted_detail/';?>" id="baseurl_rest">
	   		<div class="portlet grid_12">
			    <header>
			        <h2>Add Restricted Areas Fish In Region</h2>
			    </header>
       		<section>
       		 	<div class="clearfix">
					<label for="add_fish_region" class="form-label">Region :</label>
					<div class="form-input">
					<?php $style='id="add_fish_region"';?>
                      	<?php echo form_dropdown('region_res',$region,'',$style);?>
                    </div>
				</div>
				 <div class="clearfix" id="fish_combo" style="display:none;">
					
				</div>
				<div class="clearfix" id="fish_detail" style="display:none;">
				
				</div>
       		 	<div class="clearfix" id="restricted_toggle" style="display:none;">
       				<label for="bag" class="form-label">Is Restricted Areas:</label>
					<div class="form-input">
						<div id="radioset">
						   <input type="radio" id="yes" class="is_restricted_areas noUniform" name="radio"><label for="yes">Yes</label>
						   <input type="radio" id="no" class="is_restricted_areas noUniform" name="radio" checked="checked"><label for="no">No</label>
						</div>
						 <!-- <select name="is_restricted_areas" id="is_restricted_areas">
                      		<option value="Yes">Yes</option>
                      		<option value="No" selected="selected">No</option>
                      	</select> -->
                    </div>
				</div>
			<div class="clearfix" id="restricted_detail" style="display:none;">
				<div class="form-input restricted_detailstyle">
					<div class="clearfix add_rest" id="restricted_detail_display">
						<nav>
							<ul id="main-navigation" class="clearfix">
								<span>Restricted Areas Fish In Region</span>
								<li class="dropdown"> 
									<img alt="Add New" width="15" src="<?php echo  ADMIN_URL;?>assets/images/add.png">
									<ul><li class="current"><a href="#" id="add_text">Add Text</a></li>
										<li class="current"><a href="#" id="add_image">Add Image</a></li>
									</ul> 
								</li> 
							</ul>
						</nav>
					</div>
					<div id="restricted_imagediv" title="Image" style="display:none;">
						<form method="post" action="<?php echo ADMIN_URL.'restricted_fish_in_region/add_image';?>" name="restrictedimageform"  id="restrictedimageform"  class="" enctype="multipart/form-data">
							<input type="hidden" name="region" id="region" value=""/>
							<input type="hidden" name="fishid" id="fishid" value=""/>
							<table style="margin: 40px; ">
							<tr>
							<td><label for="restricted_image" class="form-label">Image :</label></td>
							<td><input type="file" name="restricted_image" value=""></td>
							</tr>
							<tr>
							<td></td>
							<td><small><em>Allowed maximmum size 1.5 MB,maxwidth 1600px</em></small></td>
							</tr>
							<tr>
							<td></td>
							<td  style="padding-top: 10px;">
								<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Upload" data-icon-primary="ui-icon-circle-check">Upload</button>
			              		<button class="button" type="button" name="cancelbuttonimage" id="cancelbuttonimage" value="cancel" data-icon-primary="ui-icon-circle-check">Cancel</button>
			              	</td>
							</tr>
							</table>
			           	</form>
					</div>
					
					<div id="restricted_textdiv" title="Detail" style="display:none;">
						<form method="post" action="#" name="restrictedtextform"  id="restrictedtextform"  class="" enctype="multipart/form-data">
							<table>
							<tr>
								<td style="vertical-align: middle;"><label for="restricted_msg" class="form-label">Message :</label></td>
								<td><textarea name="restricted_msg" id="restricted_msg" cols="50" rows="8"></textarea></td>
							</tr>
							<tr>
								<td style="vertical-align: middle; padding-bottom: 5px;"><label for="restricted_msg" class="form-label">Is bold :</label></td>
								<td><input type="checkbox" name="is_restricted_text_bold_add" id="is_restricted_text_bold_add"></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Save" data-icon-primary="ui-icon-circle-check">Save</button>
					                <button class="button" type="button" name="cancelbuttontext" id="cancelbuttontext" value="cancel" data-icon-primary="ui-icon-circle-check">Cancel</button>	
								</td>
							</tr>
							</table>
			            </form>
					</div>
					<div class="clearfix" id="restricted_data">
					</div>
				</div>
			</div>
				
			</section>
			</div>
		</div>
</section>