<html>

<head>
<style>
.toxin_alerts_combo{
	width:295px;
	height:28px;
	border: 1px solid #b1b1b1;
	border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25) rgba(0,0,0,0.25);
	position: relative;
	text-shadow: 0 1px 1px rgba(255,255,255,0.75);
	display: inline-block;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	background-color: #f0f0f0;
	background-image: -moz-linear-gradient(top, #fff, #d9d9d9);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fff), to(#d9d9d9));
	background-image: -webkit-linear-gradient(top, #fff, #d9d9d9);
	background-image: -o-linear-gradient(top, #fff, #d9d9d9);
	background-image: linear-gradient(to bottom, #fff, #d9d9d9);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffd9d9d9', GradientType=0);
	zoom: 1;
}
.toxin_link_submit{
	width: 50px;
	height: 28px;
	background-color: #006dcc;
	min-width: 50px;
	color: #fff;
	border: 1px solid #b1b1b1;
	position: relative;
	text-shadow: 0 1px 1px rgba(255,255,255,0.75);
	display: inline-block;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25) rgba(0,0,0,0.25);
	background-color: #006dcc;
	background-image: -moz-linear-gradient(top, #08c, #04c);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#08c), to(#04c));
	background-image: -webkit-linear-gradient(top, #08c, #04c);
	background-image: -o-linear-gradient(top, #08c, #04c);
	background-image: linear-gradient(to bottom, #08c, #04c);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0044cc', GradientType=0);
	zoom: 1;
}
.toxin_link_submit:hover{
	background-color: #005fb3;
	background-image: -moz-linear-gradient(top, #0077b3, #003cb3);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#0077b3), to(#003cb3));
	background-image: -webkit-linear-gradient(top, #0077b3, #003cb3);
	background-image: -o-linear-gradient(top, #0077b3, #003cb3);
	background-image: linear-gradient(to bottom, #0077b3, #003cb3);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0077b3', endColorstr='#ff003cb3', GradientType=0);
	zoom: 1;
}
.toxin_link_cancel{
	width: 70px;
	height: 28px;
	border: 1px solid #b1b1b1;
	border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25) rgba(0,0,0,0.25);
	position: relative;
	text-shadow: 0 1px 1px rgba(255,255,255,0.75);
	display: inline-block;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	background-color: #f0f0f0;
	background-image: -moz-linear-gradient(top, #fff, #d9d9d9);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fff), to(#d9d9d9));
	background-image: -webkit-linear-gradient(top, #fff, #d9d9d9);
	background-image: -o-linear-gradient(top, #fff, #d9d9d9);
	background-image: linear-gradient(to bottom, #fff, #d9d9d9);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffd9d9d9', GradientType=0);
	zoom: 1;
}
.toxin_link_cancel:hover{
	color: #333;
	background-color: #e3e3e3;
	background-image: -moz-linear-gradient(top, #f2f2f2, #ccc);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f2f2f2), to(#ccc));
	background-image: -webkit-linear-gradient(top, #f2f2f2, #ccc);
	background-image: -o-linear-gradient(top, #f2f2f2, #ccc);
	background-image: linear-gradient(to bottom, #f2f2f2, #ccc);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff2f2f2', endColorstr='#ffcccccc', GradientType=0);
	zoom: 1;
}
</style>
</head>
<body>
    <table>
	<tr>
		<td>
		<?php $style='id="toxin_alerts_list" class="toxin_alerts_combo"';?>
	    Select Toxin Alert : <?php echo form_dropdown('toxin_alerts_list',$toxin_alerts,'',$style);?>
		</td>
	</tr>
	<tr style="float: right;">
		<td>
			<input type="submit" name="submit" value="Ok" id="submit-toxin-link" class="toxin_link_submit">
			<input type="button" name="cancel" Value="Cancel" id="close-toxin_combo" class="toxin_link_cancel">
		</td>
		
	</tr>
	</table>
</body>
 <script>
    document.getElementById("submit-toxin-link").onclick = function(){
       function isOnlyTextSelected(anchorElm) {
			var html = window.parent.tinyMCE.activeEditor.selection.getContent();

			// Partial html and not a fully selected anchor element
			if (/</.test(html) && (!/^<a [^>]+>[^<]+<\/a>$/.test(html) || html.indexOf('href=') == -1)) {
				return false;
			}

			if (anchorElm) {
				var nodes = anchorElm.childNodes, i;

				if (nodes.length === 0) {
					return false;
				}

				for (i = nodes.length - 1; i >= 0; i--) {
					if (nodes[i].nodeType != 3) {
						return false;
					}
				}
			}

			return true;
		}

       	var toxin_alert_id = document.getElementById("toxin_alerts_list").value;
		var href = 'nzfishing/#toxin_id='+toxin_alert_id;
       	var selection  = window.parent.tinyMCE.activeEditor.selection;
       	var dom = window.parent.tinyMCE.activeEditor.dom;
       	var data = {};
       	selectedElm = selection.getNode();
       	anchorElm = dom.getParent(selectedElm, 'a[href]');
       	onlyText = isOnlyTextSelected(anchorElm);
       	data.text = initialText = anchorElm ? (anchorElm.innerText || anchorElm.textContent) : selection.getContent({format: 'text'});
       	data.href = anchorElm ? dom.getAttrib(anchorElm, 'href') : '';

       	var linkAttrs = {href: href};
       			
       	if(anchorElm) 
       	{
       		window.parent.tinyMCE.activeEditor.focus();

       		if (onlyText && data.text != initialText) {
       			if ("innerText" in anchorElm) {
       				anchorElm.innerText = data.text;
       			} else {
       				anchorElm.textContent = data.text;
       			}
       		}

       		dom.setAttribs(anchorElm, linkAttrs);

       		selection.select(anchorElm);
       		window.parent.tinyMCE.activeEditor.undoManager.add();
       	} else {
       		if (onlyText) {
       			window.parent.tinyMCE.activeEditor.insertContent(dom.createHTML('a', linkAttrs, dom.encode(data.text)));
       		} else {
       			window.parent.tinyMCE.activeEditor.execCommand('mceInsertLink', false, linkAttrs);
       		}
       	}

       	tinyMCEPopup.close();
       	
    };
    document.getElementById("close-toxin_combo").onclick = function(){
    	tinyMCEPopup.close();
    };
</script>
</html>


