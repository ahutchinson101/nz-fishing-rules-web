<section>
	<header class="container_12 clearfix">
        <div class="grid_10"><h1>Toxin Alerts</h1></div>
        <div class="grid_2" style="text-align: right;">
            <a href="<?php echo ADMIN_URL; ?>toxin_alerts/export" class="button add_new"><span><span>Export All</span></span></a>
        </div>
    </header>
	<section class="container_12 clearfix">
		<div class="message error" style="display: none"><h3>Error!</h3></div>
    	 <?php if($this->session->flashdata('error-message')):?>
                <div class="message error"> 
                	<h3>Error!</h3> 
                       <p><?php echo $this->session->flashdata('error-message'); ?></p>
                </div>
            <?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"><h3>Success!</h3><p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
       	<?php endif;?>
		<div class="clear"></div>
		<div class="form">
			<input type="hidden" value="<?php echo ADMIN_URL.'toxin_alerts/add_toxin_alerts/';?>" id="baseurltext">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'toxin_alerts/check_fish/';?>" id="baseurl">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'toxin_alerts/alert_detail/';?>" id="baseurl_alerts">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'toxin_alerts/alert_list/';?>" id="alertslist_url">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'toxin_alerts/region_list/';?>" id="regionurl_alerts">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'toxin_alerts/alerts_imageurl/';?>" id="alerts_imageurl">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'toxin_alerts/alert_update/';?>" id="alerts_upadteurl">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'toxin_alerts/alert_delete/';?>" id="alerts_deleteurl">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'toxin_alerts/alertsrestorder/';?>" id="alerts_sorturl">
	   		<div class="portlet grid_12">
			    <header>
			        <h2>Toxin Alerts</h2>
			    </header>
       		<section>
       		 	<div class="clearfix">
       		 		<label for="toxin_alerts_fish_region" class="form-label">Region :</label>
					<div class="form-input">
					<?php $style='id="toxin_alerts_fish_region"';?>
                      	<?php echo form_dropdown('region_res',$region,'',$style);?>
                    </div>
				</div>
			</section>
			</div>
		</div>
</section>
