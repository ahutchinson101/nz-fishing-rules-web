<section>
	<header class="container_12 clearfix">
	    <div class="grid_12">
	        <h1>My Account</h1>
        </div>
    </header>
    <section class="container_12 clearfix">
	    <?php if(validation_errors()):?>
			<div class="message error"> 
		       	<h3>Error!</h3> 
	    	    <?php echo validation_errors(); ?>
	        </div>
		<?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"> 
            	<h3>Success!</h3> 
                <p><?php echo $this->session->flashdata('flash-message'); ?></p> 
            </div>
        <?php endif;?>
		<?php if($this->session->flashdata('error-message')):  ?>
			<div class="message error">
            	<h3>Failed!</h3>
                <p><?php echo $this->session->flashdata('error-message'); ?></p>
            </div>
        <?php endif;?>
        <div class="clear"></div>
	    <div class="portlet grid_12">
			<header>
				<h2>My Account</h2>
			</header>
			<section>
				<form method="post" action="" name="contentform" enctype="multipart/form-data" id="contentform"  class="form has-validation">
					<div class="clearfix">
				    	<label for="username" class="form-label">Username : <em>*</em><small>your username</small></label>
				        <div class="form-input"><input type="text" id="username" name="username" required="required" value="<?php echo $user->username?>" /></div>
				    </div>
				    <div class="clearfix">
				    	<label for="password" class="form-label">Password <em>*</em><small>Change this field to change password.</small></label>
				        <div class="form-input"><input type="password" id="password" name="password" value="<?php echo set_value('password');?>"  /></div>
				    </div>
				    <div class="clearfix">
				    	<label for="passwordagain" class="form-label">Password (Again) <em>*</em><small>Type password again</small></label>
				        <div class="form-input"><input type="password" id="passwordagain" name="passwordagain" value="<?php echo set_value('passwordagain');?>" data-equals="password"  /></div>
				    </div>
				    <div class="clearfix">
				    	<label for="email" class="form-label"><?php echo $this->lang->line('cp_email');?> <em>*</em><small>Your Email.</small></label>
				        <div class="form-input"><input type="email" id="email" name="email" value="<?php echo $user->email;?>" required="required" /></div>
				   	</div>
				    <div class="form-action clearfix">
				    	<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Save" data-icon-primary="ui-icon-circle-check">Save</button>
				        <button class="button" type="reset">Reset</button>
				    </div>
					<input type="hidden" name="user_id" value="<?php echo $user->id;?>" />	
				</form>
			</section>
		</div>
       	<div class="clear"></div>
     </section>
