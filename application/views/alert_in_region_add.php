<section>
	<header class="container_12 clearfix"><div class="grid_12"><h1>ADD Alert In Region</h1></div></header>
    <section class="container_12 clearfix">
    	<?php if(validation_errors()):?>
			<div class="message error"><h3>Error!</h3><?php echo validation_errors(); ?></div>
		<?php endif;?>
		 <?php if($this->session->flashdata('error-message')):?>
                <div class="message error"> 
                	<h3>Error!</h3> 
                       <p><?php echo $this->session->flashdata('error-message'); ?></p>
                </div>
            <?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"><h3>Success!</h3><p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
       	<?php endif;?>
		<div class="clear"></div>
	   	<form method="post" action="" name="contentform"  id="contentform"  class="form has-validation">
	   		<div class="portlet grid_12">
			    <header>
			        <h2>Add Alert In Region</h2>
			    </header>
       		<section>
       		 	<div class="clearfix">
					<label for="add_fish_region" class="form-label">Region :</label>
					<div class="form-input">
					<?php $style='id="add_fish_region"';?>
                      	<?php echo form_dropdown('region',$region,'',$style);?>
                    </div>
				</div>
				<div class="clearfix">
					<label for="region_msg" class="form-label">Region Message :</label>
					<div class="form-input"><textarea rows="10" cols="" name="region_msg" id="region_msg" ></textarea></div>
				</div>
				<div class="form-action clearfix">
					<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Add New" data-icon-primary="ui-icon-circle-check">Add New</button>
					<button class="button" type="reset">Reset</button>
					<a href="<?php echo ADMIN_URL;?>alert_in_region" class="button approve"><span><span>Back</span></span></a>
				</div>
			</section>
			</div>
		</form>
	</section>