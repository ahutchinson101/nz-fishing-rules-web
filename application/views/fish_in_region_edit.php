	<style type="text/css">
.second-detail{float:left; width:600px; padding:0 0 10px 0;}
.second-detail .new-label {
    float: left;
    font-size: 12px;
    font-weight: bold;
    margin: 0;
    text-align: right;
    width: 200px;
}
.second-detail .new-form-input {
	float:left;
	width:250px;
	font-size: 12px;
	padding: 0 0 0 7px;
    margin: 0 0 0 0;
}	
</style>
<section>
	<header class="container_12 clearfix"><div class="grid_12"><h1>Edit Fish In Region</h1></div></header>
    <section class="container_12 clearfix">
    	<?php if(validation_errors()):?>
			<div class="message error"><h3>Error!</h3><?php echo validation_errors(); ?></div>
		<?php endif;?>
		 <?php if($this->session->flashdata('error-message')):?>
                <div class="message error"> 
                	<h3>Error!</h3> 
                       <p><?php echo $this->session->flashdata('error-message'); ?></p>
                </div>
            <?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"><h3>Success!</h3><p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
       	<?php endif;?>
		<div class="clear"></div>
	   	<form method="post" action="" name="contentform"  id="contentform"  class="form has-validation">
	   		<input type="hidden" name="fish_in_region_id" value="<?php echo $fish_in_region->id;?>">
	   		<input type="hidden" name="region_id" value="<?php echo $fish_in_region->region_id;?>">
	   		<div class="portlet grid_12">
			    <header>
			        <h2>Edit Fish In Region</h2>
			    </header>
       		 <section>
       		 <div class="clearfix">
       		 <div class=second-detail>
					<label for="size" class="new-label">Region :</label>
					<div class="new-form-input"><?php echo $region->name;?></div>
				</div>
				<div class=second-detail>
					<label for="size" class="new-label">Keyname :</label>
					<div class="new-form-input"><?php echo $fish->keyname;?></div>
				</div>
				<div class="second-detail">
					<label for="size" class="new-label">Keyword:</label>
						<div class="new-form-input"><?php echo $fish->keyword;?></div>
				</div>
				<div class="second-detail">
					<label for="size" class="new-label">Fish Type :</label>
						<div class="new-form-input"><?php echo $fish->fish_type_name;?></div>
				</div>
				<div class="second-detail">
					<label for="size" class="new-label">Common Alternate Name :</label>
						<div class="new-form-input"><?php echo $fish->common_alt_name;?></div>
				</div>
				<div class="second-detail">
					<label for="size" class="new-label">Maori Name :</label>
						<div class="new-form-input"><?php echo $fish->mori_name;?></div>
				</div>
				<div class="second-detail">
					<label for="size" class="new-label">Scientific Name :</label>
						<div class="new-form-input"><?php echo $fish->scientific_name;?></div>
				</div>
				<div class="second-detail">
					<label for="size" class="new-label">National Size :</label>
						<div class="new-form-input"><?php echo $fish->national_size;?></div>
				</div>
				<div class="second-detail">
					<label for="size" class="new-label">National Daily :</label>
						<div class="new-form-input"><?php echo $fish->national_daily;?></div>
				</div>
       		 	</div>
                 <div class="clearfix">
                     <label class="form-label">Regions:</label>
                     <div class="form-input">
                         <?php foreach ($regions as $region_item): ?>
                             <label><?php echo $region_item->name;?></label>
                             <input type="checkbox" name="regions[]"  <?php if(in_array($region_item->id,$region_ids)): ?> checked <?php endif;?> value="<?php echo $region_item->id;?>">
                         <?php endforeach; ?>
                     </div>
                 </div>
       		 	<div class="clearfix">
					<label for="size" class="form-label">Size :</label>
					<div class="form-input">
                      	<input type="text" name="size" id="size" value="<?php echo $fish_in_region->size;?>">
                    </div>
				</div>
				<div class="clearfix">
					<label for="daily" class="form-label">Daily :</label>
					<div class="form-input">
                      	<input type="text" name="daily" id="daily" value="<?php echo $fish_in_region->daily;?>">
                    </div>
				</div>
				<div class="clearfix">
					<label for="order" class="form-label">Order :</label>
					<div class="form-input">
                      	<input type="text" name="order" id="order" value="<?php echo $fish_in_region->order;?>">
                    </div>
				</div>
				<div class="clearfix">
					<label for="part_of_area_bag_limit" class="form-label">Part of area bag limit:</label>
					<div class="form-input">
                      	<input type="checkbox" name="part_of_area_bag_limit" id="part_of_area_bag_limit" value="1" <?php if(strtolower($fish_in_region->part_of_area_bag_limit)=='yes'){?>checked="checked"<?php }?> >
                    </div>
				</div>
				<div class="clearfix">
					<label for="bag" class="form-label">Bag:</label>
					<div class="form-input">
                      	<input type="text" name="bag" id="bag" value="<?php echo $fish_in_region->bag; ?>">
                    </div>
				</div>
				<div class="clearfix">
					<label for="minimum_set_net_mesh_size" class="form-label">Minimum Set Net Mesh Size :</label>
					<div class="form-input">
                      	<input type="text" name="minimum_set_net_mesh_size" id="minimum_set_net_mesh_size" value="<?php echo $fish_in_region->minimum_set_net_mesh_size; ?>">
                    </div>
				</div>
				<div class="clearfix">
					<label for="extra_note" class="form-label">Notes/Extra:</label>
					<div class="form-input">
						<textarea rows="7" cols="" name="extra_note" id="extra_note"><?php echo $fish_in_region->extra_note; ?></textarea>
                    </div>
				</div>
				<div class="form-action clearfix">
					<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Save" data-icon-primary="ui-icon-circle-check">Save</button>
					<button class="button" type="reset">Reset</button>
					<a href="<?php echo ADMIN_URL;?>fish_in_region" class="button approve"><span><span>Back</span></span></a>
				</div>
			</section>
			</div>
		</form>
	</section>
