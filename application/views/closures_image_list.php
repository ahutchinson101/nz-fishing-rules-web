<html>

<head>
<script type="text/javascript" src="<?php echo INCLUDE_URL;?>assets/tinymce/plugins/compat3x/tiny_mce_popup.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(e) {
	var options = { 
	  	beforeSubmit:  showRequest,  // pre-submit callback 
	    success:       showResponse  // post-submit callback 
	};
	jQuery('#closuresimageform').ajaxForm(options);
});
function showRequest(formData, jqForm, options) 
{
	var queryString = jQuery.param(formData);
	return true; 
} 
	 
function showResponse(responseText, statusText, xhr, $form)  
{ 
	if(responseText==3)
	{
		jQuery(".message").css("display","block");
		jQuery('.message').append('Select Region and Fish.');
	}
	if(responseText==0)
	{
		jQuery(".message").css("display","block");
		jQuery('.message').append('Select File.');
	}
	else if(responseText==1)
	{
		jQuery(".message").css("display","block");
		jQuery('.message').append('File Upload Error.');
	}
	else
	{
		jQuery("#nodata").remove();
		jQuery(".message").css("display","none");
		jQuery(".restricted_detail_item").append(responseText);
		jQuery("#restricted_imagediv").dialog("close");
	}
}
</script>
<style>
.closure_fish_combo{
	width:295px;
	height:28px;
	border: 1px solid #b1b1b1;
	border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25) rgba(0,0,0,0.25);
	position: relative;
	text-shadow: 0 1px 1px rgba(255,255,255,0.75);
	display: inline-block;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	background-color: #f0f0f0;
	background-image: -moz-linear-gradient(top, #fff, #d9d9d9);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fff), to(#d9d9d9));
	background-image: -webkit-linear-gradient(top, #fff, #d9d9d9);
	background-image: -o-linear-gradient(top, #fff, #d9d9d9);
	background-image: linear-gradient(to bottom, #fff, #d9d9d9);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffd9d9d9', GradientType=0);
	zoom: 1;
}
.closure_submit{
	width: 50px;
	height: 28px;
	background-color: #006dcc;
	min-width: 50px;
	color: #fff;
	border: 1px solid #b1b1b1;
	position: relative;
	text-shadow: 0 1px 1px rgba(255,255,255,0.75);
	display: inline-block;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25) rgba(0,0,0,0.25);
	background-color: #006dcc;
	background-image: -moz-linear-gradient(top, #08c, #04c);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#08c), to(#04c));
	background-image: -webkit-linear-gradient(top, #08c, #04c);
	background-image: -o-linear-gradient(top, #08c, #04c);
	background-image: linear-gradient(to bottom, #08c, #04c);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0044cc', GradientType=0);
	zoom: 1;
}
.closure_submit:hover{
	background-color: #005fb3;
	background-image: -moz-linear-gradient(top, #0077b3, #003cb3);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#0077b3), to(#003cb3));
	background-image: -webkit-linear-gradient(top, #0077b3, #003cb3);
	background-image: -o-linear-gradient(top, #0077b3, #003cb3);
	background-image: linear-gradient(to bottom, #0077b3, #003cb3);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0077b3', endColorstr='#ff003cb3', GradientType=0);
	zoom: 1;
}
.closure_cancel{
	width: 70px;
	height: 28px;
	border: 1px solid #b1b1b1;
	border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25) rgba(0,0,0,0.25);
	position: relative;
	text-shadow: 0 1px 1px rgba(255,255,255,0.75);
	display: inline-block;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: 3px;
	-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);
	background-color: #f0f0f0;
	background-image: -moz-linear-gradient(top, #fff, #d9d9d9);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fff), to(#d9d9d9));
	background-image: -webkit-linear-gradient(top, #fff, #d9d9d9);
	background-image: -o-linear-gradient(top, #fff, #d9d9d9);
	background-image: linear-gradient(to bottom, #fff, #d9d9d9);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffd9d9d9', GradientType=0);
	zoom: 1;
}
.closure_cancel:hover{
	color: #333;
	background-color: #e3e3e3;
	background-image: -moz-linear-gradient(top, #f2f2f2, #ccc);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#f2f2f2), to(#ccc));
	background-image: -webkit-linear-gradient(top, #f2f2f2, #ccc);
	background-image: -o-linear-gradient(top, #f2f2f2, #ccc);
	background-image: linear-gradient(to bottom, #f2f2f2, #ccc);
	background-repeat: repeat-x;
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff2f2f2', endColorstr='#ffcccccc', GradientType=0);
	zoom: 1;
}
</style>
</head>
<body>
	<form method="post" action="<?php echo ADMIN_URL.'closures_and_restrictions/add_closure_image';?>" name="closuresimageform"  id="closuresimageform"  class="" enctype="multipart/form-data">
		<input type="hidden" name="region" id="region" value="<?php echo $regionid; ?>"/>
		<table style="margin: 40px; ">
		<tr>
			<td><label for="closure_image" class="form-label">Image :</label></td>
			<td><input type="file" name="closure_image" value=""></td>
		</tr>
		<tr>
			<td></td>
			<td><small><em>Allowed maximmum size 1.5 MB,maxwidth 1600px</em></small></td>
		</tr>
		<tr>
			<td></td>
			<td  style="padding-top: 10px;">
				<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Upload" data-icon-primary="ui-icon-circle-check">Upload</button>
		   		<button class="button" type="button" name="cancelbuttonimage" id="cancelbuttonimage" value="cancel" data-icon-primary="ui-icon-circle-check">Cancel</button>
		   	</td>
		</tr>
		</table>
	</form>
    <?php print_r($closures_images);?>
</body>
</html>


