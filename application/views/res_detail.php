<?php	
	if(count($rest)>0){
	?>
	<ul id="rest_sort" class="restricted_detail_item">
	<?php
	foreach ($rest as $restfish)
	{
		if($restfish->restricted_text!="")
		{?>
			<li id=<?php echo $restfish->id; ?> class="liitem">
			<span style="float: right;">
				<a href="#" style="float: right;" class="remove_rest" id="<?php echo $restfish->id; ?>"><img alt="Remove" src="<?php echo  ADMIN_URL;?>assets/images/action4.gif"></a>
				<a href="#" style="float: right; padding-right: 6px;" class="edit_rest_text"><img alt="Edit" src="<?php echo  ADMIN_URL;?>assets/images/edit_action.gif"></a>
			</span>
			<span class="item" style="display:block;">
				<span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span>
				<?php 
				if($restfish->is_restricted_text_bold == 1){?><b><?php echo nl2br($restfish->restricted_text);?></b><?php }else{ echo nl2br($restfish->restricted_text); }?>
			</span>
			<div class="clearfix" id="restricted_textupdatediv" style="display:none;">
				<form method="post" action="#" name="restrictedtextupdateform"  id="restrictedtextupdateform"  class="form has-validation" enctype="multipart/form-data">
				<label for="restricted_updatemsg" class="form-label"></label>
				<input type="hidden" value="<?php echo $restfish->id; ?>" name="restricted_updatemsgid" id="restricted_updatemsgid" />
				<textarea name="restricted_updatemsg" id="restricted_updatemsg" cols="40" rows="5"><?php echo $restfish->restricted_text; ?></textarea><br/>
				<input type="checkbox" name="is_restricted_text_bold" id="is_restricted_text_bold" <?php if($restfish->is_restricted_text_bold){ ?>checked="checked"<?php } ?>> : Is bold<br/>
	            <button class="button" type="submit" name="submitbutton" id="submitbutton" value="Save" data-icon-primary="ui-icon-circle-check">Save</button>
	            <button class="button" type="button" name="cancelbutton" id="cancelbutton" value="Save" data-icon-primary="ui-icon-circle-check">Cancel</button>
	            </form>
			</div>
			</li>
			<?php
		}
		else {
			?>
			<li id="<?php echo $restfish->id; ?>" class="liitem">
			<span style="float: right;">
				<a href="#" style="float: right;" class="remove_rest" id="<?php echo $restfish->id; ?>"><img alt="Remove" src="<?php echo  ADMIN_URL;?>assets/images/action4.gif"></a>
				<a href="#" style="float: right; padding-right: 6px;" class="edit_rest_image" id="<?php echo $restfish->id; ?>"><img alt="Edit" src="<?php echo  ADMIN_URL;?>assets/images/edit_action.gif"></a>
			</span>
			<span class="item" style="display:block;">
				<span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span>
				<img src="<?php echo  ADMIN_URL;?>assets/rest/<?php echo  $restfish->restricted_image;?>">
			</span>
			<div class="clearfix" id="restricted_imageupdatediv" style="display:none;">
				<form method="post" action="<?php echo ADMIN_URL.'restricted_fish_in_region/update_image';?>" name="restrictedimageupdateform"  id="restrictedimageupdateform_<?php echo $restfish->id; ?>"  class="restrictedimageupdateform form has-validation" enctype="multipart/form-data">
					<input type="hidden" value="<?php echo $restfish->id; ?>" name="restricted_updateimageid" id="restricted_updateimageid" />
					<input type="hidden" value="<?php echo $restfish->restricted_image; ?>" name="oldimage" id="oldimage" />
					<input type="file" name="restricted_updateimage" value=""><br/>
					<small><em>Allowed maximmum size 2 MB</em></small><br/>
	              	<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Upload" data-icon-primary="ui-icon-circle-check">Upload</button>
	              	<button class="button" type="button" name="cancelbutton" id="cancelbutton" value="Save" data-icon-primary="ui-icon-circle-check">Cancel</button>
	           	</form>
			</div>
			</li>
		<?php 
		}
	}
	?>
	</ul>
	<?php
	}
	else {
		echo "0";
	}
	?>


