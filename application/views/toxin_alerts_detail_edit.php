<form method="post" action="#" name="toxin_alertsupdateform" id="toxin_alertsupdateform"
      class="form has-validation" enctype="multipart/form-data">
    <label for="restricted_updatemsg" class="form-label"></label>
    <input type="hidden" value="<?php echo !empty($alert->id) ? $alert->id : ''; ?>" name="toxin_alert_id"
           id="toxin_alert_id"/>
    <table>
        <tr style="display: none;">
            <td style="vertical-align:middle;"><label for="areas" class="form-label">Areas
                    :</label></td>
            <td><input type="text" name="areas" id="areas" class="areas"
                       style="width: 100%" value="<?php echo !empty($alert->type) ? $alert->type : ''; ?>"/></td>
        </tr>
        <tr>
            <td style="vertical-align: middle;"><label for="date_added"
                                                       class="form-label">Date on
                    :</label></td>
            <td><input type="date" name="date_added" id="date_added" style="width: 100%"
                       value="<?php echo !empty($alert->date_added) ? $alert->date_added : ''; ?>"/></td>
        </tr>
        <tr>
            <td style="vertical-align: middle;"><label for="title" class="form-label">Title
                    :</label></td>
            <td><input type="text" name="title" id="title" style="width: 100%"
                       value="<?php echo !empty($alert->title) ? $alert->title : ''; ?>"/></td>
        </tr>
        <tr style="display: none;">
            <td style="vertical-align: middle;"><label for="title" class="form-label">Key
                    points :</label></td>
            <td><input type="text" name="key_points" id="key_points" style="width: 100%"
                       value="<?php echo !empty($alert->key_points) ? $alert->key_points : ''; ?>"/></td>
        </tr>
        <tr>
            <td style="vertical-align: middle;"><label for="restricted_msg"
                                                       class="form-label">Description
                    :</label></td>
            <td>
                                                <textarea name="content" id="edit-toxin-alert-content"
                                                          class="edit_toxin_alert_content"
                                                          style="width: 100%; height: 200px"><?php echo !empty($alert->content) ? $alert->content : ''; ?></textarea>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button class="button" type="submit" name="submitbutton"
                        id="submitbutton" value="Save"
                        data-icon-primary="ui-icon-circle-check">Save
                </button>
                <button class="button" type="button" name="toxin_alert_cancelbutton"
                        id="toxin_alert_cancelbutton" value="Save"
                        data-icon-primary="ui-icon-circle-check">Cancel
                </button>
            </td>
        </tr>
    </table>
</form>
