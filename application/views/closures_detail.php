<section>
    <header class="container_12 clearfix">
        <div class="grid_10"><h1>Closures and Restrictions</h1></div>
        <div class="grid_2" style="text-align: right;">
            <a href="<?php echo ADMIN_URL; ?>closures_and_restrictions/export" class="button add_new"><span><span>Export All</span></span></a>
        </div>
    </header>
    <section class="container_12 clearfix">
        <div class="message error" style="display: none"><h3>Error!</h3></div>
        <?php if ($this->session->flashdata('error-message')): ?>
            <div class="message error">
                <h3>Error!</h3>
                <p><?php echo $this->session->flashdata('error-message'); ?></p>
            </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('flash-message')): ?>
            <div class="message success"><h3>Success!</h3>
                <p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
        <?php endif; ?>
        <div class="clear"></div>
        <div class="form">
            <input type="hidden" value="<?php echo ADMIN_URL . 'closures_and_restrictions/add_closures/'; ?>"
                   id="baseurltext">
            <input type="hidden" value="<?php echo ADMIN_URL . 'restricted_fish_in_region/add_image/'; ?>"
                   id="baseurlimage">
            <input type="hidden" value="<?php echo ADMIN_URL . 'closures_and_restrictions/check_fish/'; ?>"
                   id="baseurl">
            <input type="hidden" value="<?php echo ADMIN_URL . 'restricted_fish_in_region/fish_detail/'; ?>"
                   id="baseurl_fish">
            <input type="hidden" value="<?php echo ADMIN_URL . 'restricted_fish_in_region/restricted_detail/'; ?>"
                   id="baseurl_rest">
            <input type="hidden" value="<?php echo ADMIN_URL . 'closures_and_restrictions/closures_detail/'; ?>"
                   id="baseurl_closures">
            <input type="hidden" value="<?php echo ADMIN_URL . 'closures_and_restrictions/closure_list/'; ?>"
                   id="closureslist_url">
            <input type="hidden" value="<?php echo ADMIN_URL . 'closures_and_restrictions/closure_combo_list/'; ?>"
                   id="regionurl_closures">
            <input type="hidden" value="<?php echo ADMIN_URL . 'closures_and_restrictions/closures_imageurl/'; ?>"
                   id="closures_imageurl">
            <input type="hidden" value="<?php echo ADMIN_URL . 'closures_and_restrictions/closure_update/'; ?>"
                   id="closures_upadteurl">
            <input type="hidden" value="<?php echo ADMIN_URL . 'closures_and_restrictions/closure_delete/'; ?>"
                   id="closures_deleteurl">
            <input type="hidden" value="<?php echo ADMIN_URL . 'closures_and_restrictions/closuresrestorder/'; ?>"
                   id="closures_sorturl">
            <div class="portlet grid_12">
                <header>
                    <h2>Closures and Restrictions</h2>
                </header>
                <section>
                    <div class="container_12 clearfix">
                        <div class="grid_6">
                            <label for=closures_fish_region class="form-label">Region :</label>
                            <div class="form-input">
                                <?php $style = 'id="closures_fish_region"'; ?>
                                <?php echo form_dropdown('region_res', $region, $regionid, $style); ?>
                            </div>
                        </div>

                        <div class="grid_4">
                            <form method="get" action="">
                                <div class="container_12 form-input" id="region_select_box" style="margin-left: 0;">
                                    <?php $style = 'width:250px'; ?>
                                    <?php echo form_input(array(
                                        'name' => 'search',
                                        'id' => 'search',
                                        'placeholder' => 'Search',
                                        'value' => !empty($this->input->get('search')) ? $this->input->get('search') : '',
                                        'style' => $style,
                                    )); ?>
                                    <button type="submit" class="button">Filter</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="clearfix" id="restricted_detail">
                        <div class="form-input restricted_detailstyle">
                            <div class="clearfix add_rest" id="restricted_detail_display">
                                <nav>
                                    <ul id="main-navigation" class="clearfix">
                                        <span>Closures and Restrictions</span>
                                        <li style="margin: 0px;padding: 0px;">
                                            <a href="#" id="addclosures_restrictions" style="padding: 0px;"><img
                                                        alt="Add New" width="15"
                                                        src="<?php echo ADMIN_URL; ?>assets/images/add.png"></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>

                            <div id="closures_restrictionsdiv" title="Detail" style="display:none;">
                                <form method="post" action="#" name="closures_restrictionsform"
                                      id="closures_restrictionsform" class="" enctype="multipart/form-data">
                                    <table>
                                        <tr style="display: none;">
                                            <td style="vertical-align: middle;"><label for="areas" class="form-label">Areas
                                                    :</label></td>
                                            <td><input type="text" value="" name="areas" id="areas" class="areas"
                                                       style="width: 100%"/></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;"><label for="title" class="form-label">Date
                                                    Added :</label></td>
                                            <td><input type="date" value="" name="date_added" id="date_added"
                                                       style="width: 100%"/></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;"><label for="title" class="form-label">Title
                                                    :</label></td>
                                            <td><input type="text" value="" name="title" id="title"
                                                       style="width: 100%"/></td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td style="vertical-align: middle;"><label for="title" class="form-label">Key
                                                    points :</label></td>
                                            <td><input type="text" value="" name="key_points" id="key_points"
                                                       style="width: 100%"/></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;"><label for="restricted_msg"
                                                                                       class="form-label">Description
                                                    :</label></td>
                                            <td>
                                                <textarea name="content" id="newcontent" class="closure_content"
                                                          style="width: 100%; height: 200px"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <button class="button" type="submit" name="submitbutton"
                                                        id="submitbutton" value="Save"
                                                        data-icon-primary="ui-icon-circle-check">Save
                                                </button>
                                                <button class="button" type="button" name="cancelclosures"
                                                        id="cancelclosures" value="cancel"
                                                        data-icon-primary="ui-icon-circle-check">Cancel
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="closures_restrictionsupdatediv" title="Detail" style="display:none;">

                            </div>
                            <div class="clearfix" id="restricted_data">
                                <?php
                                if (count($closuresdata) > 0) {
                                    ?>
                                    <ul id="rest_sort" class="restricted_detail_item">
                                        <?php
                                        foreach ($closuresdata as $closure) {
                                            ?>
                                            <li id=<?php echo $closure->id; ?> class="liitem">
                                                <span style="float: right;">
                                                    <a href="#" style="float: right;" class="remove_closure"
                                                       id="<?php echo $closure->id; ?>"><img alt="Remove"
                                                                                             src="<?php echo ADMIN_URL; ?>assets/images/action4.gif"></a>
                                                    <a href="#" style="float: right; padding-right: 6px;" class="edit_closure" id="edit-closure-<?php echo $closure->id; ?>" data-id="<?php echo $closure->id; ?>">
                                                        <img alt="Edit" src="<?php echo ADMIN_URL; ?>assets/images/edit_action.gif">
                                                    </a>
                                                </span>
                                                <span class="item" style="display:block;">
								                    <span class="ui-icon ui-icon-arrowthick-2-n-s" style="float: left; margin-top: 5px;"></span>
                                                    <?php echo nl2br($closure->title); ?>
							                    </span>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                } else { ?>
                                    <ul id="rest_sort" class="restricted_detail_item">
                                        <li id="nodata">No Data Available.</li>
                                    </ul>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </section>


