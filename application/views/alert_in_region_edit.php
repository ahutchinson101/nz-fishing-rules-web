<style type="text/css">
.second-detail{float:left; width:600px; padding:0 0 10px 0;}
.second-detail .new-label {
    float: left;
    font-size: 12px;
    font-weight: bold;
    margin: 0;
    text-align: right;
    width: 200px;
}
.second-detail .new-form-input {
	float:left;
	width:250px;
	font-size: 12px;
	padding: 0 0 0 7px;
    margin: 0 0 0 0;
}	
</style>
<section>
	<header class="container_12 clearfix"><div class="grid_12"><h1>Edit Alert Message In Region</h1></div></header>
    <section class="container_12 clearfix">
    	<?php if(validation_errors()):?>
			<div class="message error"><h3>Error!</h3><?php echo validation_errors(); ?></div>
		<?php endif;?>
		 <?php if($this->session->flashdata('error-message')):?>
                <div class="message error"> 
                	<h3>Error!</h3> 
                       <p><?php echo $this->session->flashdata('error-message'); ?></p>
                </div>
            <?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"><h3>Success!</h3><p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
       	<?php endif;?>
		<div class="clear"></div>
	   	<form method="post" action="" name="contentform"  id="contentform"  class="form has-validation">
	   		<div class="portlet grid_12">
			    <header>
			        <h2>Edit Alert Message In Region</h2>
			    </header>
       		 <section>
       		 <div class="clearfix">
       		 <div class=second-detail>
					<label for="size" class="new-label">Region :</label>
					<div class="new-form-input"><?php echo $region->name;?></div>
				</div>
				</div>
				<input type="hidden" name="alert_in_region_id" value="<?php echo $alert_in_region->id;?>">
	   			<input type="hidden" name="region_id" value="<?php echo $alert_in_region->region_id;?>">
				<div class="clearfix">
					<label for="region_msg" class="form-label">Region Message :</label>
					<div class="form-input">
						<textarea rows="10" cols="" name="region_msg" id="region_msg" ><?php echo $alert_in_region->alert_msg;?></textarea>
                    </div>
				</div>
       		 	<div class="form-action clearfix">
					<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Save" data-icon-primary="ui-icon-circle-check">Save</button>
					<button class="button" type="reset">Reset</button>
					<a href="<?php echo ADMIN_URL;?>alert_in_region" class="button approve"><span><span>Back</span></span></a>
				</div>
			</section>
			</div>
		</form>
	</section>
	