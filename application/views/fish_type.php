<section>
	<header class="container_12 clearfix"><div class="grid_12"><h1>Fish Type</h1></div></header>
    <section class="container_12 clearfix">
    	<div class="clear"></div>
    	
        <?php if(validation_errors()):?>
			<div class="message error"><h3>Error!</h3><?php echo validation_errors(); ?></div>
		<?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"><h3>Success!</h3><p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
       	<?php endif;?>
        <div class="clear"></div>
        	<div class="grid_12">
            	<form name="frm" action="" method="post">
                	<div id="demo" class="clearfix"> 
                    	<table class="display" id="example"> 
                        	<thead> 
                            	<tr>
                            		<th align="left" width="20%">Fish Type Name</th>
									<th align="left" width="10%">Major Type</th>
									<th align="left" width="20%">Video Title</th>
									<th align="left" width="20%">Video File Name</th>
									<th align="left" width="20%">Video URL</th>
									<th align="left">Action</th>
                            	</tr> 
                            </thead> 
                            <tbody> 
                            	<?php
								$i=1; 
								foreach ($all_fish_type as $fish_type){
								if($i%2==0){
									$class="even";
								}else{
									$class="odd";
								}?>
									<tr class="gradeA <?php echo $class;?>">
										<td align="left"><?php echo $fish_type->fish_type_name;?></td>
										<td align="left"><?php echo $fish_type->major_type;?></td>
										<td align="left"><?php echo $fish_type->video_title;?></td>
										<td align="left"><?php echo $fish_type->video_file;?></td>
                                        <td align="left"><?php echo $fish_type->video_url;?></td>
										<td style="width:140px;">
											<div class="actions_menu">
												<ul>
													<li><a class="edit" href="<?php echo ADMIN_URL;?>fish_type/edit/<?php echo $fish_type->id;?>">EDIT</a></li>
													<li><a class="delete" onclick="javascript:return confirm('Are you sure delete this fish type with all fish and fish in region?');" href="<?php echo ADMIN_URL;?>fish_type/delete/<?php echo $fish_type->id;?>">DELETE</a></li>
													<li><input type="checkbox" name="multicontent[]" id="multicontent" value="<?php echo $fish_type->id;?>" /></li>
												</ul>
											</div>
										</td>
									</tr>
								<?php $i++;}?>
                          	</tbody> 
                    	</table> 
                	</div>
                    <div class="clear"></div>
                    <div class="paging">
                     	 <?php //echo $this->pagination->create_links(); ?>
                    </div>
                    <div class="clear"></div>
                    <div class="table_menu">
						<ul class="left">
							<li><a href="<?php echo ADMIN_URL;?>fish_type/add" class="button add_new"><span><span>Add New</span></span></a></li>
						</ul>
						<ul class="right">
							<li><a href="#"  onclick="select_all();" class="button check_all"><span><span>Check All</span></span></a></li>
							<li><a href="#" onclick="unselect_all();"  class="button uncheck_all"><span><span>Uncheck All</span></span></a></li>
							<li><input name="multiaction" value="Delete"  type="submit" /></li>
						</ul>
					</div>
				</form>
          	</div>
            <div class="clear"></div>
      	</section>
        <script type="text/javascript">
			//<![CDATA[
			function select_all(){
		        checkboxes = document.getElementsByTagName("input");
				state = true;
	    	    for (i=0; i<checkboxes.length ; i++){
	          		if (checkboxes[i].type == "checkbox"){
	        			checkboxes[i].checked=state;
	        		 $.uniform.update(checkboxes[i]);
			  		}
	       		}
			}
			//]]>
		</script>
        <script type="text/javascript">
			//<![CDATA[
			function unselect_all(){
		        checkboxes = document.getElementsByTagName("input");
    			state = false;
	        	for (i=0; i<checkboxes.length ; i++){
	          		if (checkboxes[i].type == "checkbox"){
	        			checkboxes[i].checked=state;
	        	 		$.uniform.update(checkboxes[i]);
			  		}
	        	}
			}
			//]]>
		</script>