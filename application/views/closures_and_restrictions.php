<section>
	<header class="container_12 clearfix">
        <div class="grid_10"><h1>Closures and Restrictions</h1></div>
        <div class="grid_2" style="text-align: right;">
            <a href="<?php echo ADMIN_URL; ?>closures_and_restrictions/export" class="button add_new"><span><span>Export All</span></span></a>
        </div>
    </header>
	<section class="container_12 clearfix">
		<div class="message error" style="display: none"><h3>Error!</h3></div>
    	 <?php if($this->session->flashdata('error-message')):?>
                <div class="message error"> 
                	<h3>Error!</h3> 
                       <p><?php echo $this->session->flashdata('error-message'); ?></p>
                </div>
            <?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"><h3>Success!</h3><p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
       	<?php endif;?>
		<div class="clear"></div>
		<div class="form">
			<input type="hidden" value="<?php echo ADMIN_URL.'closures_and_restrictions/add_closures/';?>" id="baseurltext">
			<input type="hidden" value="<?php echo ADMIN_URL.'restricted_fish_in_region/add_image/';?>" id="baseurlimage">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'closures_and_restrictions/check_fish/';?>" id="baseurl">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'restricted_fish_in_region/fish_detail/';?>" id="baseurl_fish">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'restricted_fish_in_region/restricted_detail/';?>" id="baseurl_rest">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'closures_and_restrictions/closures_detail/';?>" id="baseurl_closures">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'closures_and_restrictions/closure_list/';?>" id="closureslist_url">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'closures_and_restrictions/region_list/';?>" id="regionurl_closures">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'closures_and_restrictions/closures_imageurl/';?>" id="closures_imageurl">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'closures_and_restrictions/closure_update/';?>" id="closures_upadteurl">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'closures_and_restrictions/closure_delete/';?>" id="closures_deleteurl">
	   		<input type="hidden" value="<?php echo ADMIN_URL.'closures_and_restrictions/closuresrestorder/';?>" id="closures_sorturl">
	   		<div class="portlet grid_12">
			    <header>
			        <h2>Closures and Restrictions</h2>
			    </header>
       		<section>
       		 	<div class="clearfix">
       		 		<label for=closures_fish_region class="form-label">Region :</label>
					<div class="form-input">
					<?php $style='id="closures_fish_region"';?>
                      	<?php echo form_dropdown('region_res',$region,'',$style);?>
                    </div>
				</div>
			</section>
			</div>
		</div>
</section>
