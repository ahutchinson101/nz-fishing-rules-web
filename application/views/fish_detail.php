<style type="text/css">
.second-detail{float:left; width:600px; padding:0 0 10px 0;}
.second-detail .new-label {
    float: left;
    font-size: 12px;
    font-weight: bold;
    margin: 0;
    text-align: right;
    width: 200px;
}
.second-detail .new-form-input {
	float:left;
	width:250px;
	font-size: 12px;
	padding: 0 0 0 7px;
    margin: 0 0 0 0;
}	
</style>
<div class=second-detail>
	<label for="size" class="new-label">Keyname :</label>
	<div class="new-form-input"><?php echo $fish->keyname;?></div>
</div>
<div class="second-detail">
	<label for="size" class="new-label">Keyword:</label>
		<div class="new-form-input"><?php echo $fish->keyword;?></div>
</div>
<div class="second-detail">
	<label for="size" class="new-label">Fish Type :</label>
		<div class="new-form-input"><?php echo $fish->fish_type_name;?></div>
</div>
<div class="second-detail">
	<label for="size" class="new-label">Common Alternate Name :</label>
		<div class="new-form-input"><?php echo $fish->common_alt_name;?></div>
</div>
<div class="second-detail">
	<label for="size" class="new-label">Maori Name :</label>
		<div class="new-form-input"><?php echo $fish->mori_name;?></div>
</div>
<div class="second-detail">
	<label for="size" class="new-label">Scientific Name :</label>
		<div class="new-form-input"><?php echo $fish->scientific_name;?></div>
</div>
<div class="second-detail">
	<label for="size" class="new-label">National Size :</label>
		<div class="new-form-input"><?php echo $fish->national_size;?></div>
</div>
<div class="second-detail">
	<label for="size" class="new-label">National Daily :</label>
		<div class="new-form-input"><?php echo $fish->national_daily;?></div>
</div>
