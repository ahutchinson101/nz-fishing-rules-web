<section>
	<header class="container_12 clearfix"><div class="grid_12"><h1>Add Fish Type</h1></div></header>
    <section class="container_12 clearfix">
    	<?php if(validation_errors()):?>
			<div class="message error"><h3>Error!</h3><?php echo validation_errors(); ?></div>
		<?php endif;?>
		 <?php if($this->session->flashdata('error-message')):?>
                <div class="message error"> 
                	<h3>Error!</h3> 
                       <p><?php echo $this->session->flashdata('error-message'); ?></p>
                </div>
            <?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"><h3>Success!</h3><p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
       	<?php endif;?>
		<div class="clear"></div>
	   	<form method="post" action="" name="contentform"  id="contentform"  class="form has-validation" enctype="multipart/form-data">
	   		<div class="portlet grid_12">
			    <header>
			        <h2>Add Fish Type</h2>
			    </header>
       		 <section>
       		 	<div class="clearfix">
					<label for="fish_type_name" class="form-label">Fish Type Name :</label>
					<div class="form-input">
                      	<input type="text" name="fish_type_name" id="fish_type_name" value="<?php echo set_value('fish_type_name');?>">
                    </div>
				</div>
				<div class="clearfix">
					<label for="major_type" class="form-label">Major Type :</label>
					<div class="form-input">
                      	<input type="text" name="major_type" id="major_type" value="<?php echo set_value('major_type');?>">
                    </div>
				</div>
				<div class="clearfix">
					<label for="video_url" class="form-label">Video :</label>
					<div class="form-input">
						<input type="radio" name="video_checkbox" value="file">&nbsp;FILES &nbsp;&nbsp;<input type="radio" name="video_checkbox" value="url">&nbsp;URL
					</div>
				</div>
				<div class="clearfix" id="video_file_field">
					<label for="video_file" class="form-label">Video File :</label>
					<div class="form-input">
                      	<input type="file" name="video_file" id="video_file" value="<?php echo set_value('video_file');?>"><br/>
                      	<small><em>Please upload only mp4 OR 3gp files</em></small>
                      	
                    </div>
				</div>
				<div class="clearfix" id="video_url_field">
					<label for="video_url" class="form-label">Video URL :</label>
					<div class="form-input">
						<input type="text" value="<?php echo set_value('video_url');?>" name="video_url" id="video_url">
					</div>
				</div>
				<div class="clearfix">
					<label for="video_title" class="form-label">Video Title :</label>
					<div class="form-input">
                      	<input type="text" name="video_title" id="video_title" value="<?php echo set_value('video_title');?>"><br/>
                      	
                    </div>
				</div>
				<div class="form-action clearfix">
					<button class="button" type="submit" name="submitbutton" id="submitbutton" value="Add New" data-icon-primary="ui-icon-circle-check">Add New</button>
					<button class="button" type="reset">Reset</button>
					<a href="<?php echo ADMIN_URL;?>fish_type" class="button approve"><span><span>Back</span></span></a>
				</div>
			</section>
			</div>
		</form>
	</section>
	