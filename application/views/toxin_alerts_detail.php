<section>
    <header class="container_12 clearfix">
        <div class="grid_10"><h1>Toxin Alerts</h1></div>
        <div class="grid_2" style="text-align: right;">
            <a href="<?php echo ADMIN_URL; ?>toxin_alerts/export"
               class="button add_new"><span><span>Export All</span></span></a>
        </div>
    </header>
    <section class="container_12 clearfix">
        <div class="message error" style="display: none"><h3>Error!</h3></div>
        <?php if ($this->session->flashdata('error-message')): ?>
            <div class="message error">
                <h3>Error!</h3>
                <p><?php echo $this->session->flashdata('error-message'); ?></p>
            </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('flash-message')): ?>
            <div class="message success"><h3>Success!</h3>
                <p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
        <?php endif; ?>
        <div class="clear"></div>
        <div class="form">
            <input type="hidden" value="<?php echo ADMIN_URL . 'toxin_alerts/add_toxin_alerts/'; ?>" id="baseurltext">
            <input type="hidden" value="<?php echo ADMIN_URL . 'toxin_alerts/check_fish/'; ?>" id="baseurl">
            <input type="hidden" value="<?php echo ADMIN_URL . 'toxin_alerts/alert_detail/'; ?>" id="baseurl_alerts">
            <input type="hidden" value="<?php echo ADMIN_URL . 'toxin_alerts/alert_list/'; ?>" id="alertslist_url">
            <input type="hidden" value="<?php echo ADMIN_URL . 'toxin_alerts/alert_combo_list/'; ?>"
                   id="regionurl_alerts">
            <input type="hidden" value="<?php echo ADMIN_URL . 'toxin_alerts/alert_update/'; ?>" id="alerts_upadteurl">
            <input type="hidden" value="<?php echo ADMIN_URL . 'toxin_alerts/alert_delete/'; ?>" id="alerts_deleteurl">
            <input type="hidden" value="<?php echo ADMIN_URL . 'toxin_alerts/alertsrestorder/'; ?>" id="alerts_sorturl">
            <div class="portlet grid_12">
                <header>
                    <h2>Toxin Alerts</h2>
                </header>
                <section>
                    <div class="container_12 clearfix">
                        <div class="grid_6">
                            <label for="toxin_alerts_fish_region" class="form-label">Region :</label>
                            <div class="form-input">
                                <?php $style = 'id="toxin_alerts_fish_region"'; ?>
                                <?php echo form_dropdown('region_res', $region, $regionid, $style); ?>
                            </div>
                        </div>
                        <div class="grid_4">
                            <form method="get" action="">
                                <div class="container_12 form-input" id="region_select_box" style="margin-left: 0;">
                                    <?php $style = 'width:250px'; ?>
                                    <?php echo form_input(array(
                                        'name' => 'search',
                                        'id' => 'search',
                                        'placeholder' => 'Search',
                                        'value' => !empty($this->input->get('search')) ? $this->input->get('search') : '',
                                        'style' => $style,
                                    )); ?>
                                    <button type="submit" class="button">Filter</button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="clearfix" id="restricted_detail">
                        <div class="form-input restricted_detailstyle">
                            <div class="clearfix add_rest" id="restricted_detail_display">
                                <nav>
                                    <ul id="main-navigation" class="clearfix">
                                        <span>Toxin Alerts</span>
                                        <li style="margin: 0px;padding: 0px;">
                                            <a href="#" id="add_toxin_alerts" style="padding: 0px;"><img alt="Add New"
                                                                                                         width="15"
                                                                                                         src="<?php echo ADMIN_URL; ?>assets/images/add.png"></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>

                            <div id="toxin_alertsdiv" title="Detail" style="display:none;">
                                <form method="post" action="#" name="toxin_alerts_form" id="toxin_alerts_form" class=""
                                      enctype="multipart/form-data">
                                    <table>
                                        <tr style="display: none;">
                                            <td style="vertical-align: middle;"><label for="areas" class="form-label">Areas
                                                    :</label></td>
                                            <td><input type="text" value="" name="areas" id="areas" class="areas"
                                                       style="width: 100%"/></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;"><label for="title" class="form-label">Date
                                                    Added :</label></td>
                                            <td><input type="date" value="" name="date_added" id="date_added"
                                                       style="width: 100%"/></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;"><label for="title" class="form-label">Title
                                                    :</label></td>
                                            <td><input type="text" value="" name="title" id="title"
                                                       style="width: 100%"/></td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td style="vertical-align: middle;"><label for="title" class="form-label">Key
                                                    points :</label></td>
                                            <td><input type="text" value="" name="key_points" id="key_points"
                                                       style="width: 100%"/></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: middle;"><label class="form-label">Description
                                                    :</label></td>
                                            <td>
                                                <textarea name="content" class="toxin_alert_content"
                                                          style="width: 100%; height: 200px"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <button class="button" type="submit" name="submitbutton"
                                                        id="submitbutton" value="Save"
                                                        data-icon-primary="ui-icon-circle-check">Save
                                                </button>
                                                <button class="button" type="button" name="cancel_toxin_alerts"
                                                        id="cancel_toxin_alerts" value="cancel"
                                                        data-icon-primary="ui-icon-circle-check">Cancel
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                            <div id="toxin_alertsupdatediv" style="display:none;">

                            </div>
                            <div class="clearfix" id="restricted_data">
                                <?php
                                if (count($alertsdata) > 0) {
                                    ?>
                                    <ul id="toxin_sort" class="restricted_detail_item">
                                        <?php
                                        foreach ($alertsdata as $alert) {
                                            ?>
                                            <li id=<?php echo $alert->id; ?> class="liitem">
                                                <span style="float: right;">
                                                    <a href="#" style="float: right;" class="remove_toxin_alert"
                                                       id="<?php echo $alert->id; ?>"><img alt="Remove"
                                                                                           src="<?php echo ADMIN_URL; ?>assets/images/action4.gif"></a>
                                                    <a href="#" style="float: right; padding-right: 6px;" class="edit_toxin_alert" id="edit-toxin-alert-<?php echo $alert->id ?>" data-id="<?php echo $alert->id ?>">
                                                        <img
                                                                alt="Edit" src="<?php echo ADMIN_URL; ?>assets/images/edit_action.gif"></a>
                                                </span>
                                                <span class="item" style="display:block;">
                                                    <span class="ui-icon ui-icon-arrowthick-2-n-s"
                                                          style="float: left; margin-top: 5px;"></span>
                                                    <?php echo nl2br($alert->title . ' ' . $alert->type); ?>
                                                </span>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                } else { ?>
                                    <ul id="toxin_sort" class="restricted_detail_item">
                                        <li id="nodata">No Data Available.</li>
                                    </ul>
                                <?php }
                                ?>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </section>


