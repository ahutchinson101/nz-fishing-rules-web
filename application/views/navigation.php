<!-- Sidebar -->
<aside>
	
	<nav style="padding-top: 20px;">
		<h2 <?php if($this->uri->segment(1)=='fish_in_region'){ echo '';}else{ echo 'class="collapsed"';}?>>Fish In Region</h2>
		<ul>
			<li class="<?php if($this->uri->segment(1)=='fish_in_region' && $this->uri->segment(2)==''){ echo 'current';}?>">
				<a href="<?php echo ADMIN_URL?>fish_in_region">Fish In Region</a>
			</li>
			<li class="<?php if($this->uri->segment(1)=='fish_in_region' && $this->uri->segment(2)=='add'){ echo 'current';}?>">
				<a href="<?php echo ADMIN_URL?>fish_in_region/add">Add New</a>
			</li>
		</ul>
	</nav>
	<?php /*?><nav style="padding-top: 20px;">
		<h2 <?php if($this->uri->segment(1)=='restricted_fish_in_region'){ echo '';}else{ echo 'class="collapsed"';}?>>Restricted Area</h2>
		<ul>
			<li class="<?php if($this->uri->segment(1)=='restricted_fish_in_region' && $this->uri->segment(2)==''){ echo 'current';}?>">
				<a href="<?php echo ADMIN_URL?>restricted_fish_in_region">Restricted</a>
			</li>
		</ul>
	</nav><?php */?>
	<nav style="padding-top: 20px;">
		<h2 <?php if($this->uri->segment(1)=='fish'){ echo '';}else{ echo 'class="collapsed"';}?>>Fish</h2>
		<ul>
			<li class="<?php if($this->uri->segment(1)=='fish' && $this->uri->segment(2)==''){ echo 'current';}?>">
				<a href="<?php echo ADMIN_URL?>fish">Fish</a>
			</li>
			<li class="<?php if($this->uri->segment(1)=='fish' && $this->uri->segment(2)=='add'){ echo 'current';}?>">
				<a href="<?php echo ADMIN_URL?>fish/add">Add New</a>
			</li>
		</ul>
	</nav>
	<nav style="padding-top: 20px;">
		<h2 <?php if($this->uri->segment(1)=='fish_type'){ echo '';}else{ echo 'class="collapsed"';}?>>Fish Type</h2>
		<ul>
			<li class="<?php if($this->uri->segment(1)=='fish_type' && $this->uri->segment(2)==''){ echo 'current';}?>">
				<a href="<?php echo ADMIN_URL?>fish_type">Fish Type</a>
			</li>
			<li class="<?php if($this->uri->segment(1)=='fish_type' && $this->uri->segment(2)=='add'){ echo 'current';}?>">
				<a href="<?php echo ADMIN_URL?>fish_type/add">Add New</a>
			</li>
		</ul>
	</nav>
	<nav style="padding-top: 20px;">
		<h2 <?php if($this->uri->segment(1)=='alert_in_region'){ echo '';}else{ echo 'class="collapsed"';}?>>Alert In Region</h2>
		<ul>
			<li class="<?php if($this->uri->segment(1)=='alert_in_region' && $this->uri->segment(2)==''){ echo 'current';}?>">
				<a href="<?php echo ADMIN_URL?>alert_in_region">Alerts</a>
			</li>
		</ul>
	</nav>
	<nav style="padding-top: 20px;">
		<h2 <?php if($this->uri->segment(1)=='closures_and_restrictions'){ echo '';}else{ echo 'class="collapsed"';}?>>Closures And Restrictions</h2>
		<ul>
			<li class="<?php if($this->uri->segment(1)=='closures_and_restrictions' && $this->uri->segment(2)==''){ echo 'current';}?>">
				<a href="<?php echo ADMIN_URL?>closures_and_restrictions">Closures And Restrictions</a>
			</li>
		</ul>
	</nav>
    <nav style="padding-top: 20px;">
        <h2 <?php if($this->uri->segment(1)=='toxin_alerts'){ echo '';}else{ echo 'class="collapsed"';}?>>Toxin Alerts</h2>
        <ul>
            <li class="<?php if($this->uri->segment(1)=='toxin_alerts' && $this->uri->segment(2)==''){ echo 'current';}?>">
                <a href="<?php echo ADMIN_URL?>toxin_alerts">Toxin Alerts</a>
            </li>
        </ul>
    </nav>



	<nav style="padding-top: 20px;">
		<h2 <?php if($this->uri->segment(1)=='account'){ echo '';}else{ echo 'class="collapsed"';}?>>Administrator</h2>
		<ul style="display: none;">
			<li class="<?php if($this->uri->segment(1)=='account'){ echo "current";}?>"><a href="<?php echo ADMIN_URL?>account">My Account</a></li>
			<li><a href="<?php echo ADMIN_URL?>login/logout">Signout</a></li>
		</ul>
	</nav>

<!--    export all fish images-->

    <nav style="padding-top: 20px;">
        <h2 <?php if($this->uri->segment(1)=='account'){ echo '';}else{ echo 'class="collapsed"';}?>>Export All Fish Images</h2>
        <ul style="display: none;">
            <li><a href="<?php echo ADMIN_URL?>api/exportAllImage">Download</a></li>
        </ul>
    </nav>

</aside>
