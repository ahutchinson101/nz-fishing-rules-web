<section>
    <header class="container_12 clearfix">
        <div class="grid_10"><h1>Fish In Region</h1></div>
        <div class="grid_2" style="text-align: right;">
            <a href="<?php echo ADMIN_URL; ?>fish_in_region/export" class="button add_new"><span><span>Export All</span></span></a>
        </div>
    </header>
    <section class="container_12 clearfix">
        <div class="clear"></div>
        <div class="portlet grid_12">
            <header><h2>Region Filter</h2></header>
            <section>
                <div class="container_12 clearfix">
                    <div class="grid_8">
                        <label for="region" class="form-label" style="float:left; margin-right: 10px;">Region :</label>
                        <div class="form-input" id="region_select_box">
                            <?php $style = 'style="width:268px;" id="region"'; ?>
                            <?php echo form_dropdown('region', $region, $region_id, $style); ?>
                        </div>
                        <input type="hidden" value="<?php echo ADMIN_URL . 'fish_in_region/index/'; ?>" id="baseurl">
                    </div>
                    <div class="grid_4">
                        <form method="get" action="">
                            <div class="container_12">
                                <div class="grid_9" style="margin-right: 0">
                                    <div class="form-input" id="region_select_box">
                                        <?php $style = 'width:100%'; ?>
                                        <?php echo form_input(array(
                                            'name' => 'search',
                                            'id' => 'search',
                                            'placeholder' => 'Search',
                                            'value' => !empty($this->input->get('search')) ? $this->input->get('search') : '',
                                            'style' => $style,
                                        )); ?>
                                    </div>
                                </div>
                                <div class="grid_3" style="margin-left: 0">
                                    <button type="submit" class="button">Filter</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="clear"></div>
            </section>
        </div>
        <div class="clear" style="height:10px;">&nbsp;</div>
        <?php if (validation_errors()): ?>
            <div class="message error"><h3>Error!</h3><?php echo validation_errors(); ?></div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('flash-message')): ?>
            <div class="message success"><h3>Success!</h3>
                <p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
        <?php endif; ?>
        <div class="clear"></div>
        <div class="grid_12">
            <form name="frm" action="" method="post">
                <div id="demo" class="clearfix">
                    <table class="display" id="example">
                        <thead>
                        <tr>
                            <th align="left" width="10%">Keyname</th>
                            <th align="left" width="10%">Keyword</th>
                            <th align="left" width="10%">Fish Type</th>
                            <th align="left" width="20%">Scientific Name</th>
                            <th align="left" width="30%">National Size</th>
                            <th align="left" width="30%">National Daily</th>
                            <th align="left" width="30%">Size</th>
                            <th align="left" width="30%">Daily</th>
                            <th align="left" width="30%">Bag</th>
                            <th align="left" width="30%">Order</th>
                            <th align="left">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 1;
                        foreach ($fish_in_region as $fish) {
                            if ($i % 2 == 0) {
                                $class = "even";
                            } else {
                                $class = "odd";
                            } ?>
                            <tr class="gradeA <?php echo $class; ?>">
                                <td align="left"><?php echo $fish->keyname; ?></td>
                                <td align="left"><?php echo $fish->keyword; ?></td>
                                <td align="left"><?php echo $fish->fish_type_name; ?></td>
                                <td align="left"><?php echo $fish->scientific_name; ?></td>
                                <td align="left"><?php echo $fish->national_size; ?></td>
                                <td align="left"><?php echo $fish->national_daily; ?></td>
                                <td align="left"><?php echo $fish->size; ?></td>
                                <td align="left"><?php echo $fish->daily; ?></td>
                                <td align="left"><?php echo $fish->bag; ?></td>
                                <td align="left"><?php echo $fish->order; ?></td>
                                <td style="width:140px;">
                                    <div class="actions_menu">
                                        <ul>
                                            <li><a class="edit"
                                                   href="<?php echo ADMIN_URL; ?>fish_in_region/edit/<?php echo $fish->fish_in_region_id; ?>">EDIT</a>
                                            </li>
                                            <li><a class="delete"
                                                   onclick="javascript:return confirm('Are you sure delete this fish in region?');"
                                                   href="<?php echo ADMIN_URL; ?>fish_in_region/delete/<?php echo $fish->fish_in_region_id; ?>">DELETE</a>
                                            </li>
                                            <li><input type="checkbox" name="multicontent[]" id="multicontent"
                                                       value="<?php echo $fish->fish_in_region_id; ?>"/></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <?php $i++;
                        } ?>
                        </tbody>
                    </table>
                </div>
                <div class="clear"></div>
                <div class="paging">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
                <div class="clear"></div>
                <div class="table_menu">
                    <ul class="left">
                        <li><a href="<?php echo ADMIN_URL; ?>fish_in_region/add" class="button add_new"><span><span>Add New</span></span></a>
                        </li>
                    </ul>
                    <ul class="right">
                        <li><a href="#" onclick="select_all();"
                               class="button check_all"><span><span>Check All</span></span></a></li>
                        <li><a href="#" onclick="unselect_all();"
                               class="button uncheck_all"><span><span>Uncheck All</span></span></a></li>
                        <li><input name="multiaction" value="Delete" type="submit"/></li>
                    </ul>
                </div>
            </form>
        </div>
        <div class="clear"></div>
    </section>
    <script type="text/javascript">
        //<![CDATA[
        function select_all() {
            checkboxes = document.getElementsByTagName("input");
            state = true;
            for (i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == "checkbox") {
                    checkboxes[i].checked = state;
                    $.uniform.update(checkboxes[i]);
                }
            }
        }

        //]]>
    </script>
    <script type="text/javascript">
        //<![CDATA[
        function unselect_all() {
            checkboxes = document.getElementsByTagName("input");
            state = false;
            for (i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == "checkbox") {
                    checkboxes[i].checked = state;
                    $.uniform.update(checkboxes[i]);
                }
            }
        }

        //]]>
    </script>
