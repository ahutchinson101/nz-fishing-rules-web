<section>
	<header class="container_12 clearfix"><div class="grid_12"><h1>Edit Fish</h1></div></header>
    <section class="container_12 clearfix">
    	<?php if(validation_errors()):?>
			<div class="message error"><h3>Error!</h3><?php echo validation_errors(); ?></div>
		<?php endif;?>
		 <?php if($this->session->flashdata('error-message')):?>
                <div class="message error"> 
                	<h3>Error!</h3> 
                       <p><?php echo $this->session->flashdata('error-message'); ?></p>
                </div>
            <?php endif;?>
		<?php if($this->session->flashdata('flash-message')):  ?>
			<div class="message success"><h3>Success!</h3><p><?php echo $this->session->flashdata('flash-message'); ?></p></div>
       	<?php endif;?>
		<div class="clear"></div>
	   	<div class="portlet grid_12">
			    <header>
			        <h2>Edit Fish</h2>
			    </header>
       		 <section>
                 <div class="grid_6">
                     <form method="post" action="" name="contentform"  id="contentform"  class="form has-validation" enctype="multipart/form-data">
                         <div class="clearfix">
                             <label for="keyname" class="form-label">Fish Type :</label>
                             <div class="form-input">
                                 <?php echo form_dropdown('fish_type',$fish_type,$fish->fish_type_id);?>
                             </div>
                         </div>
                         <div class="clearfix">
                             <label for="fish_type" class="form-label">Keyname :</label>
                             <div class="form-input">
                                 <input type="text" name="keyname" id="keyname" value="<?php echo $fish->keyname;?>">
                             </div>
                         </div>
                         <div class="clearfix">
                             <label for="keyword" class="form-label">Keyword :</label>
                             <div class="form-input">
                                 <input type="text" name="keyword" id="keyword" value="<?php echo $fish->keyword;?>">
                             </div>
                         </div>
                         <div class="clearfix">
                             <label for="common_alt_name" class="form-label">Common Alternative Names :</label>
                             <div class="form-input">
                                 <input type="text" name="common_alt_name" id="common_alt_name" value="<?php echo $fish->common_alt_name;?>">
                             </div>
                         </div>
                         <div class="clearfix">
                             <label for="maori_name" class="form-label">Maori name :</label>
                             <div class="form-input">
                                 <input type="text" name="maori_name" id="maori_name" value="<?php echo $fish->mori_name;?>">
                             </div>
                         </div>
                         <div class="clearfix">
                             <label for="scientific_name" class="form-label">Scientific Name :</label>
                             <div class="form-input">
                                 <input type="text" name="scientific_name" id="scientific_name" value="<?php echo $fish->scientific_name;?>">
                             </div>
                         </div>

                         <div class="clearfix">	   	<form method="post" action="" name="contentform"  id="contentform"  class="form has-validation" enctype="multipart/form-data">

                             <label for="national_size" class="form-label">National Size :</label>
                             <div class="form-input">
                                 <textarea name="national_size" id="national_size"><?php echo $fish->national_size;?></textarea>
                             </div>
                         </div>
                         <div class="clearfix">
                             <label for="national_daily" class="form-label">National Daily :</label>
                             <div class="form-input">
                                 <textarea name="national_daily" id="national_daily"><?php echo $fish->national_daily;?></textarea>
                             </div>
                         </div>
                         <div class="clearfix">
                             <label for="accumulation" class="form-label">Accumulation :</label>
                             <div class="form-input">
                                 <input type="text" name="accumulation" id="accumulation" value="<?php echo $fish->accumulation;?>">
                             </div>
                         </div>
                         <div class="clearfix">
                             <label for="accumulation" class="form-label">Fish image :</label>
                             <div class="form-input">
                                 <?php if(is_file(FCPATH.'assets/fish/'.$fish->fish_image) && $fish->fish_image!=""){?>
                                     <input type="hidden" name="old_small_image" value="<?php echo $fish->fish_image;?>">
                                     <img src="<?php echo  ADMIN_URL;?>assets/fish/<?php echo  $fish->fish_image;?>"><br/>

                                 <?php }?>
                                 <input type="file" name="fish_image"><br/>
                                 <small><em>Allowed maximmum width and height 800*800</em></small>
                             </div>
                         </div>
                         <div class="form-action clearfix">
                             <button class="button" type="submit" name="submitbutton" id="submitbutton" value="Save" data-icon-primary="ui-icon-circle-check">Save</button>
                             <button class="button" type="reset">Reset</button>
                             <a href="<?php echo ADMIN_URL;?>fish" class="button approve"><span><span>Back</span></span></a>
                         </div>
                      </form>
                  </div>
                 <div class="grid_6">
                     <input type="hidden" value="<?php echo ADMIN_URL.'assets/fish/';?>" id="fishurl">
                     <input type="hidden" value="<?php echo ADMIN_URL.'fish/deleteImage/';?>" id="deleteFishImageUrl">
                     <textarea name="images_json" style="display: none;"><?php echo json_encode($fish_images);?></textarea>
                     <form action="<?php echo ADMIN_URL;?>fish/fileUpload"
                           class="dropzone"
                           id="my-awesome-dropzone"></form>
                 </div>
                 <div class="clearfix"></div>
			</section>
			</div>
	</section>
